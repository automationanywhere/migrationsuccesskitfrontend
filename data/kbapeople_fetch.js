module.exports.log = async function (search) { 
    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Basic Q2ZmNWMwNDNhMTI0ODA5NmJlMDZhYTRiNjFjMDFjMGM0OlM2ZDY1ZGMyOGIwYzMwYWZkZTI1ZDVjZWRiZjIwNTM2MA==");
    myHeaders.append("Content-Type", "application/x-www-form-urlencoded");
    
    var urlencoded = new URLSearchParams();
    urlencoded.append("grant_type", "refresh_token");
    urlencoded.append("refresh_token", "28f247e871b7482be4ccfa8eaf21f0cd72ac3520");
    
    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: urlencoded,
      redirect: 'follow'
    };
    
    let access_token = await fetch("https://aa281903p.searchunify.com/oauth/token", requestOptions)
      .then(response => response.json())
      .catch(error => console.log('error', error));
      
  
    var myHeaders = new Headers();
    myHeaders.append("Authorization", `Bearer ${access_token.accessToken}`);
    myHeaders.append("Content-Type", "application/json");
    
    var raw = JSON.stringify({
      "searchString": search,
      "from": 0,
      "pageNo": 1,
      "sortby": "_score",
      "uid": "c1050201-cd40-11ec-b71b-0242ac120002",
      "orderBy": "desc",
      "resultsPerPage": "10",
      "aggregations": []
    });
    
    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };
    
    let apeople = await fetch("https://aa281903p.searchunify.com/api/v2_search/searchResults", requestOptions)
      .then(response => response.json())
     
      .catch(error => console.log('error', error));
  
    
return apeople
};