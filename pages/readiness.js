import React, { useState, useEffect, useRef } from "react";
import Head from "next/head";
import Header from "../Components/Header/Header";
import Footer from "../Components/Footer/Footer";
// import PhaseComp from '../Components/Readiness/PhaseComp';
import axios from "axios";
import Router from "next/router";
import { Link } from "react-scroll";
import YouTube from "react-youtube";
import Success from "../Components/Success/Success";
import jwt_decode from "jwt-decode";
import * as Scroll from "react-scroll";
// import { Link, Button, Element, Events, animateScroll as scroll, scrollSpy, scroller } from 'react-scroll'
import { API_ENDPOINT_URL } from "../configuration";
import Survey from "../Components/Readiness/survey";
import validator from "validator";
import Apeople from "../Components/Readiness/apeople";
import ReadinessProgressBar from "../Components/Readiness/progress_bar";

export async function getStaticProps(context) {
  const opts = {
    method: "GET",
    headers: {
      Accept: "application/vnd.api+json",
      Authorization: `Basic ZGVlcGFrLnNpbmdoOkRlZXBha0AxOTkx`,
    },
  };
  const res = await fetch(API_ENDPOINT_URL + "get-global-constant", opts).then(
    (res) => res.json()
  );

  const access = await fetch(
    API_ENDPOINT_URL + "get-site-settings?_format=json",
    opts
  ).then((res) => res.json());

  return {
    props: { res, access },
    revalidate: 1,
  };
}

function Readiness({ res, access }) {
  const [ParentData, setParentData] = useState([]);
  const [ReadinessData, setReadinessData] = useState([]);
  const [PhaseNos, setPhaseNos] = useState(0);
  const [Username, setUsername] = useState("");
  const [ProgressData, setProgressData] = useState(2);
  const [CompletedPhases, setCompletedPhases] = useState([]);
  const [Passed, setPassed] = useState(false);
  const [RouteNavigation, setRouteNavigation] = useState(0);
  const [Scroller, setScroller] = useState(true);
  const [JWTDecoded, setJWTDecoded] = useState({});
  const [VideoURL, setVideoURL] = useState("");
  const [UID, setUID] = useState("");
  const [Video, setVideo] = useState(false);
  const [ScrollLoc, setScrollLoc] = useState(1);
  const [CurrentProject, setCurrentProject] = useState();
  const [Projects, setProjects] = useState([]);
  const sign = require("jwt-encode");
  const secret = "secret";
  // Cleanup functions
  const scrollval = useRef("");
  const success = useRef(false);
  const ApeopleData = useRef({});

  useEffect(() => {
    if (sessionStorage.getItem("system") !== null) {
      var decoded = jwt_decode(sessionStorage.getItem("system"));
      // var globals = jwt_decode(sessionStorage.getItem("globals"));
      var url_root = jwt_decode(sessionStorage.getItem("url_root"));
      var projects = jwt_decode(sessionStorage.getItem("projects"));
      setProjects(projects.data);

      setCurrentProject(sessionStorage.getItem("project_name"));
      setJWTDecoded(decoded);
      if (url_root.auth_root == "readiness") {
        axios({
          url:
            API_ENDPOINT_URL +
            `get-version-detail/${sessionStorage.getItem(
              "project_arr"
            )}?_format=json`,
          method: "get",
          headers: {
            Accept: "application/vnd.api+json",
            Authorization: `Bearer ${decoded.auth_token}`,
          },
        })
          .catch(() => Router.push("/api-failed"))
          .then((e) => {
            if (e.data.message !== undefined) {
              sessionStorage.removeItem("system");
              location.replace(
                API_ENDPOINT_URL +
                  "samllogin/?init=https://devmsk.aaiengineering.com/"
              );
            }
            var arr = e.data;
            if (arr.length !== 0) {
              arr.phase.push({ title: "Complete Journey A360" });

              setReadinessData(arr);
              let DynamicData = arr.phase.filter(
                (data) => data.complete_status == true
              );
              let filtered = DynamicData;
              let TrueData = [];
              let FallSwitches = [];
              for (let i = 0; i < filtered.length; i++) {
                TrueData.push(true);
              }
              arr.phase.forEach((element, index) => {
                if (element.complete_status == false) {
                  FallSwitches.push(index);
                }
              });
              if (TrueData.length == arr.phase.length - 1) {
                success.current = true;
                var Scroll = require("react-scroll");
                var scroller = Scroll.scroller;
                scroller.scrollTo(scrollval.current, {
                  offset: -100,
                });

                setProgressData(arr.phase.length - 1);
              } else {
                setProgressData(FallSwitches[0]);
                let LatestStep = arr.phase[FallSwitches[0]].steps.filter(
                  (data) => data.complete_status == false
                )[0].id;
                var indexOfStevie = arr.phase[FallSwitches[0]].steps.findIndex(
                  (i) => i.id === LatestStep
                );
                scrollval.current = `step_scroll_${[
                  FallSwitches[0],
                ]}_${indexOfStevie}`;
              }
              sessionStorage.setItem("prp_cache", FallSwitches[0]);
              setPhaseNos(FallSwitches[0]);
              setCompletedPhases(TrueData);
              // if(TrueData.length == 3){
              // 	success.current = true
              // 	sessionStorage.setItem("success",true)
              // }else{
              // 	sessionStorage.setItem("success",false)
              // }
            } else {
              // Router.push('/maintenance');
            
              null;
            }
          })
          .then(() => setParentData(res[0]))
          .then(() => {
            if (sessionStorage.getItem("dashboard") !== null) {
              axios({
                url: API_ENDPOINT_URL + `get-projects?_format=json`,
                method: "get",
                headers: {
                  Accept: "application/vnd.api+json",
                  Authorization: `Bearer ${decoded.auth_token}`,
                },
              })
                .catch(() => Router.push("/api-failed"))
                .then((e) => {
                  if (e.data.message !== undefined) {
                    sessionStorage.removeItem("system");
                    location.replace(
                      API_ENDPOINT_URL +
                        "samllogin/?init=https://devmsk.aaiengineering.com/"
                    );
                  }
                  setProjects(e.data);
                  let project = { data: e.data };
                  const project_token = sign(project, secret);
                  sessionStorage.setItem("projects", project_token);
                  sessionStorage.removeItem("dashboard");

                  sessionStorage.setItem(
                    "pr_cache",
                    e.data.filter(
                      (e) =>
                        e.projectId == sessionStorage.getItem("project_arr")
                    )[0].stepPercentage
                  );
                });
            }
          })
          .then(() => setPassed(true));

        if (decoded.user == null) {
          Router.push("/");
        } else {
          setUsername(decoded.first_name);
        }
      } else {
        if (typeof url_root.auth_root == "object") {
          Router.push("/");
        } else {
          Router.push(`/${url_root.auth_root}`);
        }
      }
    } else {
      Router.push("/");
    }
  }, []);
  let LicenseFormData = useRef({});
  let UserLicenseData = useRef({});
  useEffect(() => {
    if (sessionStorage.getItem("system") !== null) {
      var decoded = jwt_decode(sessionStorage.getItem("system"));
      axios({
        url: API_ENDPOINT_URL + "v1/get-license-info?_format=json",
        method: "get",
        headers: {
          Accept: "application/vnd.api+json",
          Authorization: `Bearer ${decoded.auth_token}`,
        },
      })
        .catch(() => Router.push("/api-failed"))

        .then((e) => {
          if (e.data.message !== undefined) {
            sessionStorage.removeItem("system");
            location.replace(
              API_ENDPOINT_URL +
                "samllogin/?init=https://devmsk.aaiengineering.com/"
            );
          }
          LicenseFormData.current = e.data[0];
        });

      axios({
        url:
          API_ENDPOINT_URL +
          `v1/get-migration-data/${sessionStorage.getItem(
            "project_arr"
          )}?_format=json`,
        method: "get",
        headers: {
          Accept: "application/vnd.api+json",
          Authorization: `Bearer ${decoded.auth_token}`,
        },
      })
        .catch(() => Router.push("/api-failed"))

        .then((e) => {
          if (e.data.message !== undefined) {
            sessionStorage.removeItem("system");
            location.replace(
              API_ENDPOINT_URL +
                "samllogin/?init=https://devmsk.aaiengineering.com/"
            );
          }
          UserLicenseData.current = e.data[0];
        });
    }
  }, []);
  useEffect(() => {
    if (sessionStorage.getItem("system") !== null) {
      var decoded = jwt_decode(sessionStorage.getItem("system"));
      setUID(decoded.UID);
    }
  }, []);

  function StepScrolling(location) {
    var Scroll = require("react-scroll");
    var scroller = Scroll.scroller;
    scroller.scrollTo(location, {
      offset: -100,
    });
  }

  useEffect(() => {
    var Scroll = require("react-scroll");
    var scroller = Scroll.scroller;

    if (CompletedPhases.length < 3) {
      setTimeout(() => {
        if (scrollval.current !== "step_scroll_0_0") {
          scroller.scrollTo(scrollval.current, {
            offset: -100,
          });
        }
      }, 3000);
    } else {
      if (sessionStorage.getItem("success") == null) {
        setTimeout(() => {
          // if(!sessionStorage.getItem("success") == "false"){
          scroller.scrollTo("tester", {
            offset: -100,
          });
          // }
        }, 3000);
      } else {
        setTimeout(() => {
          if (!sessionStorage.getItem("success") == "false") {
            scroller.scrollTo("tester", {
              offset: -100,
            });
          }
        }, 3000);
      }
    }

    return () => {};
  }, [Passed]);

  // ********************

  function ProjectRouting(id, name, notify, notify_for_cloud) {
    (function () {
      setPassed(false);
      sessionStorage.removeItem("survey");
    })();
    if (notify == false && notify_for_cloud == false) {
      var decoded = jwt_decode(sessionStorage.getItem("system"));

      axios({
        url: API_ENDPOINT_URL + `get-version-detail/${id}?_format=json`,
        method: "get",
        headers: {
          Accept: "application/vnd.api+json",
          Authorization: `Bearer ${decoded.auth_token}`,
        },
      })
        .catch(() => Router.push("/api-failed"))

        .then((e) => {
          if (e.data.message !== undefined) {
            sessionStorage.removeItem("system");
            location.replace(
              API_ENDPOINT_URL +
                "samllogin/?init=https://devmsk.aaiengineering.com/"
            );
          }
          var arr = e.data;
          if (arr.length !== 0) {
            var arr = e.data;
            if (arr.length !== 0) {
              arr.phase.push({ title: "Complete Journey A360" });
              setReadinessData(arr);
              let DynamicData = arr.phase.filter(
                (data) => data.complete_status == true
              );
              let filtered = DynamicData;
              let TrueData = [];
              let FallSwitches = [];
              for (let i = 0; i < filtered.length; i++) {
                TrueData.push(true);
              }
              arr.phase.forEach((element, index) => {
                if (element.complete_status == false) {
                  FallSwitches.push(index);
                }
              });
              if (TrueData.length == arr.phase.length - 1) {
                var Scroll = require("react-scroll");
                var scroller = Scroll.scroller;
                setTimeout(() => {
                  scroller.scrollTo("tester", {
                    offset: -100,
                  });
                }, 3000);
                // let scroll = Scroll.animateScroll;
                // scroll.scrollTo(2800);

                setProgressData(arr.phase.length - 1);
              } else {
                let LatestStep = arr.phase[FallSwitches[0]].steps.filter(
                  (data) => data.complete_status == false
                )[0].id;
                var indexOfStevie = arr.phase[FallSwitches[0]].steps.findIndex(
                  (i) => i.id === LatestStep
                );

                setProgressData(FallSwitches[0]);

                scrollval.current = `step_scroll_${[
                  FallSwitches[0],
                ]}_${indexOfStevie}`;
              }
              setPhaseNos(FallSwitches[0]);
              setCompletedPhases(TrueData);
              if (TrueData.length == arr.phase.length - 1) {
                success.current = true;
                sessionStorage.setItem("success", true);
              } else {
                sessionStorage.setItem("success", false);
              }
            } else {
              // Router.push('/maintenance');
              // console.log("response listed below")
              null;
            }
          } else {
            // Router.push('/maintenance');
            // console.log("response listed below")
            null;
          }
        })
        .then(() => {
          axios({
            url: API_ENDPOINT_URL + `get-projects?_format=json`,
            method: "get",
            headers: {
              Accept: "application/vnd.api+json",
              Authorization: `Bearer ${decoded.auth_token}`,
            },
          })
            .catch(() => Router.push("/api-failed"))

            .then((e) => {
              if (e.data.message !== undefined) {
                sessionStorage.removeItem("system");
                location.replace(
                  API_ENDPOINT_URL +
                    "samllogin/?init=https://devmsk.aaiengineering.com/"
                );
              }
              let project = { data: e.data };
              const project_token = sign(project, secret);
              sessionStorage.setItem("projects", project_token);
            });
        })
        .then(() => setPassed(true))
        .then(() => {
          axios({
            url: API_ENDPOINT_URL + "v1/get-license-info?_format=json",
            method: "get",
            headers: {
              Accept: "application/vnd.api+json",
              Authorization: `Bearer ${decoded.auth_token}`,
            },
          })
            .catch(() => Router.push("/api-failed"))

            .then((e) => {
              if (e.data.message !== undefined) {
                sessionStorage.removeItem("system");
                location.replace(
                  API_ENDPOINT_URL +
                    "samllogin/?init=https://devmsk.aaiengineering.com/"
                );
              }
              LicenseFormData.current = e.data[0];
            });

          axios({
            url: API_ENDPOINT_URL + `v1/get-migration-data/${id}?_format=json`,
            method: "get",
            headers: {
              Accept: "application/vnd.api+json",
              Authorization: `Bearer ${decoded.auth_token}`,
            },
          })
            .catch(() => Router.push("/api-failed"))

            .then((e) => {
              if (e.data.message !== undefined) {
                sessionStorage.removeItem("system");
                location.replace(
                  API_ENDPOINT_URL +
                    "samllogin/?init=https://devmsk.aaiengineering.com/"
                );
              }
              UserLicenseData.current = e.data[0];
            });
        })
        .then(() => {
          sessionStorage.setItem("project_arr", id);
          sessionStorage.setItem("project_name", name);
        });
    } else {
      sessionStorage.setItem("restricted", false);
      Router.push("/thankyou");
    }
  }

  function PhaseComp({
    data,
    MigrationID,
    Addlabel,
    MarkasDone,
    ToDo,
    Completed,
    Editdata,
    LicenseData,
    Phase,
    InputIndex,
    UserLicenseData,
  }) {
    const [PhaseData, setPhaseData] = useState(data);
    const [LicenseForm, setLicenseForm] = useState("none");
    const [CollapseData, setCollapseData] = useState([]);
    const [ReviewComp, setReviewComp] = useState("none");
    const RevCheck = useRef(false);
    function MarkAsDoneRequest(ID, index, scanner) {
      axios
        .post(
          API_ENDPOINT_URL + "v1/mark-done-step?_format=json ",
          {
            migration_data_id: MigrationID,
            step_completed: ID,
            bot_scanner: InputIndex == 0 && index == 1 && scanner,
            final_step:
              data.steps.length - 1 == index && InputIndex == 2 ? true : false,
            uid: UID,
          },
          {
            headers: {
              Accept: "application/vnd.api+json",
              Authorization: `Bearer ${JWTDecoded.auth_token}`,
            },
          }
        )
        .catch(() => Router.push("/api-failed"))

        .then((e) => {
          if (e.data.message !== undefined) {
            sessionStorage.removeItem("system");
            location.replace(
              API_ENDPOINT_URL +
                "samllogin/?init=https://devmsk.aaiengineering.com/"
            );
          }
          if (e.data.message !== undefined) {
            // Router.reload(window.location.pathname);
            (function () {
              setPassed(false);
            })();
            var decoded = jwt_decode(sessionStorage.getItem("system"));

            axios({
              url:
                API_ENDPOINT_URL +
                `get-version-detail/${sessionStorage.getItem(
                  "project_arr"
                )}?_format=json`,
              method: "get",
              headers: {
                Accept: "application/vnd.api+json",
                Authorization: `Bearer ${decoded.auth_token}`,
              },
            })
              .catch(() => Router.push("/api-failed"))

              .then((e) => {
                if (e.data.message !== undefined) {
                  sessionStorage.removeItem("system");
                  location.replace(
                    API_ENDPOINT_URL +
                      "samllogin/?init=https://devmsk.aaiengineering.com/"
                  );
                }
                var arr = e.data;
                if (arr.length !== 0) {
                  var arr = e.data;
                  if (arr.length !== 0) {
                    arr.phase.push({ title: "Complete Journey A360" });
                    setReadinessData(arr);
                    let DynamicData = arr.phase.filter(
                      (data) => data.complete_status == true
                    );
                    let filtered = DynamicData;
                    let TrueData = [];
                    let FallSwitches = [];
                    for (let i = 0; i < filtered.length; i++) {
                      TrueData.push(true);
                    }
                    arr.phase.forEach((element, index) => {
                      if (element.complete_status == false) {
                        FallSwitches.push(index);
                      }
                    });
                    if (TrueData.length == 3) {
                      var Scroll = require("react-scroll");
                      var scroller = Scroll.scroller;
                      setTimeout(() => {
                        scroller.scrollTo("tester", {
                          offset: -100,
                        });
                      }, 3000);
                      // let scroll = Scroll.animateScroll;
                      // scroll.scrollTo(2800);

                      setProgressData(4);
                    } else {
                      let LatestStep = arr.phase[FallSwitches[0]].steps.filter(
                        (data) => data.complete_status == false
                      )[0].id;
                      var indexOfStevie = arr.phase[
                        FallSwitches[0]
                      ].steps.findIndex((i) => i.id === LatestStep);

                      setProgressData(FallSwitches[0]);

                      scrollval.current = `step_scroll_${[
                        FallSwitches[0],
                      ]}_${indexOfStevie}`;
                    }
                    setPhaseNos(FallSwitches[0]);
                    setCompletedPhases(TrueData);
                  } else {
                    // Router.push('/maintenance');
                    // console.log("response listed below")
                    null;
                  }
                } else {
                  // Router.push('/maintenance');
                  // console.log("response listed below")
                  null;
                }
              })
              .then(() => setPassed(true))
              .then(() => {
                axios({
                  url: API_ENDPOINT_URL + `get-projects?_format=json`,
                  method: "get",
                  headers: {
                    Accept: "application/vnd.api+json",
                    Authorization: `Bearer ${decoded.auth_token}`,
                  },
                })
                  .catch(() => Router.push("/api-failed"))

                  .then((e) => {
                    if (e.data.message !== undefined) {
                      sessionStorage.removeItem("system");
                      location.replace(
                        API_ENDPOINT_URL +
                          "samllogin/?init=https://devmsk.aaiengineering.com/"
                      );
                    }
                    let project = { data: e.data };
                    const project_token = sign(project, secret);
                    sessionStorage.setItem("projects", project_token);
                  });
              })
              .then(() => {
                axios({
                  url: API_ENDPOINT_URL + "v1/get-license-info?_format=json",
                  method: "get",
                  headers: {
                    Accept: "application/vnd.api+json",
                    Authorization: `Bearer ${decoded.auth_token}`,
                  },
                })
                  .catch(() => Router.push("/api-failed"))

                  .then((e) => {
                    LicenseFormData.current = e.data[0];
                  });

                axios({
                  url:
                    API_ENDPOINT_URL +
                    `v1/get-migration-data/${sessionStorage.getItem(
                      "project_arr"
                    )}?_format=json`,
                  method: "get",
                  headers: {
                    Accept: "application/vnd.api+json",
                    Authorization: `Bearer ${decoded.auth_token}`,
                  },
                })
                  .catch(() => Router.push("/api-failed"))

                  .then((e) => {
                    if (e.data.message !== undefined) {
                      sessionStorage.removeItem("system");
                      location.replace(
                        API_ENDPOINT_URL +
                          "samllogin/?init=https://devmsk.aaiengineering.com/"
                      );
                    }
                    UserLicenseData.current = e.data[0];
                  });
              });
          }
        });
    }
    let LicenseIndex = useRef(0);
    let ArrowStyle = {
      "&::before": {
        color: "red",
      },
    };
    let LicenseID = useRef({
      id: "",
      index: 0,
    });
    const style = {
      display: "inline-block",
      height: "200px",
      position: "relative",
      width: "300px",
    };

    function MigrationHead() {
      const [Videos, setVideos] = useState(false);
      const [VideoCreds, setVideoCreds] = useState([320, "100%"]);
      const opts = {
        height: VideoCreds[0],
        width: VideoCreds[1],
        playerVars: {
          // https://developers.google.com/youtube/player_parameters
          autoplay: Videos && 1,
        },
      };
      function StepScroll(index) {
        var Scroll = require("react-scroll");
        var scroller = Scroll.scroller;
        scroller.scrollTo(`step_scroll_${InputIndex}_${index}`, {
          offset: -210,
        });
      }
      return (
        <React.Fragment>
          {data !== undefined && (
            <div
              className={`migration_item px-3 py-3 migration_phase_${InputIndex}`}
              style={{ boxShadow: "none" }}
            >
              <div className="col-sm-12 spacebetween-align-div">
                <h3 className={`py-2 fc-theme `}>{data.title}</h3>
              </div>
              <div className="col-12 row">
                <div className="col-sm-12 col-lg-6 col-xl-5">
                  {data.video_url !== "" && (
                    <React.Fragment>
                      {data.video_url.search("youtube") == -1 ? (
                        <span
                          className={`wistia_embed wistia_async_${
                            data.video_url.split("/")[
                              data.video_url.split("/").length - 1
                            ]
                          } popover=true popoverAnimateThumbnail=true`}
                          style={style}
                        >
                          &nbsp;
                        </span>
                      ) : (
                        <React.Fragment>
                          {!Video && (
                            <div
                              className="youtube_video_preLoaded"
                              style={{
                                width: "440px",
                              }}
                            >
                              <YouTube
                                videoId={
                                  data.video_url.split("=")[
                                    data.video_url.split("=").length - 1
                                  ]
                                }
                                opts={opts}
                                onPlay={() => {
                                  // setVideoCreds([750, 1400, index]);
                                  setVideoURL(
                                    data.video_url.split("=")[
                                      data.video_url.split("=").length - 1
                                    ]
                                  );
                                  setVideo(true);
                                }}
                              />
                            </div>
                          )}
                        </React.Fragment>
                      )}
                    </React.Fragment>
                  )}
                </div>
                <div
                  className="col-sm-12 col-lg-6 col-xl-6"
                  style={{ width: data.video_url == "" && "100%" }}
                >
                  <div
                    dangerouslySetInnerHTML={{
                      __html: data.footer_content,
                    }}
                  ></div>

                  <p className="h5 fw-600" style={{ fontSize: "20px" }}>
                    {data.field_steps_navigation_label}
                  </p>
                  <ul className="fw-lighter content links">
                    {data.steps.map((res, index) => {
                      return (
                        <li key={res.title} onClick={() => StepScroll(index)}>
                          <a
                            type="button"
                            style={{
                              color: "#4e6b8c",
                              textDecoration: "underline",
                            }}
                          >
                            {res.title}
                          </a>
                        </li>
                      );
                    })}
                  </ul>
                </div>
              </div>
            </div>
          )}
        </React.Fragment>
      );
    }

    function MigrationLicenseForm(params) {
      const [Name, setName] = useState("");
      const [Email, setEmail] = useState("");
      const [Nos, setNos] = useState(LicenseData.input_box_placeholder);
      const [Details, setDetails] = useState("");
      const [LicenseLoader, setLicenseLoader] = useState(false);
      useEffect(() => {
        setEmail(JWTDecoded.userData);
        setName(JWTDecoded.user);
      }, []);

      return (
        <div
          className="modal licence_modal license_modal"
          style={{ display: "block" }}
        >
          <div className="modal-dialog modal-xl">
            <style global jsx>{`
              body {
                overflow: hidden;
              }
            `}</style>
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  {data.steps[2].title}
                </h5>
                <button
                  type="button"
                  className="btn-close"
                  onClick={() => setLicenseForm("none")}
                  data-bs-dismiss="modal"
                  aria-label="Close"
                ></button>
              </div>
              <div
                className="modal-body"
                style={{
                  height: LicenseLoader && "628px",
                  width: LicenseLoader && "942px",
                }}
              >
                {!LicenseLoader && (
                  <React.Fragment>
                    <div className="col-12 px-2">
                      <div className="info-wrapper px-3 py-3">
                        <p className="d-flex">
                          <i className="info-icon"></i>{" "}
                          {LicenseData.field_licence_info_label}
                        </p>
                        <div className="col-12 px-3">
                          <div className="row">
                            <div className="col-4 license_fixed_info">
                              <div className="py-1">
                                {UserLicenseData.field_company_name_label}
                              </div>
                              <div className="fw-600">
                                {UserLicenseData.field_company_name}
                              </div>
                            </div>
                            <div className="col-4 license_fixed_info">
                              <div className="py-1">
                                {
                                  UserLicenseData.field_control_room_version_label
                                }
                              </div>
                              <div className="fw-600">
                                {UserLicenseData.field_control_room_version}
                              </div>
                            </div>
                            <div className="col-4 license_fixed_info">
                              <div className="py-1">
                                {UserLicenseData.field_migation_type_label}
                              </div>
                              <div className="fw-600">
                                {UserLicenseData.field_migation_type}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <form className="mt-4">
                        <div
                          dangerouslySetInnerHTML={{
                            __html: LicenseData.field_header_description,
                          }}
                        ></div>

                        <div className="row">
                          <div className="col-6">
                            <label className="col-form-label">
                              {LicenseData.field_name_label}
                            </label>
                            <input
                              type="text"
                              className="form-control"
                              onChange={(e) => setName(e.target.value)}
                              value={Name}
                            />
                          </div>
                          <div className="col-6">
                            <label className="col-form-label">
                              {LicenseData.field_email_label}
                            </label>
                            <input
                              type="email"
                              className="form-control"
                              value={Email}
                              onBlur={() => {
                                if (Email.search("@") == -1) {
                                  setEmail("Enter valid email");
                                } else {
                                  if (Email.split("@")[1] == "") {
                                    setEmail("Enter valid email");
                                  }
                                }
                              }}
                              onFocus={() => {
                                if (Email == "Enter valid email") {
                                  setEmail("");
                                }
                              }}
                              onChange={(e) => setEmail(e.target.value)}
                              placeholder={Email}
                            />
                          </div>
                          <div className="col-6">
                            <label className="col-form-label">
                              {LicenseData.field_phone_number_label}
                            </label>
                            <input
                              type="number"
                              onChange={(e) => {
                                setNos(e.target.value);
                              }}
                              // onBlur={(e) => {
                              // 	if (e.target.value.toString().length < 10) {
                              // 		setNos(LicenseData.input_box_placeholder);
                              // 	}
                              // }}
                              // value={Nos}
                              className="form-control"
                              placeholder={LicenseData.input_box_placeholder}
                            />
                          </div>

                          <div className="col-12">
                            <label className="col-form-label">
                              {LicenseData.field_more_detail_label}
                            </label>
                            <textarea
                              className="form-control"
                              id="message-text"
                              onChange={(input) => {
                                setDetails(input.target.value);
                              }}
                              placeholder={LicenseData.detail_placeholder}
                            ></textarea>
                          </div>
                        </div>
                      </form>
                    </div>
                    <div className="modal-footer mb-3">
                      {Name.length !== 0 && validator.isEmail(Email) ? (
                        <button
                          type="submit"
                          className="btn bg_primary_blue"
                          onClick={() => {
                            function LicenseSubmit(params) {
                              (function () {
                                setLicenseLoader(true);
                              })();

                              axios
                                .post(
                                  API_ENDPOINT_URL +
                                    "v1/save-request-license?_format=json",
                                  {
                                    name: Name,
                                    email: Email,
                                    phone:
                                      Nos == LicenseData.input_box_placeholder
                                        ? ""
                                        : Nos,
                                    more_details: Details,
                                    migration_data_id: MigrationID,
                                    uid: UID,
                                  },
                                  {
                                    headers: {
                                      Accept: "application/vnd.api+json",
                                      Authorization: `Bearer ${JWTDecoded.auth_token}`,
                                    },
                                  }
                                )

                                .then((response) => {
                                  if (response.data.message !== undefined) {
                                    sessionStorage.removeItem("system");
                                    location.replace(
                                      API_ENDPOINT_URL +
                                        "samllogin/?init=https://devmsk.aaiengineering.com/"
                                    );
                                  }
                                  function MarkAsDoneLicenseRequest(
                                    ID,
                                    index,
                                    scanner
                                  ) {
                                    axios
                                      .post(
                                        API_ENDPOINT_URL +
                                          "v1/mark-done-step?_format=json",
                                        {
                                          migration_data_id: MigrationID,
                                          step_completed: ID,
                                          bot_scanner:
                                            InputIndex == 0 &&
                                            index == 1 &&
                                            scanner,
                                          final_step:
                                            data.steps.length - 1 == index &&
                                            InputIndex == 2
                                              ? true
                                              : false,
                                          uid: UID,
                                        },
                                        {
                                          headers: {
                                            Accept: "application/vnd.api+json",
                                            Authorization: `Bearer ${JWTDecoded.auth_token}`,
                                          },
                                        }
                                      )
                                      .then((response) => {
                                        if (
                                          response.data.message !== undefined
                                        ) {
                                          sessionStorage.removeItem("system");
                                          location.replace(
                                            API_ENDPOINT_URL +
                                              "samllogin/?init=https://devmsk.aaiengineering.com/"
                                          );
                                        }
                                        data.steps[LicenseIndex.current][
                                          "licensed"
                                        ] = true;
                                        data.steps[
                                          LicenseID.current.index
                                        ].complete_status = true;
                                        setLicenseForm("none");
                                        Phase = Phase + 1;
                                        setCompletedPhases([
                                          ...CompletedPhases,
                                          true,
                                        ]);
                                        setProgressData((e) => e + 1);
                                        setPhaseData((e) => e);
                                        setReviewComp("block");
                                        if (
                                          sessionStorage.getItem("survey") ==
                                          null
                                        ) {
                                          let arr = [];
                                          arr.push({
                                            phase_id: InputIndex,
                                            status: false,
                                          });
                                          sessionStorage.setItem(
                                            "survey",
                                            JSON.stringify(arr)
                                          );
                                        } else {
                                          let arr = JSON.parse(
                                            sessionStorage.getItem("survey")
                                          );
                                          arr.push({
                                            phase_id: InputIndex,
                                            status: false,
                                          });
                                          sessionStorage.setItem(
                                            "survey",
                                            JSON.stringify(arr)
                                          );
                                        }

                                        setTimeout(() => {
                                          StepScrolling(`review_${InputIndex}`);
                                        }, 2000);
                                      })
                                      // .then(() => {
                                      // 	axios({
                                      // 	url: API_ENDPOINT_URL + `get-projects?_format=json`,
                                      // 	method: "get",
                                      // 	headers: {
                                      // 		Accept: "application/vnd.api+json",
                                      // 		Authorization: `Bearer ${JWTDecoded.auth_token}`,
                                      // 	},
                                      // }).then((e) => {

                                      // 	let project = { data: e.data };
                                      // 	const project_token = sign(project, secret);
                                      // 	sessionStorage.setItem("projects", project_token);
                                      // })
                                      // })
                                      .then(() => setLicenseLoader(false));
                                  }
                                  MarkAsDoneLicenseRequest(
                                    LicenseID.current.id,
                                    LicenseID.current.index,
                                    0
                                  );
                                });
                            }

                            if (Nos == LicenseData.input_box_placeholder) {
                              LicenseSubmit();
                            }
                            if (
                              Nos !== LicenseData.input_box_placeholder &&
                              Nos.toString().length >= 10
                            ) {
                              LicenseSubmit();
                            }
                            if (
                              Nos !== LicenseData.input_box_placeholder &&
                              Nos.toString().length < 10
                            ) {
                              setNos(LicenseData.input_box_placeholder);
                            }
                          }}
                        >
                          {LicenseData.field_submit_label_for_licence}
                        </button>
                      ) : (
                        <button className="disabled_btn" type="button">
                          Submit Request
                        </button>
                      )}
                    </div>
                  </React.Fragment>
                )}
                {LicenseLoader && <div className="loader"></div>}
              </div>
            </div>
          </div>
        </div>
      );
    }
    function MigrationSteps(params) {
      const [CompletedSteps, setCompletedSteps] = useState([]);
      const [AdditionalArray, setAdditionalArray] = useState([]);
      const [Collapsed, setCollapsed] = useState([]);
      const [VideoCreds, setVideoCreds] = useState([350, "30%"]);
      const [EditLock, setEditLock] = useState(false);
      // const [Video, setVideo] = useState(false);
      const [CompletedLatest, setCompletedLatest] = useState(0);
      const [BotRunnerState, setBotRunnerState] = useState(null);
      const [InnerVideo, setInnerVideo] = useState(false);
      const [InnerVideoURL, setInnerVideoURL] = useState(``);
      const [ApeopleData, setApeopleData] = useState([]);
      const [ApeopleLink, setApeopleLink] = useState(false);
      const [ApeopleSwitch, setApeopleSwitch] = useState(1);

      var Element = Scroll.Element;

      let icons = {
        video: "icon-video",
        document: "icon-doc",
        Documentation: "icon-doc",
        download: "icon-download",
        blog: "bi bi-file-earmark-richtext",
        link: "icon-link",
        globe: "icon-globe",
        document_white: "icon-white-document",
        download_white: "icon-white-download",
        video_white: "icon-white-video",
        globe_white: "icon-white-globe",
      };

      function StepScrolling(location) {
        var Scroll = require("react-scroll");
        var scroller = Scroll.scroller;
        scroller.scrollTo(location, {
          offset: -100,
        });
      }

      function MarkAsUnDoneRequest(ID) {
        axios
          .post(
            API_ENDPOINT_URL + "v1/mark-undone-step?_format=json",
            {
              migration_data_id: MigrationID,
              step_undone: ID,
              uid: UID,
            },
            {
              headers: {
                Accept: "application/vnd.api+json",
                Authorization: `Bearer ${JWTDecoded.auth_token}`,
              },
            }
          )
          .then((response) => {
            if (response.data.message !== undefined) {
              sessionStorage.removeItem("system");
              location.replace(
                API_ENDPOINT_URL +
                  "samllogin/?init=https://devmsk.aaiengineering.com/"
              );
            }
          });
      }

      function ThankyouComponent({ number }) {
        return (
          <div className="collapse-item">
            <div className="col-12 collapse-header spacebetween-align-div">
              <h5 className="px-4">{ReadinessData.thank_you[0].title}</h5>
            </div>

            <div className="collapse-body">
              <div className="col-12 row px-3 py-2">
                <div
                  dangerouslySetInnerHTML={{
                    __html: ReadinessData.thank_you[0].field_thanks_you_detail,
                  }}
                ></div>

                <div
                  data-collapse-swticher="text-collapse"
                  className="additional_resource"
                >
                  {AdditionalArray.includes(number) ? (
                    <h6
                      data-collapse-btn="text-collapse"
                      className="resource_header fw-600 p-3"
                      onClick={() => {
                        setAdditionalArray((e) =>
                          e.filter((data) => data !== number)
                        );
                      }}
                    >
                      <i className="bi bi-chevron-down inverse-arrow"></i>{" "}
                      {Addlabel}
                    </h6>
                  ) : (
                    <h6
                      data-collapse-btn="text-collapse"
                      className="resource_header fw-600 p-3"
                      onClick={() => {
                        setAdditionalArray([...AdditionalArray, number]);
                      }}
                    >
                      <i className="bi bi-chevron-down"></i> {Addlabel}
                    </h6>
                  )}
                  {AdditionalArray.includes(number) && (
                    <React.Fragment>
                      <div
                        data-collapse-body="text-collapse"
                        className="links p-3"
                        style={{
                          borderTop: "0.25px solid #a9a9a9",
                        }}
                      >
                        <div className="row">
                          {ReadinessData.thank_you[0].field_additional_resource.map(
                            (data) => {
                              return data.uri.search("wistia") !== -1 ? (
                                <a
                                  className="align-y col-12 col-md-6"
                                  href={data.uri}
                                  onClick={(e) => {
                                    e.preventDefault();
                                    if (data.uri.search("youtube") !== -1) {
                                      e.preventDefault();
                                      setInnerVideoURL("9DO1GRLXqEk");
                                      setInnerVideo(true);
                                    }
                                  }}
                                  target="_blank"
                                  rel="noreferrer"
                                >
                                  <i className={icons[data.description]}></i>{" "}
                                  <span
                                    className="px-2"
                                    style={{
                                      position: "relative",
                                      bottom: "0px",
                                    }}
                                  >
                                    {data.title}
                                  </span>
                                  <div className="wistia_additional">
                                    <span
                                      className={`wistia_embed wistia_async_${
                                        data.uri.split("/")[
                                          data.uri.split("/").length - 1
                                        ]
                                      } popover=true autoplay=true popoverAnimateThumbnail=true`}
                                      style={{
                                        display: "inline-block",
                                        height: "35px",
                                        position: "absolute",
                                        width: "19vw",
                                        marginLeft: "-400px",
                                        marginTop: "-15px",
                                      }}
                                    >
                                      &nbsp;
                                    </span>
                                  </div>
                                </a>
                              ) : (
                                <a
                                  className="align-y col-12 col-md-6"
                                  href={data.uri}
                                  onClick={(e) => {
                                    if (data.uri.search("youtube") !== -1) {
                                      e.preventDefault();
                                      setInnerVideoURL("9DO1GRLXqEk");
                                      setInnerVideo(true);
                                    }
                                  }}
                                  target="_blank"
                                  rel="noreferrer"
                                >
                                  <i className={icons[data.description]}></i>{" "}
                                  <span
                                    className="px-2"
                                    style={{
                                      position: "relative",
                                      bottom: "0px",
                                    }}
                                  >
                                    {data.title}
                                  </span>
                                </a>
                              );
                            }
                          )}
                        </div>
                      </div>
                    </React.Fragment>
                  )}
                </div>

                <div
                  dangerouslySetInnerHTML={{
                    __html: ReadinessData.thank_you[0].footer_content,
                  }}
                ></div>

                <div className="links thank_comp">
                  <div className="row">
                    {ReadinessData.thank_you[0].field_footer_content_links.map(
                      (data) => {
                        return (
                          <a
                            href={data.uri}
                            className="align-y col-12 col-md-6"
                            target="_blank"
                            key={data.uri}
                            rel="noreferrer"
                          >
                            <i className={icons[data.description]}></i>
                            {""}
                            <span className="px-2">{data.title}</span>
                          </a>
                        );
                      }
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
      }
      const opts = {
        height: VideoCreds[0],
        width: VideoCreds[1],
        playerVars: {
          // https://developers.google.com/youtube/player_parameters
          autoplay: Video && 1,
          origin: window.location.href,
        },
      };
      let RunnerPercentageValue = useRef("");

      useEffect(() => {
        document.addEventListener("click", (e) => {
          setVideoCreds([350, 570]);
          setVideo(false);
          if (e.target.className == "youtube_expanded_view") {
            setInnerVideo(false);
          }
        });
        let CompletedStepLatest = [];
        let CompletedStepsArr = [];
        data.steps.forEach((e, index) => {
          if (e.complete_status == false) {
            CompletedStepLatest.push(index);
          } else {
            CompletedStepsArr.push(index);
          }
        });

        setCompletedLatest(CompletedStepLatest[0]);
        setCollapsed(CompletedStepsArr);
      }, []);
      return (
        <div className="col-12 mt-3">
          <div className="row">
            <div className="collapse_wrapper col-12 col-md-9">
              {InnerVideo && (
                <div
                  className="youtube_expanded_view"
                  style={{
                    backgroundColor: "#00000033",
                    left: "0",
                  }}
                >
                  <div className="video_root">
                    <YouTube
                      videoId={InnerVideoURL}
                      opts={{
                        height: (82 * window.innerHeight) / 100,
                        width: (80 * window.innerWidth) / 100,
                        playerVars: {
                          autoplay: 1,
                        },
                      }}
                    />
                  </div>
                </div>
              )}
              {data.steps.map((steps, index) => {
                return (
                  <React.Fragment key={steps.title}>
                    <Element name={`step_scroll_${InputIndex}_${index}`}>
                      {steps.licensed !== undefined ? (
                        steps.complete_status && (
                          <ThankyouComponent number={index} />
                        )
                      ) : (
                        <div
                          className={`collapse-item active phase_${InputIndex}_${index}`}
                          style={{
                            borderColor:
                              Phase == InputIndex && CompletedLatest == index
                                ? "#4e6b8c "
                                : "transparent",
                          }}
                        >
                          <div
                            className="col-12 collapse-header spacebetween-align-div px-3"
                            onClick={() => {
                              if (Collapsed.includes(index) == true) {
                                setCollapsed((e) =>
                                  e.filter((data) => data !== index)
                                );
                              } else {
                                setCollapsed([...Collapsed, index]);
                              }
                            }}
                          >
                            <div className="col-9">
                              {Collapsed.includes(index) ? (
                                <div className="top-align-div">
                                  <i
                                    className="bi bi-chevron-down fc-blue px-2 inverse-arrow"
                                    onClick={() => {
                                      setCollapsed((e) =>
                                        e.filter((data) => data !== index)
                                      );
                                    }}
                                  ></i>
                                  <h5 className="fw-600">{steps.title}</h5>
                                </div>
                              ) : (
                                <div className="top-align-div">
                                  <i
                                    className="bi bi-chevron-down fc-blue px-2"
                                    onClick={() => {
                                      setCollapsed([...Collapsed, index]);
                                    }}
                                  ></i>
                                  <h5 className="fw-600">{steps.title}</h5>
                                </div>
                              )}
                            </div>
                            {Phase == InputIndex && CompletedLatest == index && (
                              <div className="col-3 right-align-div">
                                <div className="btn info-wrapper px-2 py-1 rounded-pill">
                                  {ToDo}
                                </div>
                              </div>
                            )}

                            {steps.complete_status && (
                              <div className="col-3 right-align-div">
                                <div className="btn success-wrapper px-2 py-1 rounded-pill">
                                  {Completed}
                                </div>
                              </div>
                            )}
                          </div>
                          {Collapsed.includes(index) == false && (
                            <div className="collapse-body">
                              {steps.warning_message.length !== 0 && (
                                <div
                                  dangerouslySetInnerHTML={{
                                    __html: steps.warning_message[0].value,
                                  }}
                                ></div>
                              )}
                              <div className="col-12 px-3 py-3">
                                <div className="row">
                                  {steps.video_url !== "" && (
                                    <div className="col-sm-12 col-lg-6 col-xl-5">
                                      {steps.video_url.search("youtube") ==
                                      -1 ? (
                                        <span
                                          className={`wistia_embed wistia_async_${
                                            steps.video_url.split("/")[
                                              steps.video_url.split("/")
                                                .length - 1
                                            ]
                                          } popover=true autoplay=true popoverAnimateThumbnail=true`}
                                          style={style}
                                        >
                                          &nbsp;
                                        </span>
                                      ) : (
                                        <React.Fragment>
                                          {!Video && (
                                            <div className="youtube_video_preLoaded">
                                              <YouTube
                                                videoId={
                                                  steps.video_url.split("=")[
                                                    steps.video_url.split("=")
                                                      .length - 1
                                                  ]
                                                }
                                                opts={opts}
                                                onPlay={() => {
                                                  // setVideoCreds([750, 1400, index]);
                                                  setVideoURL(
                                                    steps.video_url.split("=")[
                                                      steps.video_url.split("=")
                                                        .length - 1
                                                    ]
                                                  );
                                                  setVideo(true);
                                                }}
                                              />
                                            </div>
                                          )}
                                        </React.Fragment>
                                      )}
                                    </div>
                                  )}

                                  <div
                                    className="col-sm-12 col-lg-6 col-xl-7"
                                    style={{
                                      width: steps.video_url == "" && "100%",
                                    }}
                                  >
                                    <div
                                      dangerouslySetInnerHTML={{
                                        __html: steps.footer_content,
                                      }}
                                    ></div>
                                    {!steps.complete_status &&
                                      index == 1 &&
                                      InputIndex == 0 && (
                                        <div className="form-group">
                                          {!EditLock ? (
                                            <input
                                              type="number"
                                              className="textbox col-12 col-sm-10 col-lg-10 col-xl-10 is-valid"
                                              max="100"
                                              min="0"
                                              contentEditable={true}
                                              maxLength="3"
                                              onChange={(e) => {
                                                if (
                                                  e.target.value.search("-") ==
                                                  -1
                                                ) {
                                                  RunnerPercentageValue.current =
                                                    e.target.value;
                                                  if (
                                                    e.target.value.length == 0
                                                  ) {
                                                    setBotRunnerState(null);
                                                  } else {
                                                    setBotRunnerState(false);
                                                  }
                                                } else {
                                                  e.target.value = 0;
                                                }
                                              }}
                                              placeholder="Enter migratable bot percentage (%)"
                                              required=""
                                            />
                                          ) : (
                                            <input
                                              type="number"
                                              className="textbox col-12 col-sm-10 col-lg-10 col-xl-10 is-valid"
                                              max="100"
                                              min="0"
                                              contentEditable="false"
                                              maxLength="3"
                                              value={
                                                RunnerPercentageValue.current
                                              }
                                              placeholder={
                                                RunnerPercentageValue.current
                                              }
                                              required=""
                                            />
                                          )}

                                          <p className="label_bot_percentage col-12 col-sm-10 col-lg-10 col-xl-10">
                                            The number field should only accept
                                            numbers (no symbols, periods,
                                            letters), and only up to 3 digits{" "}
                                          </p>
                                          {typeof BotRunnerState == "string" &&
                                            parseInt(
                                              RunnerPercentageValue.current
                                            ) > 100 && (
                                              <p className="label-error col-12 col-sm-10 col-lg-10 col-xl-10">
                                                <i className="bi bi-exclamation-square-fill"></i>{" "}
                                                Please enter a percentage
                                                0%-100%
                                              </p>
                                            )}
                                          {data.steps[index - 1]
                                            .complete_status == true ? (
                                            <React.Fragment>
                                              {BotRunnerState == null ||
                                              typeof BotRunnerState ==
                                                "string" ? (
                                                <div
                                                  className="btn disable"
                                                  style={{ padding: "0" }}
                                                >
                                                  <span className="disabled_btn">
                                                    {" "}
                                                    Submit
                                                  </span>
                                                  <span className="tooltiptext">
                                                    Please enter the percent
                                                    readiness from your Bot
                                                    Scanner report
                                                  </span>
                                                </div>
                                              ) : (
                                                <button
                                                  className={
                                                    BotRunnerState
                                                      ? "btn btn-sm btn-border-blue btn-proceed px-3"
                                                      : `btn btn-secondary bg_primary_blue mt-3`
                                                  }
                                                  type="submit"
                                                  onClick={() => {
                                                    if (
                                                      parseInt(
                                                        RunnerPercentageValue.current
                                                      ) > 100
                                                    ) {
                                                      setBotRunnerState(
                                                        "Please enter a percentage 0%-100%"
                                                      );
                                                    } else {
                                                      setBotRunnerState(true);
                                                      setEditLock(true);
                                                    }
                                                    if (BotRunnerState) {
                                                      setEditLock(false);
                                                      setBotRunnerState(false);
                                                    }
                                                  }}
                                                  disabled=""
                                                >
                                                  {BotRunnerState
                                                    ? "Edit"
                                                    : "Submit"}
                                                </button>
                                              )}
                                            </React.Fragment>
                                          ) : (
                                            <button
                                              className="disabled_btn  "
                                              type="submit"
                                              disabled=""
                                            >
                                              Submit
                                            </button>
                                          )}
                                        </div>
                                      )}

                                    <br />
                                  </div>
                                </div>
                              </div>
                              <div
                                className="px-2"
                                data-collapse-swticher="text-collapse"
                                style={{
                                  cursor: "pointer",
                                }}
                              ></div>
                              {index == 1 && (
                                <React.Fragment>
                                  {BotRunnerState == true && (
                                    <React.Fragment>
                                      {parseInt(
                                        RunnerPercentageValue.current
                                      ) >= 95 &&
                                        parseInt(
                                          RunnerPercentageValue.current
                                        ) <= 99 && (
                                          <div
                                            dangerouslySetInnerHTML={{
                                              __html:
                                                ParentData
                                                  .field_more_than_95_detail[0]
                                                  .value,
                                            }}
                                          ></div>
                                        )}
                                      {parseInt(RunnerPercentageValue.current) <
                                        95 && (
                                        <div
                                          dangerouslySetInnerHTML={{
                                            __html:
                                              ParentData
                                                .field_less_than_95_detail[0]
                                                .value,
                                          }}
                                        ></div>
                                      )}
                                      {parseInt(
                                        RunnerPercentageValue.current
                                      ) == 100 && (
                                        <div
                                          dangerouslySetInnerHTML={{
                                            __html:
                                              ParentData
                                                .field_exact_100_detail[0]
                                                .value,
                                          }}
                                        ></div>
                                      )}
                                    </React.Fragment>
                                  )}
                                </React.Fragment>
                              )}
                              {steps.field_additional_resources.length !== 0 &&
                                (Phase == InputIndex &&
                                CompletedLatest == index ? (
                                  <React.Fragment>
                                    <div
                                      data-collapse-swticher="text-collapse"
                                      className="additional_resource"
                                    >
                                      <h6
                                        data-collapse-btn="text-collapse"
                                        className="resource_header fw-600 p-3"
                                      >
                                        <i className="bi bi-chevron-down"></i>{" "}
                                        Additonal Resources
                                      </h6>
                                      <div
                                        data-collapse-body="text-collapse"
                                        className="links p-3"
                                        style={{
                                          borderTop: "0.25px solid #a9a9a9",
                                        }}
                                      >
                                        <div className="row">
                                          {steps.field_additional_resources.map(
                                            (data) => {
                                              return (
                                                //   f89ddewr8n

                                                data.uri.search("wistia") !==
                                                  -1 ? (
                                                  <a
                                                    className="align-y col-12 col-md-6"
                                                    href={data.uri}
                                                    onClick={(e) => {
                                                      e.preventDefault();
                                                    }}
                                                    target="_blank"
                                                    rel="noreferrer"
                                                  >
                                                    <i
                                                      className={
                                                        icons[data.description]
                                                      }
                                                    ></i>{" "}
                                                    <span
                                                      className="px-2"
                                                      style={{
                                                        position: "relative",
                                                        bottom: "0px",
                                                      }}
                                                    >
                                                      {data.title}
                                                    </span>
                                                    <div
                                                      className="wistia_additional"
                                                      style={{
                                                        position: "relative",
                                                        opacity: "0",
                                                      }}
                                                    >
                                                      <span
                                                        className={`wistia_embed wistia_async_${
                                                          data.uri.split("/")[
                                                            data.uri.split("/")
                                                              .length - 1
                                                          ]
                                                        } popover=true autoplay=true popoverAnimateThumbnail=true`}
                                                        style={{
                                                          display:
                                                            "inline-block",
                                                          height: "35px",
                                                          position: "absolute",
                                                          width: "19vw",
                                                          marginLeft: "-6.5vw",
                                                          marginTop: "-15px",
                                                        }}
                                                      >
                                                        &nbsp;
                                                      </span>
                                                    </div>
                                                  </a>
                                                ) : (
                                                  <a
                                                    className="align-y col-12 col-md-6"
                                                    href={data.uri}
                                                    onClick={(e) => {
                                                      if (
                                                        data.uri.search(
                                                          "youtube"
                                                        ) !== -1
                                                      ) {
                                                        e.preventDefault();
                                                        setInnerVideoURL(
                                                          "9DO1GRLXqEk"
                                                        );
                                                        setInnerVideo(true);
                                                      }
                                                    }}
                                                    target="_blank"
                                                    rel="noreferrer"
                                                  >
                                                    <i
                                                      className={
                                                        icons[data.description]
                                                      }
                                                    ></i>{" "}
                                                    <span
                                                      className="px-2"
                                                      style={{
                                                        position: "relative",
                                                        bottom: "0px",
                                                      }}
                                                    >
                                                      {data.title}
                                                    </span>
                                                  </a>
                                                )
                                              );
                                            }
                                          )}
                                        </div>
                                      </div>
                                    </div>
                                    {/* <h6
                                              data-collapse-btn="text-collapse fw-600"
                                              style={{ fontWeight: "600" }}
                                              onClick={() => {
                                                setAdditionalArray([
                                                  ...AdditionalArray,
                                                  index,
                                                ]);
                                              }}
                                            >
                                              <i className="bi bi-chevron-down"></i>{" "}
                                              {Addlabel}
                                            </h6> */}
                                  </React.Fragment>
                                ) : (
                                  <React.Fragment>
                                    <React.Fragment>
                                      <div
                                        data-collapse-swticher="text-collapse"
                                        className="additional_resource"
                                      >
                                        {AdditionalArray.includes(index) ? (
                                          <h6
                                            data-collapse-btn="text-collapse"
                                            style={{ fontWeight: "600" }}
                                            className="resource_header fw-600 p-3"
                                            onClick={() => {
                                              setAdditionalArray((e) =>
                                                e.filter(
                                                  (data) => data !== index
                                                )
                                              );
                                            }}
                                          >
                                            <i className="bi bi-chevron-down inverse-arrow"></i>{" "}
                                            {Addlabel}
                                          </h6>
                                        ) : (
                                          <h6
                                            data-collapse-btn="text-collapse"
                                            style={{ fontWeight: "600" }}
                                            className="resource_header fw-600 p-3"
                                            onClick={() => {
                                              setAdditionalArray([
                                                ...AdditionalArray,
                                                index,
                                              ]);
                                            }}
                                          >
                                            <i className="bi bi-chevron-down"></i>{" "}
                                            {Addlabel}
                                          </h6>
                                        )}

                                        {AdditionalArray.includes(index) && (
                                          <React.Fragment>
                                            <div
                                              data-collapse-body="text-collapse"
                                              className="links p-3"
                                              style={{
                                                borderTop:
                                                  "0.25px solid #a9a9a9",
                                              }}
                                            >
                                              <div className="row">
                                                {steps.field_additional_resources.map(
                                                  (data) => {
                                                    return data.uri.search(
                                                      "wistia"
                                                    ) !== -1 ? (
                                                      <a
                                                        className="align-y col-12 col-md-6"
                                                        href={data.uri}
                                                        onClick={(e) => {
                                                          e.preventDefault();
                                                          if (
                                                            data.uri.search(
                                                              "youtube"
                                                            ) !== -1
                                                          ) {
                                                            e.preventDefault();
                                                            setInnerVideoURL(
                                                              "9DO1GRLXqEk"
                                                            );
                                                            setInnerVideo(true);
                                                          }
                                                        }}
                                                        target="_blank"
                                                        rel="noreferrer"
                                                      >
                                                        <i
                                                          className={
                                                            icons[
                                                              data.description
                                                            ]
                                                          }
                                                        ></i>{" "}
                                                        <span
                                                          className="px-2"
                                                          style={{
                                                            position:
                                                              "relative",
                                                            bottom: "0px",
                                                          }}
                                                        >
                                                          {data.title}
                                                        </span>
                                                        <div className="wistia_additional">
                                                          <span
                                                            className={`wistia_embed wistia_async_${
                                                              data.uri.split(
                                                                "/"
                                                              )[
                                                                data.uri.split(
                                                                  "/"
                                                                ).length - 1
                                                              ]
                                                            } popover=true autoplay=true popoverAnimateThumbnail=true`}
                                                            style={{
                                                              display:
                                                                "inline-block",
                                                              height: "35px",
                                                              position:
                                                                "absolute",
                                                              width: "19vw",
                                                              marginLeft:
                                                                "-400px",
                                                              marginTop:
                                                                "-15px",
                                                            }}
                                                          >
                                                            &nbsp;
                                                          </span>
                                                        </div>
                                                      </a>
                                                    ) : (
                                                      <a
                                                        className="align-y col-12 col-md-6"
                                                        href={data.uri}
                                                        onClick={(e) => {
                                                          if (
                                                            data.uri.search(
                                                              "youtube"
                                                            ) !== -1
                                                          ) {
                                                            e.preventDefault();
                                                            setInnerVideoURL(
                                                              "9DO1GRLXqEk"
                                                            );
                                                            setInnerVideo(true);
                                                          }
                                                        }}
                                                        target="_blank"
                                                        rel="noreferrer"
                                                      >
                                                        <i
                                                          className={
                                                            icons[
                                                              data.description
                                                            ]
                                                          }
                                                        ></i>{" "}
                                                        <span
                                                          className="px-2"
                                                          style={{
                                                            position:
                                                              "relative",
                                                            bottom: "0px",
                                                          }}
                                                        >
                                                          {data.title}
                                                        </span>
                                                      </a>
                                                    );
                                                  }
                                                )}
                                              </div>
                                            </div>
                                          </React.Fragment>
                                        )}
                                      </div>
                                    </React.Fragment>
                                  </React.Fragment>
                                ))}
                              <div className="col-12 px-2 py-2" id={index}>
                                <div className="row spacebetween-align-div">
                                  <div className="col-12 col-sm-12 col-lg-10 col-xl-10 d_flex btn_container">
                                    {steps.field_request_licence_button !==
                                      undefined && (
                                      <React.Fragment>
                                        {data.steps[index - 1]
                                          .complete_status == true &&
                                        steps.complete_status == false ? (
                                          <div
                                            data-bs-toggle="modal"
                                            data-bs-target="#request_license"
                                            data-bs-whatever="@license"
                                            className="btn h6 bg_primary_blue btn_custom"
                                            onClick={() => {
                                              LicenseIndex.current = index;
                                              LicenseID.current = {
                                                id: steps.id,
                                                index: index,
                                              };
                                              setLicenseForm("block");
                                            }}
                                          >
                                            <i className="bi bi-box-arrow-up-right"></i>{" "}
                                            <span
                                              style={{
                                                marginLeft: "10px",
                                              }}
                                            >
                                              {" "}
                                              Order my license
                                            </span>{" "}
                                          </div>
                                        ) : (
                                          <div
                                            data-bs-toggle="modal"
                                            data-bs-target="#request_license"
                                            data-bs-whatever="@license"
                                            className="disabled_btn"
                                          >
                                            <i className="bi bi-box-arrow-up-right"></i>{" "}
                                            <span
                                              style={{
                                                marginLeft: "10px",
                                              }}
                                            >
                                              {" "}
                                              Order my license
                                            </span>{" "}
                                          </div>
                                        )}
                                      </React.Fragment>
                                    )}

                                    {steps.field_footer_content_links.map(
                                      (stepLink, Linkindex) => {
                                        return (
                                          <a
                                            href={stepLink.uri}
                                            target="_blank"
                                            rel="noreferrer"
                                            style={{
                                              textDecoration: "none",
                                            }}
                                          >
                                            <div className="btn h6 btn-border-blue btn_custom">
                                              <i
                                                className={
                                                  icons[stepLink.description]
                                                }
                                              ></i>{" "}
                                              <span> {stepLink.title}</span>{" "}
                                            </div>
                                          </a>
                                        );
                                        // return (
                                        //   <React.Fragment key={steps.title}>

                                        //     {InputIndex == 0 && (
                                        //       <React.Fragment>
                                        //         {Linkindex == 0 ? (
                                        //           <a
                                        //             href={stepLink.uri}
                                        //             target="_blank"
                                        //             rel="noreferrer"
                                        //             style={{
                                        //               textDecoration:"none"
                                        //             }}
                                        //           >
                                        //             <div
                                        //               className={`${index == 0
                                        //                   ? "btn h6 bg_primary_blue btn_custom"
                                        //                   : "btn h6 btn-border-blue btn_custom"
                                        //                 }`}
                                        //             >
                                        //               <i
                                        //                 className={
                                        //                   icons[
                                        //                   stepLink.description
                                        //                   ]
                                        //                 }
                                        //               ></i>{" "}
                                        //               <span>
                                        //                 {" "}
                                        //                 {stepLink.title}
                                        //               </span>{" "}
                                        //             </div>
                                        //           </a>
                                        //         ) : (
                                        //           <a
                                        //             href={stepLink.uri}
                                        //             target="_blank"
                                        //             rel="noreferrer"
                                        //             style={{
                                        //               textDecoration:"none"
                                        //             }}
                                        //           >
                                        //             <div className="btn h6 btn-border-blue btn_custom">
                                        //               <i
                                        //                 className={
                                        //                   icons[
                                        //                   stepLink.description
                                        //                   ]
                                        //                 }
                                        //               ></i>{" "}
                                        //               <span>
                                        //                 {" "}
                                        //                 {stepLink.title}
                                        //               </span>{" "}
                                        //             </div>
                                        //           </a>
                                        //         )}
                                        //       </React.Fragment>
                                        //     )}
                                        //     {InputIndex == 1 && (
                                        //       <React.Fragment>
                                        //         {Linkindex == 0 ? (
                                        //           <a
                                        //             href={stepLink.uri}
                                        //             target="_blank"
                                        //             rel="noreferrer"
                                        //             style={{
                                        //               textDecoration:"none"
                                        //             }}
                                        //           >
                                        //             <div
                                        //               className={`${[0, 1].includes(index)
                                        //                   ? "btn h6 bg_primary_blue btn_custom"
                                        //                   : "btn h6 btn-border-blue btn_custom"
                                        //                 }`}
                                        //             >
                                        //               <i
                                        //                 className={
                                        //                   icons[
                                        //                   stepLink.description
                                        //                   ]
                                        //                 }
                                        //               ></i>{" "}
                                        //               <span>
                                        //                 {" "}
                                        //                 {stepLink.title}
                                        //               </span>{" "}
                                        //             </div>
                                        //           </a>
                                        //         ) : (
                                        //           <a
                                        //             href={stepLink.uri}
                                        //             target="_blank"
                                        //             rel="noreferrer"
                                        //             style={{
                                        //               textDecoration:"none"
                                        //             }}
                                        //           >
                                        //             <div className="btn h6 btn-border-blue btn_custom">
                                        //               <i
                                        //                 className={
                                        //                   icons[
                                        //                   stepLink.description
                                        //                   ]
                                        //                 }
                                        //               ></i>{" "}
                                        //               <span>
                                        //                 {" "}
                                        //                 {stepLink.title}
                                        //               </span>{" "}
                                        //             </div>
                                        //           </a>
                                        //         )}
                                        //       </React.Fragment>
                                        //     )}
                                        //     {InputIndex == 2 && (
                                        //       <React.Fragment>
                                        //         <a
                                        //           href={stepLink.uri}
                                        //           target="_blank"
                                        //           rel="noreferrer"
                                        //           style={{
                                        //             textDecoration:"none"
                                        //           }}
                                        //         >
                                        //           <div className="btn h6 btn-border-blue btn_custom">
                                        //             <i
                                        //               className={
                                        //                 icons[
                                        //                 stepLink.description
                                        //                 ]
                                        //               }
                                        //             ></i>{" "}
                                        //             <span>
                                        //               {" "}
                                        //               {stepLink.title}
                                        //             </span>{" "}
                                        //           </div>
                                        //         </a>
                                        //       </React.Fragment>
                                        //     )}
                                        //   </React.Fragment>
                                        // );
                                      }
                                    )}
                                  </div>

                                  {steps.field_request_licence_button ==
                                    undefined && (
                                    <React.Fragment>
                                      {steps.complete_status ? (
                                        index == CompletedLatest - 1 && (
                                          <div
                                            className="btn btn-sm btn-border-blue btn-proceed mx-3"
                                            onClick={() => {
                                              data.steps[
                                                index
                                              ].complete_status = false;
                                              setCollapsed((e) => e);
                                              MarkAsUnDoneRequest(steps.id);
                                              setCompletedLatest((e) => e - 1);
                                              if (
                                                InputIndex == 0 &&
                                                index == 1
                                              ) {
                                                setBotRunnerState(null);
                                              }
                                            }}
                                          >
                                            <i className="bi bi-check-lg"></i>
                                            {Editdata}
                                          </div>
                                        )
                                      ) : index !== 0 ? (
                                        data.steps[index - 1]
                                          .complete_status ? (
                                          index == data.steps.length - 1 ? (
                                            <div
                                              className="btn btn-sm btn-border-blue btn-proceed mx-3"
                                              style={{ marginBottom: "0px" }}
                                              onClick={() => {
                                                if (
                                                  sessionStorage.getItem(
                                                    "survey"
                                                  ) == null
                                                ) {
                                                  let arr = [];
                                                  arr.push({
                                                    phase_id: InputIndex,
                                                    status: false,
                                                  });
                                                  sessionStorage.setItem(
                                                    "survey",
                                                    JSON.stringify(arr)
                                                  );
                                                } else {
                                                  let arr = JSON.parse(
                                                    sessionStorage.getItem(
                                                      "survey"
                                                    )
                                                  );
                                                  arr.push({
                                                    phase_id: InputIndex,
                                                    status: false,
                                                  });
                                                  sessionStorage.setItem(
                                                    "survey",
                                                    JSON.stringify(arr)
                                                  );
                                                }
                                                setTimeout(() => {
                                                  StepScrolling(
                                                    `review_${InputIndex}`
                                                  );
                                                }, 2000);

                                                // setTimeout(() => {
                                                //   StepScrolling(`migration_phase_${InputIndex + 1}`)

                                                //  }, 1000);

                                                data.steps[
                                                  index
                                                ].complete_status = true;
                                                setCollapsed([
                                                  ...Collapsed,
                                                  index,
                                                ]);
                                                MarkAsDoneRequest(
                                                  steps.id,
                                                  index,
                                                  RunnerPercentageValue.current
                                                );
                                                setCompletedLatest(
                                                  (e) => e + 1
                                                );
                                                setCompletedPhases([
                                                  ...CompletedPhases,
                                                  true,
                                                ]);

                                                // if (InputIndex >= 3) {
                                                //   setProgressData(5);

                                                // } else {
                                                //   setProgressData((e) => e + 1);
                                                // }
                                                setProgressData((e) => e + 1);
                                                // setProgressData(4)
                                                sessionStorage.setItem(
                                                  "dashboard",
                                                  true
                                                );
                                              }}
                                            >
                                              <i className="bi bi-check-lg"></i>
                                              {MarkasDone}
                                            </div>
                                          ) : index == 1 && Phase == 0 ? (
                                            <React.Fragment>
                                              {BotRunnerState == true ? (
                                                <div
                                                  className="btn btn-sm btn-border-blue btn-proceed mx-3"
                                                  style={{
                                                    marginBottom: "0px",
                                                  }}
                                                  onClick={() => {
                                                    setTimeout(() => {
                                                      StepScrolling(
                                                        `step_scroll_${InputIndex}_${
                                                          index + 1
                                                        }`
                                                      );
                                                    }, 1000);

                                                    data.steps[
                                                      index
                                                    ].complete_status = true;
                                                    setCollapsed([
                                                      ...Collapsed,
                                                      index,
                                                    ]);
                                                    MarkAsDoneRequest(
                                                      steps.id,
                                                      index,
                                                      RunnerPercentageValue.current
                                                    );
                                                    setCompletedLatest(
                                                      (e) => e + 1
                                                    );
                                                    sessionStorage.setItem(
                                                      "dashboard",
                                                      true
                                                    );
                                                  }}
                                                >
                                                  <i className="bi bi-check-lg"></i>
                                                  {MarkasDone}
                                                </div>
                                              ) : (
                                                <div className="disabled_btn mx-3">
                                                  <i className="bi bi-check-lg"></i>
                                                  {MarkasDone}
                                                </div>
                                              )}
                                            </React.Fragment>
                                          ) : (
                                            <React.Fragment>
                                              <div
                                                className="btn btn-sm btn-border-blue btn-proceed mx-3"
                                                style={{ marginBottom: "0px" }}
                                                onClick={() => {
                                                  data.steps[
                                                    index
                                                  ].complete_status = true;
                                                  setCollapsed([
                                                    ...Collapsed,
                                                    index,
                                                  ]);
                                                  MarkAsDoneRequest(
                                                    steps.id,
                                                    index,
                                                    RunnerPercentageValue.current
                                                  );
                                                  setCompletedLatest(
                                                    (e) => e + 1
                                                  );
                                                  sessionStorage.setItem(
                                                    "dashboard",
                                                    true
                                                  );
                                                  setTimeout(() => {
                                                    StepScrolling(
                                                      `step_scroll_${InputIndex}_${
                                                        index + 1
                                                      }`
                                                    );
                                                  }, 1000);
                                                }}
                                              >
                                                <i className="bi bi-check-lg"></i>
                                                {MarkasDone}
                                              </div>
                                            </React.Fragment>
                                          )
                                        ) : (
                                          <div className="disabled_btn mx-3">
                                            <i className="bi bi-check-lg"></i>
                                            {MarkasDone}
                                          </div>
                                        )
                                      ) : Phase == InputIndex ? (
                                        <div
                                          className="btn btn-sm btn-border-blue btn-proceed mx-3"
                                          style={{ marginBottom: "0px" }}
                                          onClick={() => {
                                            setTimeout(() => {
                                              StepScrolling(
                                                `step_scroll_${InputIndex}_${
                                                  index + 1
                                                }`
                                              );
                                            }, 1000);

                                            MarkAsDoneRequest(
                                              steps.id,
                                              index,
                                              RunnerPercentageValue.current
                                            );
                                            data.steps[
                                              index
                                            ].complete_status = true;
                                            setCollapsed([...Collapsed, index]);

                                            setCompletedLatest((e) => e + 1);
                                            sessionStorage.setItem(
                                              "dashboard",
                                              true
                                            );
                                          }}
                                        >
                                          <i className="bi bi-check-lg"></i>
                                          {MarkasDone}
                                        </div>
                                      ) : (
                                        <div className="disabled_btn mx-3">
                                          <i className="bi bi-check-lg"></i>
                                          {MarkasDone}
                                        </div>
                                      )}
                                      {Phase > InputIndex && (
                                        <div className="disabled_btn mx-3">
                                          <i className="bi bi-check-lg"></i>
                                          {MarkasDone}
                                        </div>
                                      )}
                                      {CompletedLatest >= index + 2 && (
                                        <div className="disabled_btn">
                                          <i className="bi bi-check-lg"></i>
                                          {Editdata}
                                        </div>
                                      )}
                                    </React.Fragment>
                                  )}
                                </div>
                              </div>
                            </div>
                          )}
                        </div>
                      )}
                    </Element>
                  </React.Fragment>
                );
              })}
              <Survey
                display={
                  ReadinessData.phase[InputIndex].rating == null
                    ? "block"
                    : "none"
                }
                Completed={ReadinessData.phase[InputIndex].complete_status}
                Rating_obj={ReadinessData.phase[InputIndex].rating}
                phaseID={data.id}
                phase={data.title}
                token={JWTDecoded.auth_token}
                submitted={
                  ReadinessData.phase[InputIndex].rating == null ? false : true
                }
                PhaseNos={InputIndex}
                MigID={ReadinessData.migration_data_id}
              />
            </div>
            <div
              className="col-12 col-md-3 cass sticky-scroll"
              style={{
                height: "max-content",
              }}
            >
              {access.enableTopMigration &&
                ReadinessData.phase[InputIndex].searchText.apeople !== "" &&
                ReadinessData.phase[InputIndex].searchText.knowledge_base !==
                  "" && (
                  <Apeople
                    string="hello"
                    Search={ReadinessData.phase[InputIndex].searchText}
                  />
                )}

              <div
                className="col-12 text-center px-3"
                onClick={() => setApeopleLink((e) => !e)}
              >
                <div className="btn col-12 btn-border-blue mt-3 width_100">
                  Need More Help?
                </div>

                {ApeopleLink && (
                  <div className="apeople_help_parent">
                    <div className="menu-content-data">
                      {ParentData.field_quick_links.map((data, index) => {
                        return (
                          <div className="menu-content-item" key={index}>
                            <a
                              href={data.uri}
                              target="_blank"
                              rel="noreferrer"
                              className="menu-content-item-title"
                            >
                              {data.title}
                            </a>
                          </div>
                        );
                      })}
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      );
    }

    return (
      <React.Fragment>
        <div className="MigrationSteps">
          <MigrationHead />
          <MigrationSteps />
        </div>
        <div>{LicenseForm !== "none" && <MigrationLicenseForm />}</div>
      </React.Fragment>
    );
  }

  
  var Element = Scroll.Element;
  return (
    <React.Fragment>
      {Passed ? (
        <React.Fragment>
          <Head>
            <link
              rel="stylesheet"
              href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css"
            />

            <script
              src="https://fast.wistia.com/assets/external/E-v1.js"
              async
            ></script>
            {/* <title>Upgrade Launchpad - migration process</title> */}
          </Head>
          {ParentData !== undefined && (
            <Header
              title={ParentData.title[0].value}
              Links={ParentData.field_quick_links}
              LoginLabel={ParentData.field_login_text_label[0].value}
              LinkLabel={ParentData.field_quick_link_label[0].value}
              User={Username}
            />
          )}

          <style jsx global>{`
            header {
              position: fixed;
            }
            section {
              position: relative;
              top: 140px;
              margin-bottom: 90px;
            }
          `}</style>
          <div className={`migration_progress  px-2`}>
            {CompletedPhases !== undefined && (
              <ReadinessProgressBar
                access={access}
                ReadinessData={ReadinessData}
                Phase={CompletedPhases.length}
                Projects={Projects}
                ProgressData={ProgressData}
                UserLicenseData={UserLicenseData.current}
                ProjectRouting={(
                  projectId,
                  projectName,
                  notify_me,
                  notify_for_cloud
                ) =>
                  ProjectRouting(
                    projectId,
                    projectName,
                    notify_me,
                    notify_for_cloud
                  )
                }
              />
            )}
          </div>
          <section>
            <div className="container">
              <div className="migration_step_wrapper">
                <div className="migration_item-container">
                  {ReadinessData.phase !== undefined && (
                    <React.Fragment>
                      {/* {ReadinessData.phase.map((data,index) => {
                        return(
                          <PhaseComp
                          data={ReadinessData.phase[index]}
                          InputIndex={index}
                          Phase={ProgressData}
                          ToDo={ParentData.field_to_do_label[0].value}
                          Completed={ParentData.field_completed_label[0].value}
                          Editdata={
                            ParentData.field_edit_data_label_for_steps[0].value
                          }
                          Addlabel={
                            ParentData.field_additional_resource_text[0].value
                          }
                          MarkasDone={ParentData.field_mark_as_read_text[0].value}
                          MigrationID={ReadinessData.migration_data_id}
                          LicenseData={LicenseFormData.current}
                          UserLicenseData={UserLicenseData.current}
                        />
                        )
                      })} */}
                      <PhaseComp
                        data={ReadinessData.phase[0]}
                        InputIndex={0}
                        Phase={ProgressData}
                        ToDo={ParentData.field_to_do_label[0].value}
                        Completed={ParentData.field_completed_label[0].value}
                        Editdata={
                          ParentData.field_edit_data_label_for_steps[0].value
                        }
                        Addlabel={
                          ParentData.field_additional_resource_text[0].value
                        }
                        MarkasDone={ParentData.field_mark_as_read_text[0].value}
                        MigrationID={ReadinessData.migration_data_id}
                        LicenseData={LicenseFormData.current}
                        UserLicenseData={UserLicenseData.current}
                      />

                      <PhaseComp
                        data={ReadinessData.phase[1]}
                        InputIndex={1}
                        Phase={ProgressData}
                        ToDo={ParentData.field_to_do_label[0].value}
                        Completed={ParentData.field_completed_label[0].value}
                        Editdata={
                          ParentData.field_edit_data_label_for_steps[0].value
                        }
                        Addlabel={
                          ParentData.field_additional_resource_text[0].value
                        }
                        MarkasDone={ParentData.field_mark_as_read_text[0].value}
                        MigrationID={ReadinessData.migration_data_id}
                        LicenseData={LicenseFormData.current}
                        UserLicenseData={UserLicenseData.current}
                      />

                      <PhaseComp
                        data={ReadinessData.phase[2]}
                        InputIndex={2}
                        Phase={ProgressData}
                        ToDo={ParentData.field_to_do_label[0].value}
                        Completed={ParentData.field_completed_label[0].value}
                        Editdata={
                          ParentData.field_edit_data_label_for_steps[0].value
                        }
                        Addlabel={
                          ParentData.field_additional_resource_text[0].value
                        }
                        MarkasDone={ParentData.field_mark_as_read_text[0].value}
                        MigrationID={ReadinessData.migration_data_id}
                        LicenseData={LicenseFormData.current}
                        UserLicenseData={UserLicenseData.current}
                      />
                      {ReadinessData.phase.length == 5 && (
                        <PhaseComp
                          data={ReadinessData.phase[3]}
                          InputIndex={3}
                          Phase={ProgressData}
                          ToDo={ParentData.field_to_do_label[0].value}
                          Completed={ParentData.field_completed_label[0].value}
                          Editdata={
                            ParentData.field_edit_data_label_for_steps[0].value
                          }
                          Addlabel={
                            ParentData.field_additional_resource_text[0].value
                          }
                          MarkasDone={
                            ParentData.field_mark_as_read_text[0].value
                          }
                          MigrationID={ReadinessData.migration_data_id}
                          LicenseData={LicenseFormData.current}
                          UserLicenseData={UserLicenseData.current}
                        />
                      )}
                    </React.Fragment>
                  )}
                </div>
              </div>
            </div>
            <Element name="tester">
              {ProgressData == ReadinessData.phase.length - 1 && (
                <Success
                  data={ParentData.field_congratulation_message[0].value}
                />
              )}
            </Element>
          </section>
          {Video && (
            <div className="youtube_expanded_view">
              <div className="video_root">
                <YouTube
                  videoId={VideoURL}
                  opts={{
                    height: (82 * window.innerHeight) / 100,
                    width: (80 * window.innerWidth) / 100,
                    playerVars: {
                      autoplay: 1,
                    },
                  }}
                />
              </div>
            </div>
          )}

          <Footer data={ParentData.field_footer_section[0].value} />
        </React.Fragment>
      ) : (
        <div className="loader"></div>
      )}
    </React.Fragment>
  );
}

export default Readiness;
