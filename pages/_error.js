import react,{useState,useEffect} from "react"
import Router from "next/router";
import { THANKYOU_PUSH_URL } from "../configuration";

function Error({ statusCode }) {
    const [Log, setLog] = useState(false)
    useEffect(() => {
      if(sessionStorage.getItem("system") == null){
          setLog(false)
      }else{
          setLog(true)
      }
    }, [])
    
    return (
        <react.Fragment>
        <header className="error">
    <div className="container">
        <div className="row">
            <div className="col-8 col-sm-6 left-align-div py-2">
                <span className="logo"> <img src="/images/logo-aa.svg" /></span>
                
            </div>

            
        </div>
    </div>
</header>

<section className="ws_height" style={{
marginTop:"51px"
}}>

    <div className="container px-4">
        <div className="text-center">
            <h1 className="fw-light"><span className="fc-yellow">500</span> Server error </h1>
            <p className="fs-5 fw-normal fc-dark-grey">Try to refresh this page</p>
            <button onClick={() => {
                        if(Log){
                            sessionStorage.removeItem("system")
                            Router.push(THANKYOU_PUSH_URL)
                        }else{
                            Router.push(THANKYOU_PUSH_URL)
                        }
                    }} style={{
                 backgroundColor: "#FF4A00",
                 color: "#ffffff",
            }} className="btn bg_theme active py-2 mt-3 px-3" role="button" aria-pressed="true" title="login migration success kit">Return to home</button>
            <figure><img src="/images/404.jpg" className="img-responsive" /></figure>
        </div>
           
        

    </div>
</section>





<footer className="bg-light py-4 text-center fc-dark h6 footer error_page" style={{
position: "fixed",
width: "100%",
bottom: "0",
}}>
    <div className="container">
        <div className="col-12">
        <span>© 2021 Automation Anywhere, Inc. | <a href="https://www.automationanywhere.com/privacy"> Privacy</a> | <a href="https://www.automationanywhere.com/terms">Terms</a> | <a href="https://www.automationanywhere.com/trademark" title="Trademark">Trademark</a> | <a href="https://www.automationanywhere.com/products/automation-360" title="Products">Products</a> | <a href="https://www.automationanywhere.com/company/careers" title="carrers">Careers</a></span>

        </div>

    </div>

</footer>
    </react.Fragment>
    )
  }
  
  Error.getInitialProps = ({ res, err }) => {
    const statusCode = res ? res.statusCode : err ? err.statusCode : 404
    return { statusCode }
  }
  
  export default Error