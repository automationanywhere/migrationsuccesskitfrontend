﻿import Head from "next/head";
import React, { useState, useEffect, useRef } from "react";
import Header from "../Components/Header/Header";
import axios from "axios";
import Footer from "../Components/Footer/Footer";
import Router from "next/router";
import jwt_decode from "jwt-decode";
import { API_ENDPOINT_URL } from "../configuration";
import * as Scroll from "react-scroll";
import {
  Link,
  Button,
  Element,
  Events,
  animateScroll as scroll,
  scrollSpy,
  scroller,
} from "react-scroll";

export async function getStaticProps(context) {
  const opts = {
    method: "GET",
    headers: {
      Accept: "application/vnd.api+json",
      Authorization: `Basic ZGVlcGFrLnNpbmdoOkRlZXBha0AxOTkx`,
    },
  };

  const res = await fetch(API_ENDPOINT_URL + "get-global-constant", opts).then(
    (res) => res.json()
  );

  const access = await fetch(
    API_ENDPOINT_URL + "get-site-settings?_format=json",
    opts
  ).then((res) => res.json());

  const MigrationVersion = await fetch(
    API_ENDPOINT_URL + "v1/get-migration-version?_format=json",
    opts
  ).then((res) => res.json());

  const IQMigrationVersion = await fetch(
    API_ENDPOINT_URL + "v1/bot-migration-versions?_format=json",
    opts
  ).then((res) => res.json());

  const MigrationMultipleVersion = await fetch(
    API_ENDPOINT_URL + `get-paragraph/1?_format=json`,
    opts
  ).then((res) => res.json());

  const MigrationNoMigration = await fetch(
    API_ENDPOINT_URL + `get-paragraph/2?_format=json`,
    opts
  ).then((res) => res.json());

  const MigrationSingleVersion = await fetch(
    API_ENDPOINT_URL + `get-paragraph/3?_format=json`,
    opts
  ).then((res) => res.json());

  return {
    props: {
      res,
      access,
      MigrationVersion,
      MigrationMultipleVersion,
      MigrationNoMigration,
      MigrationSingleVersion,
      IQMigrationVersion,
    },

    revalidate: 1,
  };
}

function Migration({
  res,
  access,
  MigrationVersion,
  MigrationMultipleVersion,
  MigrationNoMigration,
  MigrationSingleVersion,
  responseOBJ,
  IQMigrationVersion,
}) {
  const [ParentData, setParentData] = useState([]);
  const [HeaderData, setHeaderData] = useState({});
  const [VersionList, setVersionList] = useState([]);
  const [VersionData, setVersionData] = useState(["OPO", "NOA", "MOA"]);
  const [ColorActive, setColorActive] = useState("");
  const [DropDown, setDropDown] = useState("none");
  const [IQBOT, setIQBOT] = useState(false);
  const [Version, setVersion] = useState("");
  const [FooterBool, setFooterBool] = useState(false);
  const [IQVersions, setIQVersions] = useState([]);
  const [IQCompleted, setIQCompleted] = useState(null)
  const [CurrentIQVersion, setCurrentIQVersion] = useState(
    "please select your IQ bot version"
  );
  const [IQerror, setIQerror] = useState(false)
  const [IQdropdown, setIQdropdown] = useState("none");
  const [Vscenarios1, setVscenarios1] = useState([]);
  const [Vscenarios2, setVscenarios2] = useState([]);
  const [Vscenarios3, setVscenarios3] = useState([]);
  const [VersionText, setVersionText] = useState("");
  const [EditMode, setEditMode] = useState(false);
  const [Username, setUsername] = useState("");
  const [Passed, setPassed] = useState(false);
  const [PremiseCheck, setPremiseCheck] = useState(false);
  const [UserCurrentData, setUserCurrentData] = useState([]);
  const [EditWarning, setEditWarning] = useState(false);
  const [ThankYouMessage, setThankYouMessage] = useState("");
  const [JWTDecoded, setJWTDecoded] = useState({});
  const [ProjectWarning, setProjectWarning] = useState(false);
  const [Loader, setLoader] = useState(false);
  const [Project, setProject] = useState(false);
  const [ProjectData, setProjectData] = useState([]);
  const [AddNewProject, setAddNewProject] = useState(true);
  const [AddProjectWarning, setAddProjectWarning] = useState(false);
  const [UID, setUID] = useState("");
  const sign = require("jwt-encode");
  const secret = "secret";

  let icons = {
    video: "icon-video",
    document: "icon-doc",
    Documentation: "icon-doc",
    download: "icon-download",
    blog: "bi bi-file-earmark-richtext",
    link: "icon-link",
    globe: "icon-globe",
  };

  function VersionPostData(params) {
    (function () {
      setLoader(true);
      sessionStorage.setItem("dashboard", true);
      sessionStorage.removeItem("survey");
    })();
    axios
      .post(
        API_ENDPOINT_URL + "v1/save-migration?_format=json",
        UserData.current,
        {
          headers: {
            Accept: "application/vnd.api+json",
            Authorization: `Bearer ${JWTDecoded.auth_token}`,
          },
        }
      )
      .catch(() => Router.push("/api-failed"))

      .then((e) => {
        if (e.data.message !== undefined) {
          sessionStorage.removeItem("system");
          location.replace(
            API_ENDPOINT_URL +
            "samllogin/?init=https://devmsk.aaiengineering.com/"
          );
        }
        if (e.data.projectExist == false) {
          if (UserData.current.notify_for_cloud == true) {
            let system = {
              auth_root: "thankyou",
            };
            const jwt = sign(system, secret);
            sessionStorage.setItem("url_root", jwt);
            Router.push("/thankyou");
          } else {
            let system = {
              auth_root: "readiness",
            };
            const jwt = sign(system, secret);
            sessionStorage.setItem("url_root", jwt);
            sessionStorage.setItem("project_arr", e.data.projectId);
            sessionStorage.setItem(
              "project_name",
              UserData.current.project_name
            );
            if (sessionStorage.getItem("restricted") !== null) {
              sessionStorage.removeItem("restricted");
            }
            Router.push("/readiness");
          }
        } else {
          setLoader(false);
          setProjectWarning(true);
          scroll.scrollToTop();
        }
      })
      .then((e) => {
        axios({
          url: API_ENDPOINT_URL + `get-projects?_format=json`,
          method: "get",
          headers: {
            Accept: "application/vnd.api+json",
            Authorization: `Bearer ${JWTDecoded.auth_token}`,
          },
        })
          .catch(() => Router.push("/api-failed"))

          .then((response) => {
            if (response.data.message !== undefined) {
              sessionStorage.removeItem("system");
              location.replace(
                API_ENDPOINT_URL +
                "samllogin/?init=https://devmsk.aaiengineering.com/"
              );
            }
            let project = { data: response.data };
            const project_token = sign(project, secret);
            sessionStorage.setItem("projects", project_token);
          });
      });
  }
  const UserData = useRef({
    control_room_version: 0,
    migration_id: 0,
    notify_me: false,
    notify_for_cloud: false,
    company: 6,
    uid: UID,
    project_name: "",
    iq_bot_migration: false,
    iq_bot_migration_version: null
  });

  const ResetUserData = () =>
  (UserData.current = {
    control_room_version: 0,
    migration_id: 0,
    notify_me: false,
    notify_for_cloud: false,
    company: 6,
    project_name: UserData.current.project_name,
  });

  const FilteredVersion = VersionList.filter((e) => {
    if (VersionText.length !== 0) {
      return e.name.includes(VersionText.toString());
    } else {
      return e;
    }
  });
  const DisabledBtnStyle = {
    opacity: "0.6",
    cursor: "not-allowed",
  };
  var ls = require("local-storage");
  console.log(IQMigrationVersion);
  useEffect(() => {
    if (sessionStorage.getItem("system") !== null) {
      var decoded = jwt_decode(sessionStorage.getItem("system"));
      var projects = jwt_decode(sessionStorage.getItem("projects"));
      var url_root = jwt_decode(sessionStorage.getItem("url_root"));

      setAddNewProject(access.addProjectPermission);
      setJWTDecoded(decoded);
      setProjectData(projects.data);

      function ParentFunc(params) {
        setParentData(res[0]);
        setVersion(res[0].field_version_placeholder_label[0].value);
        setVersionList(MigrationVersion);
        setVscenarios1(MigrationMultipleVersion);
        setVscenarios2(MigrationNoMigration);
        setVscenarios3(MigrationSingleVersion);
        setPassed(true);
        document.addEventListener("click", (e) => {
          if (e.target.className !== "selected_data textbox") {
            if (e.target.className !== "col-12 search-box") {
              setDropDown("none");
              setIQdropdown("none");
            }
          }
        });
        if (decoded.user == null) {
          Router.push("/");
        } else {
          setUsername(decoded.first_name);
        }
        if (sessionStorage.getItem("edit") !== null) {
          setEditMode(true);
          let arr = sessionStorage.getItem("current_data").split(",");
          setVersion(arr[0]);
          setColorActive(arr[1]);
          UserData.current.project_name = arr[4];
          axios({
            url:
              API_ENDPOINT_URL +
              `v1/get-allowed-migration/${arr[2]}?_format=json`,
            method: "get",
            headers: {
              Accept: "application/vnd.api+json",
              Authorization: `Bearer ${decoded.auth_token}`,
            },
          })
            .catch(() => Router.push("/api-failed"))

            .then((e) => {
              if (e.data.message !== undefined) {
                sessionStorage.removeItem("system");
                location.replace(
                  API_ENDPOINT_URL +
                  "samllogin/?init=https://devmsk.aaiengineering.com/"
                );
              }
              setVersionData(e.data);
              setUserCurrentData(e.data);

              if (e.data.length == 1) {
                setPremiseCheck(0);
              }
            });
        }
      }
      if (sessionStorage.getItem("restricted") == null) {
        if (url_root.auth_root == "migration") {
          ParentFunc();
        } else {
          if (typeof url_root.auth_root == "object") {
            Router.push("/");
          } else {
            Router.push(`/${url_root.auth_root}`);
          }
        }
      } else {
        ParentFunc();
      }
    }
  }, []);
  useEffect(() => {
    setUID(JWTDecoded.UID);
  }, [UserData.current]);

  function OnPremisesOnly() {
    const [Button, setButton] = useState(false);
    const [LocalPremiseValue, setLocalPremiseValue] = useState(null);
    useEffect(() => {
      if (PremiseCheck !== false) {
        setLocalPremiseValue(PremiseCheck);
      }
    }, []);

    return (
      <React.Fragment>
        <div className="r_crv mb-3">
          <div
            className="info-wrapper  px-4 py-4"
            dangerouslySetInnerHTML={{
              __html: Vscenarios3[0].field_on_premise_message[0].value,
            }}
          ></div>
          <div>
            <label className="fs-5 mt-5 mb-3">
              {Vscenarios3[0].field_migration_option_label[0].value}
            </label>
            {Vscenarios3[0].field_migration_options_to_show.map((e, index) => {
              return (
                <div className="form-group top-align-div mb-2" key={e}>
                  <span>
                    {EditMode ? (
                      <input
                        type="radio"
                        name="premises"
                        className="crv_custom_radio"
                        checked={EditMode && LocalPremiseValue == index && true}
                        onClick={() => {
                          setButton(true);
                          if (index == 1) {
                            ResetUserData();

                            setLocalPremiseValue(1);

                            let VersionFilter = VersionList.filter(
                              (data) => data.name == Version
                            );
                            var ProcessData = UserData.current;
                            ProcessData.notify_for_cloud = true;
                            ProcessData.control_room_version = parseInt(
                              VersionFilter[0].id
                            );

                            UserData.current = ProcessData;
                          } else {
                            ResetUserData();

                            setLocalPremiseValue(0);

                            let VersionFilter = VersionList.filter(
                              (data) => data.name == Version
                            );
                            var ProcessData = UserData.current;
                            (ProcessData.migration_id = parseInt(
                              VersionData[0].id
                            )),
                              (ProcessData.control_room_version = parseInt(
                                VersionFilter[0].id
                              ));
                            UserData.current = ProcessData;
                          }
                        }}
                      />
                    ) : (
                      <input
                        type="radio"
                        name="premises"
                        className="crv_custom_radio"
                        onClick={() => {
                          setButton(true);
                          if (index == 1) {
                            ResetUserData();

                            setLocalPremiseValue(1);

                            let VersionFilter = VersionList.filter(
                              (data) => data.name == Version
                            );
                            var ProcessData = UserData.current;
                            ProcessData.notify_for_cloud = true;
                            ProcessData.control_room_version = parseInt(
                              VersionFilter[0].id
                            );

                            UserData.current = ProcessData;
                          } else {
                            ResetUserData();

                            setLocalPremiseValue(0);

                            let VersionFilter = VersionList.filter(
                              (data) => data.name == Version
                            );
                            var ProcessData = UserData.current;
                            (ProcessData.migration_id = parseInt(
                              VersionData[0].id
                            )),
                              (ProcessData.control_room_version = parseInt(
                                VersionFilter[0].id
                              ));
                            UserData.current = ProcessData;
                          }
                        }}
                      />
                    )}
                  </span>

                  <div className="input-label px-3">
                    <span className="h6">{e.value} </span>
                    {index == 0 && (
                      <p className="option_sub">{VersionData[0].description}</p>
                    )}
                  </div>
                  <br />
                </div>
              );
            })}
          </div>
        </div>
        <div className="mt-2 helpful_data">
          {Vscenarios3[0].field_helpful_links.map((e) => {
            return (
              <React.Fragment key={e.title}>
                <a
                  className="top-align-div"
                  href={e.uri}
                  target="_blank"
                  rel="noreferrer"
                >
                  <i className={icons[e.description]}></i>
                  <span className="text-decoration-underline px-2">
                    {e.title}
                  </span>
                </a>
              </React.Fragment>
            );
          })}
        </div>

        <React.Fragment>
          {Button ? (
            <React.Fragment>
              <div
                className="btn mt-5 bg_primary_blue adjusted_btn"
                onClick={() => {
                  if (AddNewProject) {
                    if (EditMode) {
                      setEditWarning(true);
                    } else {
                      VersionPostData();
                    }
                  } else {
                    setAddProjectWarning(true);
                  }
                }}
              >
                {!EditMode ? ParentData.field_continue[0].value : "Submit"}
              </div>
              {AddProjectWarning && (
                <p
                  className="sm-content"
                  style={{
                    color: "red",
                    marginTop: "18px",
                  }}
                >
                  Start new project has been disabled , please contact your
                  administrator
                </p>
              )}
            </React.Fragment>
          ) : (
            <React.Fragment>
              <div
                className="disabled_btn adjusted_btn "
                style={{
                  height: "max-content",
                  position: "relative",
                  top: "2rem",
                }}
              >
                {!EditMode ? ParentData.field_continue[0].value : "Submit"}
              </div>
            </React.Fragment>
          )}
          {EditMode && (
            <div
              className="btn btn-border-blue btn-sm"
              data-bs-toggle="modal"
              style={{
                height: "max-content",
                marginLeft: "15px",
                marginTop: "3rem",
              }}
              data-bs-target="#on_premises"
              data-bs-whatever="@on_premises"
              onClick={() => {
                let system = {
                  auth_root: "readiness",
                };
                const jwt = sign(system, secret);
                sessionStorage.setItem("url_root", jwt);
                Router.push("/readiness");
                sessionStorage.removeItem("edit");
              }}
            >
              Cancel
            </div>
          )}
        </React.Fragment>
      </React.Fragment>
    );
  }
  function NoOptionAvailable(params) {
    return (
      <React.Fragment>
        <div className="r_crv mb-5">
          <div
            className="info-wrapper px-4 py-4"
            data-result-category="crv"
            data-result-for="error"
            dangerouslySetInnerHTML={{
              __html: Vscenarios2[0].field_no_migration_message[0].value,
            }}
          ></div>
        </div>
        <div className="mt-2 helpful_data">
          {Vscenarios2[0].field_helpful_links.map((e) => {
            return (
              <React.Fragment key={e.title}>
                <a
                  className="top-align-div"
                  href={e.uri}
                  target="_blank"
                  rel="noreferrer"
                >
                  <i className={icons[e.description]}></i>
                  <span className="text-decoration-underline px-2">
                    {e.title}
                  </span>
                </a>
              </React.Fragment>
            );
          })}
        </div>
        <p className="h6 mt-5">
          Click here to be notified when your Control Room version is ready to
          migrate:{" "}
        </p>
        <div
          className="btn mt-5 bg_primary_blue adjusted_btn"
          onClick={() => {
            if (AddNewProject) {
              if (EditMode) {
                setEditWarning(true);
                UserData.current.notify_me = true;
              } else {
                ResetUserData();
                let VersionFilter = VersionList.filter(
                  (data) => data.name == Version
                );

                var ProcessData = UserData.current;
                ProcessData.notify_me = true;
                ProcessData.control_room_version = parseInt(
                  VersionFilter[0].id
                );
                // ProcessData.migration_id = parseInt(VersionData[0].id);
                UserData.current = ProcessData;

                axios
                  .post(
                    API_ENDPOINT_URL + "v1/save-migration?_format=json",
                    ProcessData,
                    {
                      headers: {
                        Accept: "application/vnd.api+json",
                        Authorization: `Bearer ${JWTDecoded.auth_token}`,
                      },
                    }
                  )
                  .catch(() => Router.push("/api-failed"))

                  .then((e) => {
                    if (e.data.message !== undefined) {
                      sessionStorage.removeItem("system");
                      location.replace(
                        API_ENDPOINT_URL +
                        "samllogin/?init=https://devmsk.aaiengineering.com/"
                      );
                    }
                    let system = {
                      auth_root: "thankyou",
                    };
                    const jwt = sign(system, secret);
                    sessionStorage.setItem("url_root", jwt);
                    Router.push("/thankyou");
                  })

                  .then((e) => {
                    Router.push("/thankyou");
                  });
                setThankYouMessage("<p>thank you for your response</p>");
              }
            } else {
              setAddProjectWarning(true);
            }
          }}
        >
          {Vscenarios2[0].field_notify_me_label[0].value}
        </div>
        {EditMode && (
          <div
            className="btn btn-border-blue btn-sm"
            data-bs-toggle="modal"
            style={{
              height: "max-content",
              marginLeft: "15px",
              marginTop: "3rem",
            }}
            data-bs-target="#on_premises"
            data-bs-whatever="@on_premises"
            onClick={() => {
              let system = {
                auth_root: "readiness",
              };
              const jwt = sign(system, secret);
              sessionStorage.setItem("url_root", jwt);
              Router.push("/readiness");
              sessionStorage.removeItem("edit");
            }}
          >
            Cancel
          </div>
        )}
        {AddProjectWarning && (
          <p
            className="sm-content"
            style={{
              color: "red",
              marginTop: "18px",
            }}
          >
            Start new project has been disabled , please contact your
            administrator
          </p>
        )}
      </React.Fragment>
    );
  }

  const VersionDataMap = VersionData.map((e) => {
    return (
      <React.Fragment key={e.id}>
        {VersionData.length == 2 && (
          <React.Fragment>
            <div
              className={`
							col-12 col-sm-6 col-lg-5 col-xl-4 col-xxl-3 aa_type py-4 migrate_type aa_type  py-4 migrate_type
                    ${e.name == ColorActive && " active "}
                    `}
              data-version="1"
              key={e.id}
              onClick={() => {
                setColorActive(e.name);
                ResetUserData();
                let VersionFilter = VersionList.filter(
                  (data) => data.name == Version
                );
                var ProcessData = UserData.current;
                (ProcessData.control_room_version = parseInt(
                  VersionFilter[0].id
                )),
                  (ProcessData.migration_id = parseInt(e.id));
                UserData.current = ProcessData;
              }}
            >
              <div className="card mg-card text-center px-4 py-4">
                <div className="version_icon">
                  {e.name.slice(0, 1).toLowerCase() == "c" ? (
                    <i className="bi bi-cloud"></i>
                  ) : (
                    <i className="bi bi-laptop"></i>
                  )}
                </div>
                <p className="card-title h5">{e.name}</p>
                <div className="card-body">{e.description}</div>
                {e.recommended == "True" && (
                  <p className="recom_badge">
                    {ParentData.field_recommended_badge_text[0].value}
                  </p>
                )}
              </div>
            </div>
          </React.Fragment>
        )}
        {VersionData.length == 1 && e.name !== "" && <OnPremisesOnly />}
      </React.Fragment>
    );
  });

  return (
    <React.Fragment>
      {Passed ? (
        <React.Fragment>
          <Head>
            {FooterBool && (
              <link
                rel="stylesheet"
                href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css"
              />
            )}
            {/* <title>Upgrade Launchpad - start new journey</title> */}
          </Head>
          <Header
            title={ParentData.title[0].value}
            Links={ParentData.field_quick_links}
            LoginLabel={ParentData.field_login_text_label[0].value}
            LinkLabel={ParentData.field_quick_link_label[0].value}
            User={Username}
          />
          {Loader && (
            <div className="loader_parent">
              <div className="loader"></div>{" "}
              <style jsx global>{`
                body {
                  overflow: hidden;
                }
              `}</style>
            </div>
          )}

          <section>
            <div className="container-fluid pd-b px-4">
              <div className="row" style={{ marginTop: "65px" }}>
                <div className="col-12 col-md-5 col-lg-5 col-xl-4">
                  {!EditMode ? (
                    <div
                      className="full-height black_design text-white px-4 py-5 "
                      dangerouslySetInnerHTML={{
                        __html: ParentData.body[0].value.replace(
                          "John",
                          Username
                        ),
                      }}
                    ></div>
                  ) : (
                    <div
                      style={{ minHeight: "70vh" }}
                      className="full-height black_design text-white px-4 py-5 "
                      dangerouslySetInnerHTML={{
                        __html: `<h1 className="fw-light">Choose <br> Migration <br>Preference!</h1>

<p className="mt-sm-4 content text-white">Let's get you setup - it only takes a moment. This will help us give you curated information for your migration journey.</p>`,
                      }}
                    ></div>
                  )}
                </div>

                <div className="col-12 col-md-7 col-lg-7 col-xl-8 mb-4">
                  <div className="mg_ass_form">
                    <div className="col-sm-12">
                      <div className="col-sm-11 margin-auto">
                        <div className="mg_ass_selection">
                          <div className="col-sm-12 spacebetween-align-div py-4">
                            <h2 className=" fw-light fc-theme-dark">
                              {ParentData.field_get_started_head_lable[0].value}
                            </h2>
                          </div>
                          {!EditMode ? (
                            <div className="form-group py-3">
                              <label className="fs-5">
                                What do you want to name your project?
                              </label>
                              <p className="sm-content">
                                If you have multiple Control Rooms, we recommend
                                naming a project, for example &quot;Finance Dev
                                Control Room&quot;
                              </p>
                              <input
                                type="text"
                                className="textbox col-12 col-sm-9 col-lg-8 col-xl-8"
                                placeholder="Please enter project name"
                                maxLength="50"
                                onChange={(e) => {
                                  UserData.current.project_name =
                                    e.target.value.trim();
                                  let filter = ProjectData.filter(
                                    (data) =>
                                      data.projectName.toLowerCase() ==
                                      e.target.value.toLowerCase().trim()
                                  );
                                  if (e.target.value.length == 0) {
                                    setProject(false);
                                  } else {
                                    if (filter.length !== 0) {
                                      setProjectWarning(true);
                                      setProject(false);
                                    } else {
                                      setProjectWarning(false);
                                      setProject(true);
                                    }
                                  }
                                }}
                              />
                              {ProjectWarning && EditMode == false && (
                                <p
                                  className="sm-content"
                                  style={{ color: "red", marginTop: "10px" }}
                                >
                                  {
                                    ParentData.field_project_exist_error[0]
                                      .value
                                  }
                                </p>
                              )}
                            </div>
                          ) : (
                            <div className="form-group py-3">
                              <label className="fs-5">
                                What do you want to name your project?
                              </label>
                              <p className="sm-content">
                                If you have multiple Control Rooms, we recommend
                                naming a project, for example &quot;Finance Dev
                                Control Room&quot;
                              </p>
                              <input
                                type="text"
                                className="textbox col-12 col-sm-9 col-lg-8 col-xl-8"
                                placeholder="Please enter project name"
                                maxLength="50"
                                style={{
                                  caretColor: EditMode && "transparent",
                                  outline: EditMode && "none",
                                }}
                                value={
                                  sessionStorage
                                    .getItem("current_data")
                                    .split(",")[4]
                                }
                                contentEditable={false}
                              />
                              {ProjectWarning && (
                                <p
                                  className="sm-content"
                                  style={{ color: "red", marginTop: "10px" }}
                                >
                                  {
                                    ParentData.field_project_exist_error[0]
                                      .value
                                  }
                                </p>
                              )}
                            </div>
                          )}

                          <div className="form-group py-3">
                            <label className="fs-5">
                              {
                                ParentData.field_control_room_version_label[0]
                                  .value
                              }
                            </label>
                            <p className="sm-content">
                              {
                                ParentData.field_log_in_control_room_label[0]
                                  .value
                              }
                            </p>
                            <div
                              className="select-dropdown col-sm-12 col-lg-8 col-xl-8"
                              style={{ marginTop: "15px" }}
                            >
                              <span
                                className="selected_data textbox"
                                onClick={() => {
                                  {
                                    if (UserData.current.project_name !== "") {
                                      if (DropDown == "none") {
                                        setDropDown("block");
                                      } else {
                                        setDropDown("none");
                                      }
                                    }
                                  }
                                }}
                              >
                                {Version}
                              </span>
                              <div
                                className="select_dropdown_data"
                                style={{ display: DropDown }}
                              >
                                <div className="select-search">
                                  <input
                                    type="text"
                                    className="col-12 search-box"
                                    onChange={(e) =>
                                      setVersionText(e.target.value)
                                    }
                                  />
                                </div>
                                <ul className="select-dropdown-content">
                                  {FilteredVersion.map((e) => {
                                    return (
                                      <li
                                        key={e.name}
                                        onClick={() => {
                                          axios({
                                            url:
                                              API_ENDPOINT_URL +
                                              `v1/get-allowed-migration/${e.id}?_format=json`,
                                            method: "get",
                                            headers: {
                                              Accept:
                                                "application/vnd.api+json",
                                              Authorization: `Bearer ${JWTDecoded.auth_token}`,
                                            },
                                          }).then((e) => {
                                            if (e.data.message !== undefined) {
                                              sessionStorage.removeItem(
                                                "system"
                                              );
                                              location.replace(
                                                API_ENDPOINT_URL +
                                                "samllogin/?init=https://devmsk.aaiengineering.com/"
                                              );
                                            }

                                            setVersionData(e.data);
                                          });

                                          setFooterBool(true);
                                          setVersion(e.name);
                                          setColorActive("");
                                          setDropDown("none");
                                          ResetUserData();
                                          UserData.current
                                            .control_room_version ==
                                            parseInt(e.id);
                                        }}
                                      >
                                        {e.name}
                                      </li>
                                    );
                                  })}
                                </ul>
                              </div>
                            </div>
                          </div>

                          <div className="r_crv">
                            <div
                              className="form-group py-0 active"
                              data-result-category="crv"
                            >
                              {Version.length < 10 &&
                                VersionData.length == 2 && (
                                  <label className="fs-5">
                                    {
                                      ParentData
                                        .field_migration_type_question[0].value
                                    }
                                  </label>
                                )}
                              <div className="row">
                                {Version ==
                                  ParentData.field_version_placeholder_label[0]
                                    .value && (
                                    <div
                                      className="r_crv skeleton"
                                      style={{ opacity: "0.6" }}
                                    >
                                      <div
                                        className="form-group py-3 active"
                                        data-result-category="crv"
                                      >
                                        <label className="fs-5">
                                          What Automation 360 deployment would you
                                          like to migrate to?
                                        </label>
                                        <div className="row">
                                          <div
                                            className="col-12 col-sm-6 col-lg-5 col-xl-4 col-xxl-3 aa_type py-4 migrate_type aa_type  py-4 migrate_type"
                                            data-version="1"
                                          >
                                            <div className="card mg-card text-center px-4 py-4">
                                              <div className="version_icon">
                                                <i className="bi bi-cloud"></i>
                                              </div>
                                              <p className="card-title h5">
                                                Cloud
                                              </p>
                                              <div className="card-body">
                                                Hosted and managed in the
                                                Automation Anywhere Enterprise
                                                Cloud.
                                              </div>
                                              <p className="recom_badge">
                                                Recommended
                                              </p>
                                            </div>
                                          </div>
                                          <div
                                            className="col-12 col-sm-6 col-lg-5 col-xl-4 col-xxl-3 aa_type py-4 migrate_type aa_type  py-4 migrate_type"
                                            data-version="2"
                                          >
                                            <div className="card mg-card text-center px-4 py-4">
                                              <div className="version_icon">
                                                <i className="bi bi-laptop"></i>
                                              </div>
                                              <p className="card-title h5">
                                                On-premises
                                              </p>
                                              <div className="card-body">
                                                Hosted on your infrastructure with
                                                optional updates from the
                                                Automation Anywhere Enterprise
                                                Cloud
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div className="helpful_data">
                                        <a
                                          className="top-align-div"
                                          href="https://www.automationanywhere.com/rpa/cloud-rpa"
                                          rel="noreferrer"
                                          target="_blank"
                                        >
                                          <i className="doc_default_black"></i>{" "}
                                          <span className="text-decoration-underline px-2">
                                            Learn about Cloud RPA{" "}
                                          </span>
                                        </a>
                                        <a
                                          className="top-align-div"
                                          href="https://www.youtube.com/watch?v=9DO1GRLXqEk"
                                          rel="noreferrer"
                                          target="_blank"
                                        >
                                          <i className="bi bi-youtube"></i>{" "}
                                          <span className="text-decoration-underline px-2">
                                            How to choose your Automation 360
                                            deployment model{" "}
                                          </span>
                                        </a>
                                        <a
                                          className="top-align-div"
                                          target="_blank"
                                          rel="noreferrer"
                                          href="https://docs.automationanywhere.com/bundle/enterprise-v2019/page/enterprise-cloud/topics/migration/choose-deployment-model.html"
                                        >
                                          <i className="doc_default_black"></i>{" "}
                                          <span className="text-decoration-underline px-2">
                                            Read about deployment options
                                          </span>
                                        </a>
                                      </div>
                                      <div className="form-group top-align-div mb-2" style={{
                                        marginTop: "30px"
                                      }}>
                                        <span>
                                          <input
                                            type="radio"
                                            name="premises"
                                            checked={false}
                                            className="crv_custom_radio" />
                                        </span>

                                        <div className="input-label px-3">
                                          <span className="h6">Confirm if IQ Bot Upgrade is planned Yes/No?</span>


                                        </div>
                                        <br />
                                      </div>
                                      <div className="disabled_btn mt-5">
                                        {!EditMode
                                          ? ParentData.field_continue[0].value
                                          : "Submit"}
                                      </div>


                                    </div>
                                  )}
                                {VersionData.length !== 0 ? (
                                  VersionDataMap
                                ) : (
                                  <NoOptionAvailable />
                                )}
                              </div>
                            </div>
                          </div>
                        </div>

                        {VersionData.length == 2 && (
                          <div className="mt-2 helpful_data">
                            {Vscenarios2[0].field_helpful_links.map((e) => {
                              return (
                                <React.Fragment key={e.title}>
                                  <a
                                    className="top-align-div"
                                    href={e.uri}
                                    target="_blank"
                                    rel="noreferrer"
                                  >
                                    <i className={icons[e.description]}></i>{" "}
                                    <span className="text-decoration-underline px-2">
                                      {e.title}
                                    </span>
                                  </a>
                                </React.Fragment>
                              );
                            })}
                          </div>
                        )}
                        {Version !==
                          ParentData.field_version_placeholder_label[0]
                            .value && VersionData.length !== 0 && <div className="form-group top-align-div mb-2" style={{ marginTop: "30px" }}>
                            <span>
                              <input
                                type="radio"
                                name="premises"
                                checked={IQBOT}
                                onClick={() => {
                                  setIQCompleted(false)
                                  setIQBOT((e) => !e)
                                  UserData.current.iq_bot_migration = !IQBOT
                                }}
                                className="crv_custom_radio" />
                            </span>

                            <div className="input-label px-3">
                              <span className="h6">Confirm if IQ Bot Upgrade is planned Yes/No?</span>


                            </div>
                            <br />
                          </div>}

                        {IQBOT && (
                          <div className="form-group py-3">
                            <label className="fs-5">
                              {
                                ParentData.field_control_room_version_label[0]
                                  .value
                              }
                            </label>
                            <p className="sm-content">
                              {
                                ParentData.field_log_in_control_room_label[0]
                                  .value
                              }
                            </p>
                            <div
                              className="select-dropdown col-sm-12 col-lg-8 col-xl-8"
                              style={{ marginTop: "15px" }}
                            >
                              <span
                                className="selected_data textbox"
                                onClick={() => {
                                  {
                                    if (UserData.current.project_name !== "") {
                                      if (DropDown == "none") {
                                        setIQdropdown("block");
                                      } else {
                                        setIQdropdown("none");
                                      }
                                    }
                                  }
                                }}
                              >
                                {CurrentIQVersion}
                              </span>
                              <div
                                className="select_dropdown_data"
                                style={{
                                  display: IQdropdown,
                                  borderColor: "#94949461",
                                  borderRadius: "0",
                                }}
                              >
                                <ul className="select-dropdown-content">
                                  {IQMigrationVersion.map((e) => {
                                    return (
                                      <li
                                        key={e.tid}
                                        onClick={() => {
                                          setCurrentIQVersion(e.name);
                                          setIQdropdown("none");
                                          if (e.is_bot_migration_allowed) {
                                            setIQCompleted(true)
                                            UserData.current.iq_bot_migration_version = e.tid
                                          } else {
                                            IQerror(true)
                                          }
                                        }}
                                      >
                                        {e.name}
                                      </li>
                                    );
                                  })}
                                </ul>
                              </div>
                            </div>
                          </div>
                        )}
                        {IQerror && <p className="sm-content" style={{
                          color: "red"
                        }}>version not supported</p>}
                        {EditMode == false && (
                          <React.Fragment>
                            {VersionData.length == 2 && (
                              <React.Fragment>
                                {ColorActive !== "" && Project && [null, true].includes(IQCompleted) ? (
                                  <React.Fragment>
                                    <div
                                      className="btn mt-5 bg_primary_blue"
                                      style={{
                                        height: "fit-content",
                                        marginTop: "3rem",
                                      }}
                                      onClick={() => {
                                        if (AddNewProject) {
                                          if (EditMode) {
                                            setEditWarning(true);
                                          } else {
                                            VersionPostData();
                                          }
                                        } else {
                                          setAddProjectWarning(true);
                                        }
                                      }}
                                    >
                                      {!EditMode
                                        ? ParentData.field_continue[0].value
                                        : "Submit"}
                                    </div>
                                    {AddProjectWarning && (
                                      <p
                                        className="sm-content"
                                        style={{
                                          color: "red",
                                          marginTop: "18px",
                                        }}
                                      >
                                        Start new project has been disabled ,
                                        please contact your administrator
                                      </p>
                                    )}
                                  </React.Fragment>
                                ) : (
                                  <React.Fragment>
                                    <div className="disabled_btn">
                                      {!EditMode
                                        ? ParentData.field_continue[0].value
                                        : "Submit"}
                                    </div>
                                  </React.Fragment>
                                )}
                              </React.Fragment>
                            )}
                          </React.Fragment>
                        )}

                        {[1, 0].includes(VersionData.length) == false && (
                          <React.Fragment>
                            {EditMode && (
                              <React.Fragment>
                                <div
                                  className="edit_flexed"
                                  style={{
                                    display: "flex",
                                    marginTop: "0px",
                                  }}
                                >
                                  {Version ==
                                    sessionStorage
                                      .getItem("current_data")
                                      .split(",")[0] ? (
                                    <React.Fragment>
                                      {ColorActive !==
                                        sessionStorage
                                          .getItem("current_data")
                                          .split(",")[1] ? (
                                        <div
                                          className="btn mt-5 bg_primary_blue"
                                          style={{
                                            height: "fit-content",
                                            marginTop: "3rem",
                                          }}
                                          onClick={() => {
                                            if (EditMode) {
                                              setEditWarning(true);
                                            } else {
                                              VersionPostData();
                                            }
                                          }}
                                        >
                                          Submit
                                        </div>
                                      ) : (
                                        <div className="disabled_btn">
                                          Submit
                                        </div>
                                      )}
                                    </React.Fragment>
                                  ) : (
                                    <React.Fragment>
                                      {ColorActive !== "" ? (
                                        <div
                                          className="btn bg_primary_blue"
                                          style={{
                                            height: "fit-content",
                                            marginTop: "3rem",
                                          }}
                                          onClick={() => {
                                            if (AddNewProject) {
                                              if (EditMode) {
                                                setEditWarning(true);
                                              } else {
                                                VersionPostData();
                                              }
                                            } else {
                                              setAddProjectWarning(true);
                                            }
                                          }}
                                        >
                                          {!EditMode
                                            ? ParentData.field_continue[0].value
                                            : "Submit"}
                                        </div>
                                      ) : (
                                        <div className="disabled_btn">
                                          {!EditMode
                                            ? ParentData.field_continue[0].value
                                            : "Submit"}
                                        </div>
                                      )}
                                    </React.Fragment>
                                  )}
                                  {EditMode && (
                                    <div
                                      className="btn btn-border-blue btn-sm"
                                      data-bs-toggle="modal"
                                      style={{
                                        height: "max-content",
                                        marginLeft: "15px",
                                        marginTop: "3rem",
                                      }}
                                      data-bs-target="#on_premises"
                                      data-bs-whatever="@on_premises"
                                      onClick={() => {
                                        let system = {
                                          auth_root: "readiness",
                                        };
                                        const jwt = sign(system, secret);
                                        sessionStorage.setItem("url_root", jwt);
                                        Router.push("/readiness");
                                        sessionStorage.removeItem("edit");
                                      }}
                                    >
                                      Cancel
                                    </div>
                                  )}
                                </div>
                              </React.Fragment>
                            )}
                          </React.Fragment>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {EditWarning && (
              <div className="warning_parent">
                <style jsx global>{`
                  body {
                    overflow: hidden;
                  }
                `}</style>
                <div
                  className="modal fade on_premises show"
                  id="on_premises"
                  aria-labelledby="exampleModalLabel"
                  aria-modal="true"
                  role="dialog"
                  style={{ display: "flex", alignItems: "center" }}
                >
                  <div className="modal-dialog modal-lg">
                    <div className="modal-content">
                      <div className="modal-body">
                        <div className="col-12 px-2">
                          <div className="px-3 py-3">
                            <p className="fw-600">
                              <i className="bi bi-exclamation warning-custom-icon"></i>{" "}
                              Warning
                            </p>
                            <p className="content">
                              If you continue, your migration progress of which
                              steps are complete will be lost, and you’ll need
                              to start from the beginning. Would you like to
                              continue?
                            </p>
                            <div className="text-center">
                              <div
                                className="btn h6 bg_primary_blue btn_custom"
                                type="button"
                                style={{
                                  marginTop: "0",
                                }}
                                data-bs-dismiss="modal"
                                aria-label="Close"
                                onClick={() => {
                                  setEditWarning(false);
                                  setVersionData(UserCurrentData);
                                  setColorActive(
                                    sessionStorage
                                      .getItem("current_data")
                                      .split(",")[1]
                                  );
                                  setVersion(
                                    sessionStorage
                                      .getItem("current_data")
                                      .split(",")[0]
                                  );
                                }}
                              >
                                No, keep original selections
                              </div>
                              <div
                                className="btn h6 btn-border-blue btn_custom"
                                style={{
                                  marginTop: "0",
                                }}
                                onClick={() => {
                                  (function () {
                                    setLoader(true);
                                    sessionStorage.setItem("dashboard", true);
                                    var ProcessData = UserData.current;
                                  })();
                                  let data = UserData.current;
                                  data["migration_data_id"] = sessionStorage
                                    .getItem("current_data")
                                    .split(",")[3];

                                  axios
                                    .post(
                                      API_ENDPOINT_URL +
                                      "v1/save-migration?_format=json",
                                      data,
                                      {
                                        headers: {
                                          Accept: "application/vnd.api+json",
                                          Authorization: `Bearer ${JWTDecoded.auth_token}`,
                                        },
                                      }
                                    )
                                    .then((e) => {
                                      if (e.data.message !== undefined) {
                                        sessionStorage.removeItem("system");
                                        location.replace(
                                          API_ENDPOINT_URL +
                                          "samllogin/?init=https://devmsk.aaiengineering.com/"
                                        );
                                      }
                                      if (
                                        [
                                          data.notify_for_cloud,
                                          data.notify_me,
                                        ].includes(true)
                                      ) {
                                        let system = {
                                          auth_root: "thankyou",
                                        };
                                        const jwt = sign(system, secret);
                                        sessionStorage.setItem("url_root", jwt);
                                        Router.push("/thankyou");
                                      } else {
                                        let system = {
                                          auth_root: "readiness",
                                        };
                                        const jwt = sign(system, secret);
                                        sessionStorage.setItem("url_root", jwt);
                                        Router.push("/readiness");
                                      }
                                    })
                                    .then(() => {
                                      sessionStorage.removeItem("edit");
                                      sessionStorage.removeItem("current_data");
                                      sessionStorage.removeItem("survey");
                                    });
                                }}
                              >
                                Yes, continue
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            )}
          </section>

          <Footer data={ParentData.field_footer_section[0].value} val="" />
        </React.Fragment>
      ) : (
        <div className="loader"></div>
      )}
    </React.Fragment>
  );
}

export default Migration;
