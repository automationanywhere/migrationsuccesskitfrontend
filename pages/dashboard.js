import React, { useState, useEffect, useRef } from "react";
import jwt_decode from "jwt-decode";
import Header from "../Components/Header/Header";
import Footer from "../Components/Footer/Footer";
import axios from "axios";
import { API_ENDPOINT_URL } from "../configuration";
import Router from "next/router";

export async function getStaticProps(context) {
  const opts = {
    method: "GET",
    headers: {
      Accept: "application/vnd.api+json",
      Authorization: `Basic ZGVlcGFrLnNpbmdoOkRlZXBha0AxOTkx`,
    },
  };
  const res = await fetch(API_ENDPOINT_URL + "get-global-constant", opts).then(
    (res) => res.json()
  );

  const access = await fetch(
    API_ENDPOINT_URL + "get-site-settings?_format=json",
    opts
  ).then((res) => res.json());

  return {
    props: { res, access },

    revalidate: 1,
  };
}

function Dashboard({ res, access }) {
  const [ParentData, setParentData] = useState([]);
  const [Username, setUsername] = useState("");
  const [DashBoardData, setDashBoardData] = useState([]);
  const [Loader, setLoader] = useState(true);
  const [SelectedID, setSelectedID] = useState(null);
  const [AddProject, setAddProject] = useState(true);
  const sign = require("jwt-encode");
  const secret = "secret";

  useEffect(() => {
    (() => {
      var decoded = jwt_decode(sessionStorage.getItem("system"));

      var projects = jwt_decode(sessionStorage.getItem("projects"));
      // var globals = jwt_decode(sessionStorage.getItem("globals"));
      if (sessionStorage.getItem("access") !== null) {
        if (access.addProjectPermission) {
          setAddProject(true);
        } else {
          setAddProject(false);
        }
      }

      setDashBoardData(projects.data);
      setParentData(res[0]);
      setUsername(decoded.first_name);
    })();

    setLoader(false);
  }, []);
  useEffect(() => {
    var decoded = jwt_decode(sessionStorage.getItem("system"));

    axios({
      url: API_ENDPOINT_URL + `get-projects?_format=json`,
      method: "get",
      headers: {
        Accept: "application/vnd.api+json",
        Authorization: `Bearer ${decoded.auth_token}`,
      },
    })
      .catch(() => Router.push("/api-failed"))

      .then((e) => {
        if(e.data.message !== undefined){
            sessionStorage.removeItem("system")
            location.replace(API_ENDPOINT_URL + "samllogin/?init=https://devmsk.aaiengineering.com/");
        }
            
             var FinalData = Object.keys(DashBoardData).map((key) => {
          return DashBoardData[key];
        });
        if (typeof e.data == "object") {
          var FinalData = Object.keys(e.data).map((key) => {
            return e.data[key];
          });
          setDashBoardData(FinalData);
        } else {
          setDashBoardData(e.data);
        }

        let project = { data: e.data };
        const project_token = sign(project, secret);
        sessionStorage.setItem("projects", project_token);
        sessionStorage.removeItem("dashboard");
        
       
      });

    //   return () => {
    //     second
    //   }
  }, [Loader]);

  console.log(ParentData);
  return Loader ? (
    <div className="loader"></div>
  ) : (
    <React.Fragment>
      <Header
        title={ParentData.title[0].value}
        Links={ParentData.field_quick_links}
        LoginLabel={ParentData.field_login_text_label[0].value}
        LinkLabel={ParentData.field_quick_link_label[0].value}
        User={Username}
      />

      <section className="mt-5">
        <div className="container-fluid pd-b px-4">
          <div className="row">
            <div className="col-12 col-md-5 col-lg-5 col-xl-4">
              <div className="full-height black_design text-white px-4 py-5 ">
                <h1 className="fw-light">
                  Welcome
                  <br />
                  Back,
                  <span className="fc-yellow">{Username}!</span>
                </h1>

                <p className="mt-sm-4 content text-white">
                  Let&apos;s get started with new project or select any existing
                  project
                </p>
              </div>
            </div>
            <div className="col-12 col-md-7 col-lg-7 col-xl-8 mb-4">
              <div className="mg_ass_form">
                <div className="col-sm-12">
                  <div className="col-sm-11 margin-auto">
                    <div className="mg_ass_selection">
                      <h2 className=" fw-light fc-theme-dark py-4">
                        Select a Project to go ahead with Migration
                      </h2>
                      <div
                        className="mg_project custom-scrollbar scroll-wrapper-dashboard"
                        style={{
                          maxHeight: "60vh",
                          paddingRight: "20px",
                          overflowY: "scroll",
                          overflowX: "hidden",
                        }}
                      >
                        <div className="row">
                          {AddProject && (
                            <div
                              className="col-12 col-sm-6 projects"
                              onClick={() => {
                                let system = {
                                  auth_root: "migration",
                                };
                                const jwt = sign(system, secret);
                                sessionStorage.setItem("url_root", jwt);
                                if (sessionStorage.getItem("edit") !== null) {
                                  sessionStorage.removeItem("edit");
                                }
                                Router.push("/migration");
                              }}
                            >
                              <div className="card p-card dashed_border">
                                <div className="p-user-info px-3 pt-4 pb-2">
                                  <div className="h5 p-title">
                                    Start a new project
                                  </div>
                                  <div className="py-3 text-center h4 p-icon">
                                    <i className="p-plus-icon"></i>
                                  </div>
                                </div>
                              </div>
                            </div>
                          )}

                          {DashBoardData.map((e) => {
                            return (
                              <div
                                className={`col-12 col-sm-6 projects `}
                                key={e.projectId}
                                onClick={() => {
                                  sessionStorage.setItem(
                                    "project_arr",
                                    e.projectId
                                  );
                                  sessionStorage.setItem(
                                    "project_name",
                                    e.projectName
                                  );
                                  setSelectedID(e.projectId);
                                  // Router.push("/readiness");
                                }}
                              >
                                <div
                                  className={`card p-card  ${
                                    e.completeStatus
                                      ? "p_success"
                                      : "p_in_progress"
                                  } ${
                                    e.projectId == SelectedID &&
                                    "project_selected"
                                  }`}
                                >
                                  <div className="p-user-info px-3 pt-4 pb-2">
                                    <div className="h5 p-title">
                                      {e.projectName}
                                    </div>
                                    <div className="p-sm-title">
                                      {e.authorName}
                                      <span className="xs-sm-content px-1 fst-italic">
                                        ({e.publishDate})
                                      </span>
                                    </div>
                                  </div>
                                  <div className="progress_info bg-light-grey px-3 pt-2 pb-2">
                                    <div className="spacebetween-align-div col-12 ">
                                      <p className="p-sm-title mb-0">
                                        {e.completeStatus
                                          ? "Migrated to automation 360"
                                          : e.phaseName}
                                      </p>
                                      <div className="right-align-div">
                                        <span
                                          className={`btn detail_wrapper px-3 py-1 rounded-pill ${
                                            e.completeStatus &&
                                            "dashboard_completed"
                                          }`}
                                        >
                                          {e.completeStatus
                                            ? "completed"
                                            : `${e.stepPercentage}%`}
                                        </span>
                                      </div>
                                    </div>
                                    <div className={`project_progress_bar`}>
                                      <span></span>
                                    </div>
                                    <div
                                      className="progress_main"
                                      style={{
                                        width: e.completeStatus
                                          ? "100%"
                                          : `${e.stepPercentage}%`,
                                        backgroundColor: e.completeStatus
                                          ? "#75c05c"
                                          : "#4E6B8C",
                                      }}
                                    ></div>
                                  </div>
                                </div>
                              </div>
                            );
                          })}
                        </div>
                      </div>
                      {SelectedID !== null ? (
                        <div
                          className="btn mt-4 bg_primary_blue "
                          onClick={() => {
                            let Filter = DashBoardData.filter(
                              (data) => data.projectId == SelectedID
                            );
                            if (
                              Filter[0].notify_me == false &&
                              Filter[0].notify_for_cloud == false
                            ) {
                              let system = {
                                auth_root: "readiness",
                              };
                              const jwt = sign(system, secret);
                              sessionStorage.setItem("url_root", jwt);
                              Router.push("/readiness");
                            } else {
                              let system = {
                                auth_root: "thankyou",
                              };
                              const jwt = sign(system, secret);
                              sessionStorage.setItem("url_root", jwt);
                              Router.push("/thankyou");
                            }
                          }}
                        >
                          Continue
                        </div>
                      ) : (
                        <div className="btn mt-4 btn-disabled">Continue</div>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer data={ParentData.field_footer_section[0].value} val="" />
    </React.Fragment>
  );
}

export default Dashboard;
