import React from 'react'

function Failed() {
  return (
   <React.Fragment>
       <header className="error">
        <div className="container">
            <div className="row">
                <div className="col-8 col-sm-6 left-align-div py-2">
                    <span className="logo"> <img src="/images/logo-aa.svg" /></span>
                    
                </div>

                
            </div>
        </div>
    </header>
    
    <section className="ws_height" style={{
    marginTop:"51px"
}}>

        <div className="container px-4">
                <div className="text-center">
                    <h1 className="fw-light">Unable to connect to <span className="fc-yellow">API</span></h1>
                    <p className="fs-5 fw-normal fc-dark-grey">We will retry when the connect is made and redirect to the page. </p>
                    <figure><img src="/images/404.jpg" className="img-responsive" /></figure>
                </div>
               
            

        </div>
    </section>


  


    <footer className="bg-light py-4 text-center fc-dark h6 footer error_page" style={{
    position: "fixed",
    width: "100%",
    bottom: "0",
}}>
        <div className="container">
            <div className="col-12">
                <span>© 2021 Automation Anywhere, Inc. | <a href="https://www.automationanywhere.com/privacy"> Privacy</a> | <a href="https://www.automationanywhere.com/terms">Terms</a> | <a href="#" title="Trademark">Trademark</a> | <a href="#" title="Products">Products</a> | <a href="#" title="customers">Customers</a> | <a href="#" title="carrers">Careers</a></span>
            </div>

        </div>

    </footer>
   </React.Fragment>
  )
}

export default Failed