export default async function handler(req, res) {
  var apeople_call = require("../../data/apeople_fetch");
  const fs = require("fs");

  let rawdata = fs.readFileSync("data/apeople.json");
  let parsed_data = JSON.parse(rawdata);
  const date = new Date();
  const date_day = date.getDate();
  if (parsed_data.length == 0) {
    apeople_call.log(req.headers.input).then((e) => {
        var apeople_data = {
          name: req.headers.input,
          date: date_day,
          data: e,
        };
        parsed_data.push(apeople_data)
        fs.writeFileSync("data/apeople.json", JSON.stringify(parsed_data));
        res.json(apeople_data.data);
      });
  } else {
     
    let filtered = parsed_data.filter((data) => data.name == req.headers.input);
    if (filtered.length !== 0) {
      if (filtered[0].date == date_day) {
        res.json(filtered[0].data);
      } else {
        apeople_call.log(req.headers.input).then((e) => {
          var apeople_data = {
            name: req.headers.input,
            date: date_day,
            data: e,
          };
          let index = parsed_data.findIndex((x) => x.name == req.headers.input);
          parsed_data[index] = apeople_data;
          fs.writeFileSync("data/apeople.json", JSON.stringify(parsed_data));
          res.json(apeople_data.data);
        });
      }
    }else{
        apeople_call.log(req.headers.input).then((e) => {
            var apeople_data = {
              name: req.headers.input,
              date: date_day,
              data: e,
            };
            parsed_data.push(apeople_data)
            fs.writeFileSync("data/apeople.json", JSON.stringify(parsed_data));
            res.json(apeople_data.data);
          });
    }
  }
}
