import Head from "next/head";
import Image from "next/image";
import Header from "../Components/Header/Header";
import React, { useEffect, useState, useLayoutEffect } from "react";
import axios from "axios";
import Link from "next/link";
import Footer from "../Components/Footer/Footer";
import Router from "next/router";
import jwt_decode from "jwt-decode";
import { API_ENDPOINT_URL } from "../configuration";
import bodyParser from "body-parser";
import { promisify } from "util";

const getBody = promisify(bodyParser.urlencoded());

export async function getServerSideProps({ req, res }) {
  await getBody(req, res);
  let current_user = null;
  let projects = null
  if (req.body?.jwt_token !== undefined) {
    const opts = {
      method: "GET",
      headers: {
        Accept: "application/vnd.api+json",
        Authorization: `Bearer ${req.body?.jwt_token}`,
      },
    };
    const res = await fetch(
      API_ENDPOINT_URL + "current-user?_format=json",
      opts
    ).catch(() => current_user = false)
    current_user = await res.json();
    current_user[0]["token"] = req.body?.jwt_token
    const projects_data = await fetch(
      API_ENDPOINT_URL + `get-projects?_format=json`,
      opts
    ).catch(() => current_user = false)
    projects = await projects_data.json();

  }
  console.log(projects)
  return {
    props: {
      server_token: current_user,
      projects : projects
    },
  };
}

export default function Home({ server_token , projects}) {
  const sign = require("jwt-encode");
  const secret = "secret";
  console.log(projects);
  useLayoutEffect(() => {
    if (server_token == null && sessionStorage.getItem("system") == null) {
      location.replace(
        API_ENDPOINT_URL + "samllogin/?init=https://devmsk.aaiengineering.com/"
      );
    } else if (
      server_token !== null &&
      sessionStorage.getItem("system") == null
    ) {
      let token = server_token[0].token
      
          let project = { data: projects };
          const project_token = sign(project, secret);
          sessionStorage.setItem("projects", project_token);
          let system = {
            userData: server_token[0].mail,
            user: server_token[0].name,
            UID: server_token[0].uid,
            first_name: server_token[0].field_firstname,
            auth_token: token,
          };
          const jwt = sign(system, secret);
          sessionStorage.setItem("system", jwt);
          axios({
            url: API_ENDPOINT_URL + "get-recent-project?_format=json",
            method: "get",
            headers: {
              Accept: "application/vnd.api+json",
              Authorization: `Bearer ${token}`,
            },
          }).then((res) => {
            if (projects.length == 0) {
              sessionStorage.setItem(
                "url_root",
                sign(
                  {
                    auth_root: "migration",
                  },
                  secret
                )
              );
              Router.push("/migration");
            } else {
              if (res.data.LastAccessedProject == "undefined") {
                Router.push("/dashboard");
              } else {
                if (
                  projects.filter(
                    (data) => data.projectId == res.data.LastAccessedProject
                  ).length == 0
                ) {
                  Router.push("/dashboard");
                } else {
                  sessionStorage.setItem(
                    "project_arr",
                    res.data.LastAccessedProject
                  );
                  sessionStorage.setItem(
                    "project_name",
                    projects.filter(
                      (data) => data.projectId == res.data.LastAccessedProject
                    )[0].projectName
                  );
                  sessionStorage.setItem(
                    "url_root",
                    sign(
                      {
                        auth_root: "readiness",
                      },
                      secret
                    )
                  );
                  Router.push("/readiness");
                }
              }
            }
          });
          
        
    } else if (window.sessionStorage.getItem("system") !== null) {
      var decoded = jwt_decode(sessionStorage.getItem("system"));
      axios({
        url: API_ENDPOINT_URL + `get-projects?_format=json`,
        method: "get",
        headers: {
          Accept: "application/vnd.api+json",
          Authorization: `Bearer ${decoded.auth_token}`,
        },
      })
        .catch(() => Router.push("/api-failed"))

        .then((response) => {
          if (response.data.length == 0) {
            Router.push("/migration");
          } else {
            Router.push("/dashboard");
          }
        });
    }
    else if (server_token == false){
      Router.push("/api-failed")
    }
  }, []);

  return <div className="loader"></div>;
}
