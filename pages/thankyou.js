import React, { useState, useEffect } from 'react';
import Header from '../Components/Header/Header';
import Footer from '../Components/Footer/Footer';
import axios from 'axios';
import Router from 'next/router';
import jwt_decode from 'jwt-decode';
import { API_ENDPOINT_URL, THANKYOU_PUSH_URL } from "../configuration";


export async function getStaticProps(context) {
	const opts = {
		method: 'GET',
		headers: {
      Accept: "application/vnd.api+json",
      Authorization: `Basic ZGVlcGFrLnNpbmdoOkRlZXBha0AxOTkx`,
    },
	};
	const res = await fetch(
		API_ENDPOINT_URL + 'get-global-constant',
		opts
	).then((res) => res.json());

	return {
		props: { res },

		revalidate: 1,
	};
}


function Thankyou({res}) {
	const [ParentData, setParentData] = useState([]);
	const [Username, setUsername] = useState('');
	const [Passed, setPassed] = useState(false);
	const sign = require('jwt-encode');
	const secret = 'secret';
	useEffect(() => {
		if (sessionStorage.getItem('system') !== null) {
			var decoded = jwt_decode(sessionStorage.getItem('system'));
			var url_root = jwt_decode(sessionStorage.getItem('url_root'));
			// var globals = jwt_decode(sessionStorage.getItem('globals'));
			if(sessionStorage.getItem("restricted") == null){
				if (url_root.auth_root == 'thankyou') {
				setParentData(res[0]);
				setUsername(decoded.first_name);
				setPassed(true);
			} else {
				if (typeof url_root.auth_root == 'object') {
					Router.push('/');
				} else {
					Router.push(`/${url_root.auth_root}`);
				}
			}
			}else{
					setParentData(res[0]);
					setUsername(decoded.first_name);
					setPassed(true);
			}
			
		}
	}, []);

	return (
		<div>
			{Passed && (
				<React.Fragment>
					<Header
						title={ParentData.title[0].value}
						Links={ParentData.field_quick_links}
						LoginLabel={ParentData.field_login_text_label[0].value}
						LinkLabel={ParentData.field_quick_link_label[0].value}
						User={Username}
					/>
					<section>
						<div className='container pd-b px-4'>
							<div className='full-height-vh align-center'>
								<div className='text-center'>
									<h1 className='fc-theme'>Thank You!</h1>
									<p className='fs-5 fc-dark'>
										We will notify you when a new Automation 360 version is
										available.{' '}
									</p>
									<div
										className='btn mt-3 bg_primary_blue'
										onClick={() =>
											Router.push(THANKYOU_PUSH_URL)
										}>
										Go to Migration website
									</div>
								</div>
							</div>
						</div>
					</section>
					<Footer
						data={ParentData.field_footer_section[0].value}
						val='fixed-bottom'
					/>
				</React.Fragment>
			)}
		</div>
	);
}

export default Thankyou;
