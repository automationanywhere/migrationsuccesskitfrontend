import "../styles/globals.css";
import "../styles/theme.css";
import "../styles/standard.css";
import "../styles/bootstrap.css";
// import '../styles/library/search_dropdown/select2.min.css';
// import '../styles/progress.css';
import "../styles/plugin.css";
import Header from "../Components/Header/Header";
import React, { useEffect, useState } from "react";
import Head from "next/head";
function MyApp({ Component, pageProps }) {
  const [ParentData, setParentData] = useState([]);
  const [UserName, setUserName] = useState("");

  return (
    <React.Fragment>
      <Head>
        <link rel="shortcut icon" href="/images/favicon.png" type="image/png" />
        <title>Upgrade Launchpad</title>
        {/* <meta
          httpEquiv="Content-Security-Policy"
          content="
   
   
   media-src:;
      worker-src blob:; 
      
      
     
      "
        /> */}
        <meta
          httpEquiv="Content-Security-Policy"
          content="script-src 'self' https://www.youtube.com/ https://s.go-mpulse.net/ https://fast.wistia.com/  'unsafe-eval' 'unsafe-inline'"
        />
      </Head>
      <Component {...pageProps} />
    </React.Fragment>
  );
}

export default MyApp;
