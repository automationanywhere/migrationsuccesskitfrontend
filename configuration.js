export const API_ENDPOINT_URL =
	'https://qamsk.aaiengineering.com/migration_success_kit/';
	
export const HEADER_PROFILE_URL =
	'https://uat-automationanywhere.cs5.force.com/Apeople/s/';
export const HEADER_LOGOUT_URL =
	'https://qamsk.aaiengineering.com/migration_success_kit/user/logout';

export const THANKYOU_PUSH_URL =
	'https://stage.aai.marketing/products/automation-360/migration';

export const AUTH_LOGOUT_URL = "https://uat-automationanywhere.cs72.force.com/secur/logout.jsp"