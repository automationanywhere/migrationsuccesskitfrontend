import React,{ useState , useEffect , useRef } from "react";
import axios from "axios";
import { API_ENDPOINT_URL } from "../../configuration";
import Router from "next/router";
import jwt_decode from "jwt-decode";
import { Link } from "react-scroll";
export default function ReadinessProgressBar({access,ReadinessData,Phase,token,Projects,ProgressData,UserLicenseData,ProjectRouting}) {
    const [ReadinessProject, setReadinessProject] = useState(false);
    const [LocalProjects, setLocalProjects] = useState([]);
    const [EditProject, setEditProject] = useState(true);
    const [StartNewProject, setStartNewProject] = useState(true);
    const [Display, setDisplay] = useState("block")
    const [Length, setLength] = useState(0);
    var Scroll = require("react-scroll");
    var scroller = Scroll.scroller;
    const edit = useRef(false);
    var decoded = jwt_decode(sessionStorage.getItem("system"));
    const sign = require("jwt-encode");
    const secret = "secret";
    useEffect(() => {
      if (access.editProjectPermission) {
        setEditProject(true);
      } else {
        setEditProject(false);
      }
      if (access.addProjectPermission) {
        setStartNewProject(true);
      } else {
        setStartNewProject(false);
      }
      document.addEventListener("click", (e) => {
        if (Projects.length == 1) {
          if (e.clientY > 300) {
            setReadinessProject(false);
          }
        } else {
          if (e.clientY > 500) {
            setReadinessProject(false);
          }
        }
      });
      setLength(Phase);
    }, []);
    
    useEffect(() => {
      axios({
        url: API_ENDPOINT_URL + `get-projects?_format=json`,
        method: "get",
        headers: {
          Accept: "application/vnd.api+json",
          Authorization: `Bearer ${decoded.auth_token}`,
        },
      })
        .catch(() => Router.push("/api-failed"))

        .then((e) => {
          (() => {
            if (e.data.message !== undefined) {
              sessionStorage.removeItem("system");
              location.replace(
                API_ENDPOINT_URL +
                "samllogin/?init=https://devmsk.aaiengineering.com/"
              );
            }
            setLocalProjects(e.data);
          })();
          let project = { data: e.data };
          const project_token = sign(project, secret);
          sessionStorage.setItem("projects", project_token);
          if (
            e.data.filter(
              (e) => e.projectId == sessionStorage.getItem("project_arr")
            )[0].completeStatus == true
          ) {
            setLength(3);
          }
        });
      // var projects = jwt_decode(sessionStorage.getItem('projects'));
      // setLocalProjects(projects.data)
    }, [ReadinessProject]);

    useEffect(() => {
      window.onscroll = function(e) {
        // print "false" if direction is down and "true" if up

        if(this.oldScroll > this.scrollY){
          setDisplay("block")
        }else{
          setDisplay("none")

        }
        this.oldScroll = this.scrollY;
      }
    }, [])
    
    return (
      <React.Fragment>
        
        <style jsx global>{`
        
      
        ul#progressbar li:nth-child(${ReadinessData.phase.length}) span:before {
          content: "\f3cb";
      }
      ul#progressbar a:nth-child(5) li span:before {
        content: "\f3cb";
        font-size: 1rem !important;
      
      
    }
      `}</style>
        <div
          className="project-header text-center py-3" 
          style={{ cursor: "pointer" , display:Display }}
          onClick={() => setReadinessProject((e) => !e)}
        >
          <span className="p-nav-title">
            {ReadinessProject
              ? "All Projects"
              : sessionStorage.getItem("project_name")}
          </span>
          {!ReadinessProject ? (
            <i className="bi bi-caret-down-fill mx-2 p-nav-icon "></i>
          ) : (
            <i className="bi bi-x bi-x-lg mx-2 p-nav-icon "></i>
          )}
        </div>
        {!ReadinessProject ? (
          <div className="container" style={{ display:Display }}>
            <div className="col-sm-12">
              <div className="row align-center">
                <div className="col-sm-12 col-lg-9 col-xl-9 text-center">
                  <div className="px-0 pt-2 pb-0 mb-1">
                    <form id="form">
                      <div className="prog">
                        <ul id="progressbar" className="col-8 margin-auto">
                          {ReadinessData.phase !== undefined && (
                            <React.Fragment>
                              {ReadinessData.phase.map((data, index) => {
                                return (
                                  <React.Fragment key={index}>
                                    {index < ProgressData ? (
                                      <Link
                                        to={
                                          index == 3
                                            ? "Success"
                                            : `migration_phase_${index}`
                                        }
                                        offset={-200}
                                      >
                                        <li
                                          className="active"
                                          id={`phase_${index !== 0 && index - 1
                                            }_${index !== 0 &&
                                            ReadinessData.phase[index - 1].steps
                                              .length - 1
                                            }`}
                                        >
                                          <span></span>
                                        </li>
                                      </Link>
                                    ) : (
                                      <React.Fragment>
                                        {ProgressData == index ? (
                                          <Link
                                            to={
                                              index == ReadinessData.phase.length -1
                                                ? "Success"
                                                : `migration_phase_${index}`
                                            }
                                            offset={-200}
                                          >
                                            <li
                                              className="in_progress"
                                              key={data.title}
                                            >
                                              <span></span>
                                            </li>
                                          </Link>
                                        ) : (
                                          <li>
                                            <span></span>
                                          </li>
                                        )}
                                      </React.Fragment>
                                    )}
                                  </React.Fragment>
                                );
                              })}
                            </React.Fragment>
                          )}
                          {Phase == 4 && (
                            <div
                              className="fake_progress"
                              style={{ width:  "100%" }}
                            ></div>
                          )}
                          {Phase == 3 && (
                            <div
                              className="fake_progress"
                              style={{ width:  ReadinessData.phase.length == 5 ? "75%" : "100%"  }}
                            ></div>
                          )}
                          {Phase == 0 && (
                            <div
                              className="fake_progress"
                              style={{ width: "0%" }}
                            ></div>
                          )}
                          {Phase == 1 && (
                            <div
                              className="fake_progress"
                              style={{ width: ReadinessData.phase.length == 5 ? "27%" : "35%" }}
                            ></div>
                          )}
                          {Phase == 2 && (
                            <div
                              className="fake_progress"
                              style={{ width: ReadinessData.phase.length == 5 ? "50%" : "65%"  }}
                            ></div>
                          )}
                        </ul>
                        <ul className={`step_name ${ReadinessData.phase.length == 5 && "iq_bot"}`}>
                          {ReadinessData.phase !== undefined && (
                            <React.Fragment>
                              {ReadinessData.phase.map((e, index) => {
                                return (
                            
                                  index == 0 ? (
                                    <Link
                                      to={`migration_phase_${index}`}
                                      offset={-200}
                                    >
                                      <li
                                        className="progress_step_name"
                                        key={e.title}
                                      >
                                        <span></span>
                                        <p>{e.title + ProgressData}</p>
                                        <p>{""}</p>
                                        <p
                                          style={{
                                            marginTop: "-14px",
                                          }}
                                        >
                                          {UserLicenseData !==
                                            undefined &&
                                            UserLicenseData
                                              .field_control_room_version}
                                        </p>
                                      </li>
                                    </Link>
                                  ) : (
                                    <Link
                                      to={`migration_phase_${index}`}
                                      offset={-200}
                                    >
                                      <li
                                        className="progress_step_name"
                                        key={e.title}
                                      >
                                        <span></span>
                                        <p>{e.title}</p>
                                      </li>
                                    </Link>
                                  )

                       
                                );
                              })}
                            </React.Fragment>
                          )}
                        </ul>
                      </div>
                    </form>
                  </div>
                </div>
                {Length !== 3 && EditProject && (
                  <span
                    className="btn text-white edit_journey"
                    onClick={() => {
                      let system = {
                        auth_root: "migration",
                      };
                      let obj = [
                        UserLicenseData.field_control_room_version,
                        UserLicenseData.field_migation_type,
                        UserLicenseData.field_control_room_version_id,
                        ReadinessData.migration_data_id,
                        sessionStorage.getItem("project_name"),
                      ];
                      console.log(UserLicenseData);
                      const jwt = sign(system, secret);
                      sessionStorage.setItem("edit", true);
                      sessionStorage.setItem("current_data", obj);
                      sessionStorage.setItem("url_root", jwt);
                      Router.push("/migration");
                    }}
                  >
                    {" "}
                    <i className="bi bi-pencil-fill"></i> Edit Journey
                  </span>
                )}
              </div>
            </div>
          </div>
        ) : (
          <div className="container">
            <div className="col-sm-12 project-list active">
              <div className="mg_project p-overlap py-3">
                <div className="container">
                  <div className="row  custom-scrollbar scroll-wrapper-readiness">
                    {StartNewProject && (
                      <div className="col-12 col-sm-12 col-md-6 col-lg-4 projects">
                        <div
                          className="card p-card dashed_border"
                          onClick={() => {
                            sessionStorage.setItem("restricted", false);
                            sessionStorage.removeItem("survey");

                            Router.push("/migration");
                          }}
                        >
                          <div className="p-user-info px-3 pt-3 pb-1">
                            <div className="h5 p-title">
                              Start a new project
                            </div>
                            <div className="py-3 text-center h4 p-icon">
                              <i className="p-plus-icon"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                    )}

                    {LocalProjects.filter(
                      (e) =>
                        e.projectId == sessionStorage.getItem("project_arr")
                    ).map((data) => {
                      return (
                        <div
                          className="col-12 col-sm-12 col-md-6 col-lg-4 projects"
                          key={data.projectId}
                        >
                          <div
                            className="card p-card p_in_progress"
                            onClick={() => {
                              ProjectRouting(
                                data.projectId,
                                data.projectName,
                                data.notify_me,
                                data.notify_for_cloud
                              );
                            }}
                          >
                            <div className="p-user-info px-3 pt-3 pb-1">
                              <div className="h5 p-title">
                                {data.projectName}
                              </div>
                              <div className="p-sm-title">
                                {data.authorName}
                                <span className="xs-sm-content px-1 fst-italic">
                                  ({data.publishDate})
                                </span>
                              </div>
                            </div>
                            <div className="progress_info bg-light-grey px-3 pt-2 pb-2">
                              <div className="spacebetween-align-div col-12 ">
                                <p className="p-sm-title mb-0">
                                  {data.completeStatus
                                    ? "Migrated to automation 360"
                                    : data.phaseName}
                                </p>
                                <div className="right-align-div">
                                  <span
                                    className={`btn detail_wrapper px-3 py-1 rounded-pill ${data.completeStatus &&
                                      "dashboard_completed"
                                      }`}
                                  >
                                    {" "}
                                    {data.completeStatus
                                      ? "completed"
                                      : `${data.stepPercentage}%`}
                                  </span>
                                </div>
                              </div>
                              <div className="project_progress_bar">
                                <span></span>
                              </div>
                              <div
                                className="progress_main"
                                style={{
                                  width: data.completeStatus
                                    ? "100%"
                                    : `${data.stepPercentage}%`,
                                  backgroundColor: data.completeStatus
                                    ? "#75c05c"
                                    : "rgb(78, 107, 140)",
                                }}
                              ></div>
                            </div>
                          </div>
                        </div>
                      );
                    })}
                    {LocalProjects.filter(
                      (e) =>
                        e.projectId !== sessionStorage.getItem("project_arr")
                    ).map((data) => {
                      return (
                        <div
                          className="col-12 col-sm-12 col-md-6 col-lg-4 projects"
                          key={data.projectId}
                        >
                          <div
                            className="card p-card p_in_progress"
                            onClick={() => {
                              ProjectRouting(
                                data.projectId,
                                data.projectName,
                                data.notify_me,
                                data.notify_for_cloud
                              );
                            }}
                          >
                            <div className="p-user-info px-3 pt-3 pb-1">
                              <div className="h5 p-title">
                                {data.projectName}
                              </div>
                              <div className="p-sm-title">
                                {data.authorName}
                                <span className="xs-sm-content px-1 fst-italic">
                                  ({data.publishDate})
                                </span>
                              </div>
                            </div>
                            <div className="progress_info bg-light-grey px-3 pt-2 pb-2">
                              <div className="spacebetween-align-div col-12 ">
                                <p className="p-sm-title mb-0">
                                  {data.completeStatus
                                    ? "Migrated to automation 360"
                                    : data.phaseName}
                                </p>
                                <div className="right-align-div">
                                  <span
                                    className={`btn detail_wrapper px-3 py-1 rounded-pill ${data.completeStatus &&
                                      "dashboard_completed"
                                      }`}
                                  >
                                    {" "}
                                    {data.completeStatus
                                      ? "completed"
                                      : `${data.stepPercentage}%`}
                                  </span>
                                </div>
                              </div>
                              <div className="project_progress_bar">
                                <span></span>
                              </div>
                              <div
                                className="progress_main"
                                style={{
                                  width: data.completeStatus
                                    ? "100%"
                                    : `${data.stepPercentage}%`,
                                  backgroundColor: data.completeStatus
                                    ? "#75c05c"
                                    : "rgb(78, 107, 140)",
                                }}
                              ></div>
                            </div>
                          </div>
                        </div>
                      );
                    })}
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
      </React.Fragment>
    );
  }