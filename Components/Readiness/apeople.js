import React,{useState,useEffect} from 'react'
import axios from 'axios'
export default function Apeople({string,Search}) {
    const [ApeopleMainData, setApeopleMainData] = useState([])
    const [ApeopleData, setApeopleData] = useState([])
    const [ApeopleSwitch, setApeopleSwitch] = useState(1)
    const [KBdata, setKBdata] = useState([])
    const [Loaded, setLoaded] = useState(false)
    useEffect(() => {
      if([Search.apeople,Search.knowledge_base].includes('') == false){
         axios.get("/api/apeople_fetch",{
            headers:{
              input : Search.apeople
            }
          }).then((res) => {
            setApeopleData(res.data)
            setApeopleMainData(res.data)

          }).then((data) => {
            axios.get("/api/kbapeople_fetch",{
                headers:{
                  input : Search.knowledge_base
                }
              }).then((res) => {
                setKBdata(res.data)
              }).then(() => setLoaded(true))
          });
          console.log([Search.apeople,Search.knowledge_base])
          console.log(Search)

          console.log([Search.apeople,Search.knowledge_base].includes(''))

      }else{
        console.log("no data")
      }
       
    }, [])

    

    

  return (
    <div className="trend_topic mg-t-62">
                <div className="tt_title px-4 py-3">
                  <h5 className="fw-600">Top Migration Threads</h5>
                       </div>
                <div className="apeople_switchbox">
                  <div
                    onClick={() => {
                      setApeopleSwitch(1)
                      setApeopleMainData([])

                      setApeopleMainData(ApeopleData)


                    //   axios.get("/api/apeople_fetch",{
                    //     headers:{
                    //       input : Search.apeople
                    //     }
                    //   }).then((res) => {
                    //     setApeopleData(res.data)
                    //   });
                    }}
                    style={{
                      backgroundColor: ApeopleSwitch == 1 && "#FF7733",
                      color: ApeopleSwitch == 1 && "white",
                    }}
                  >
                    <h5>A-People Posts</h5>
                  </div>
                  <div
                    onClick={() => {
                        (async () => {
                            setApeopleMainData([])
                            setApeopleSwitch(2)
                          })().then(() =>  setApeopleMainData(KBdata))
                      
                     
                      

                       
                    //   axios.get("/api/kbapeople_fetch",{
                    //     headers:{
                    //       input : Search.knowledge_base
                    //     }
                    //   }).then((res) => {
                    //     setApeopleData(res.data);
                        
                    //   }).catch((e) => null);
                    }}
                    style={{
                      marginLeft: "4px",
                      backgroundColor: ApeopleSwitch == 2 && "#FF7733",
                      color: ApeopleSwitch == 2 && "white",
                    }}
                  >
                    <h5>Knowledge Articles </h5>
                  </div>
                </div>
                <div className="tt_content">
                  {/* <div className="menu_content_header"><i className="mx-2 icon-design bi bi-box-arrow-up-right"></i> Quick Links</div> */}
                  <div className="tt-content-data mb-3">
                      {ApeopleMainData.result !== undefined && 
                      ApeopleMainData.result.hits.slice(0, 3).map((data) => {
                        return (
                          <div
                            className="tt-content-item"
                            key={data.highlight.TitleToDisplay[0]}
                          >
                            <a
                              href={data.clientHref+data.Id}
                              target="_blank"
                              className="tt-content-item-title"
                              style={{
                                  textDecoration:"underline",
                                  color:"#4E6B8C"
                              }}
                            >
                              <div
                className="overflow-hidden"                               dangerouslySetInnerHTML={{
                                  __html: data.highlight.TitleToDisplayString.length > 55 ? data.highlight.TitleToDisplayString[0].slice(0,55) + "..." :  data.highlight.TitleToDisplayString[0],
                                }}
                              ></div>
                            </a>
                          </div>
                        );
                      })
                      
                      }
                    
                  </div>
                  {ApeopleSwitch == 1 && <a
                    href={ApeopleSwitch == 1 ? Search.start_new_post_for_apeople_URL : Search.start_new_post_for_knowledge_base_URL}
                    className="btn col-12 bg_primary_blue mb-3"
                    target="_blank"
                  >
                    Start New Post
                    
                  </a>}
                  
                  <a
                    href={ApeopleSwitch == 1 ? Search.show_more_result_for_apeople_URL : Search.show_more_result_for_knowledge_base_URL}
                    className="btn col-12 btn-border-blue mb-3 apeople_btn"
                    target="_blank"
                  >
                  
                    {ApeopleSwitch == 1 ? "Show More Topics" : "Read More"}

                  </a>
                </div>
              </div>
  )
}
