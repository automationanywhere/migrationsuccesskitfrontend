import axios from "axios";
import React, { useState, useEffect, useRef } from "react";
import * as Scroll from "react-scroll";
import { API_ENDPOINT_URL } from "../../configuration";
import Router from "next/router";

function Survey({
  submitted,
  display,
  phase,
  phaseID,
  token,
  PhaseNos,
  MigID,
  Completed,
  Rating_obj,
  DefVal
}) {
  const [Submit, setSubmit] = useState(submitted);
  const [Rating, setRating] = useState(0);
  const [Loading, setLoading] = useState(false);
  const [Val, setVal] = useState("none");
  const feedback = useRef("");

  var Element = Scroll.Element;

  function StepScrolling(location) {
    console.log("scroll");
    var Scroll = require("react-scroll");
    var scroller = Scroll.scroller;
    scroller.scrollTo(location, {
      offset: -200,
    });
  }

  function SubmitReview(params) {
    var data = JSON.stringify(    {
      rating: Rating,
      rating_detail: feedback.current,
      phase_id: phaseID,
      migration_data_id	: MigID
    });

    var config = {
      method: "post",
      url: API_ENDPOINT_URL + "v1/save-survey?_format=json",
      headers: {
        Accept: "application/vnd.api+json",
        Authorization:
          `Bearer ${token}`,
        "Content-Type": "application/json",
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        let arr = JSON.parse(sessionStorage.getItem("survey"));
        let Index = arr.findIndex((obj) => obj.phase_id == PhaseNos);
        arr[Index].status = true;
        sessionStorage.setItem("survey", JSON.stringify(arr));
        setSubmit(true);
        setTimeout(() => {
          StepScrolling(`migration_phase_${PhaseNos + 1}`);
        }, 3000);
      })
      .catch((err) => {
        console.log(err)
         Router.push("api-failed");
      })
      

    // axios
    //   .post(
    //     API_ENDPOINT_URL + "/v1/save-survey?_format=json",
        // {
        //   rating: Rating,
        //   rating_detail: feedback.current,
        //   phase_id: phaseID,
        //   migration_data_id	: MigID
        // },
    //     {
    //       headers: {
    //         Accept: "application/vnd.api+json",
    //         Authorization: `Bearer ${token}`,
    //         "Content-Type" : 'application/json'
    //       },
    //     }
    //   )
  }

  useEffect(() => {
    if (sessionStorage.getItem("survey") !== null) {
      let arr = JSON.parse(sessionStorage.getItem("survey"));
      let filtered = arr.filter((data) => data.phase_id == PhaseNos);
      if (filtered.length !== 0) {
        setVal("block");
        if (filtered[0].status == true) {
          setSubmit(true);
        }
      }
    }
  }, []);
console.log(Completed,Rating_obj,phase)
  return (
    <Element name={`review_${PhaseNos}`}>
      {!Submit && (
        <div
          className="review"
          style={{
            display: Completed == true && Rating_obj == null ? "block" : Val,
          }}
        >
          <div className="col-12 review-header spacebetween-align-div">
            <h5 className="px-4">How was your experience with “{phase}”?</h5>{" "}
            
          </div>
          <div className="review-body">
            <div className="px-3">
              <div className="pb-3">
                <figure
                  className="review-img"
                  style={{
                    cursor: "pointer",
                  }}
                >
                  <img
                    src="/images/Vector.svg"
                    data-rate="1"
                    onClick={() => setRating(1)}
                  />
                  <img
                    src="/images/Vector.svg"
                    data-rate="1"
                    onClick={() => setRating(2)}
                  />
                  <img
                    src="/images/Vector.svg"
                    data-rate="1"
                    onClick={() => setRating(3)}
                  />
                  <img
                    src="/images/Vector.svg"
                    data-rate="1"
                    onClick={() => setRating(4)}
                  />
                  <img
                    src="/images/Vector.svg"
                    data-rate="1"
                    onClick={() => setRating(5)}
                  />
                </figure>
                <figure
                  className="review-img reviewed"
                  style={{
                    position: "absolute",
                    marginTop: "-57px",
                    cursor: "pointer",
                  }}
                >
                  {[...new Array(Rating)].map((data, index) => {
                    return (
                      <img
                        src="/images/filled_star.svg"
                        data-rate="1"
                        onClick={() => setRating(index + 1)}
                      />
                    );
                  })}
                </figure>
              </div>
              <form className="mt-2">
                <div className="row">
                  <div className="col-12">
                    <label className="col-form-label">
                      Provide your feedback (optional)
                    </label>
                    <textarea
                      className="form-control resize-none"
                      id="message-text"
                      placeholder="Type here"
                      onChange={(e) => (feedback.current = e.target.value)}
                    ></textarea>
                  </div>
                </div>
              </form>
              <div className="mt-3 text-center">
                {Rating == 0 ? (
                  <button type="button" className="btn btn-disabled">
                    Submit
                  </button>
                ) : (
                  <button
                    type="button"
                    className="btn bg_primary_blue mb-3"
                    onClick={SubmitReview}
                  >
                    Submit
                  </button>
                )}
              </div>
            </div>
          </div>
        </div>
      )}
      {Submit && (
        <div>
          <h6 class="px-4 py-4 fw-bold mb-3 review">
            <i class="bi bi-check-circle-fill text-success px-2"></i> Thank you
            for your feedback!
          </h6>
        </div>
      )}
    </Element>
  );
}

export default Survey;
