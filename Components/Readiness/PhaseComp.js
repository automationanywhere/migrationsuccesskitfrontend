import React, { useState, useEffect, useRef } from 'react';
import { Link } from 'react-scroll';
import axios from 'axios';
import YouTube from 'react-youtube';

function PhaseComp({
	data,
	MigrationID,
	Addlabel,
	MarkasDone,
	ToDo,
	Completed,
	Editdata,
	LicenseData,
	Phase,
	InputIndex,
	UserLicenseData,
}) {
	const [PhaseData, setPhaseData] = useState(data);
	const [LicenseForm, setLicenseForm] = useState('none');
	const [CollapseData, setCollapseData] = useState([]);
	function MarkAsDoneRequest(ID) {
		axios
			.post(' https://3.208.44.204/migration_success_kit/v1/mark-done-step ', {
				migration_data_id: MigrationID,
				step_completed: ID,
			})
			.then((response) => console.log(response));

		console.log({
			migration_data_id: MigrationID,
			step_completed: ID,
		});
	}
	let LicenseIndex = useRef(0);
	let ArrowStyle = {
		'&::before': {
			color: 'red',
		},
	};
	let LicenseID = useRef({
		id: '',
		index: 0,
	});
	const style = {
		display: 'inline-block',
		height: '300px',
		position: 'relative',
		width: '100%',
	};
	console.log(Phase);
	function MigrationHead() {
		const [Video, setVideo] = useState(false);
		const [VideoCreds, setVideoCreds] = useState([350, '100%']);
		const opts = {
			height: VideoCreds[0],
			width: VideoCreds[1],
			playerVars: {
				// https://developers.google.com/youtube/player_parameters
				autoplay: Video && 1,
			},
		};
		return (
			<React.Fragment>
				{data !== undefined && (
					<div
						className={`migration_item px-3 py-3 migration_phase_${InputIndex}`}
						style={{ boxShadow: 'none' }}>
						<div className='col-sm-12 spacebetween-align-div'>
							<h3 className={`py-2 fc-theme `}>{data.title}</h3>
						</div>
						<div className='col-12 row'>
							<div className='col-sm-12 col-lg-6 col-xl-6'>
								{data.video_url !== '' && (
									<React.Fragment>
										{data.video_url.search('youtube') == -1 ? (
											<span
												className={`wistia_embed wistia_async_${
													data.video_url.split('/')[
														data.video_url.split('/').length - 1
													]
												} popover=true popoverAnimateThumbnail=true`}
												style={style}>
												&nbsp;
											</span>
										) : (
											<React.Fragment>
												{Video ? (
													<React.Fragment>
														{VideoCreds[2] == index && (
															<div className='youtube_video_player'>
																<style global jsx>{`
																	body {
																		overflow: hidden;
																	}
																`}</style>
																<YouTube
																	videoId={
																		data.video_url.split('=')[
																			data.video_url.split('=').length - 1
																		]
																	}
																	opts={opts}
																/>
															</div>
														)}
													</React.Fragment>
												) : (
													<div className='youtube_video_preLoaded'>
														<YouTube
															videoId={
																data.video_url.split('=')[
																	data.video_url.split('=').length - 1
																]
															}
															opts={opts}
															onPlay={() => {
																setVideoCreds([750, 1400, index]);
																setVideo(true);
															}}
														/>
													</div>
												)}
											</React.Fragment>
										)}
									</React.Fragment>
								)}
							</div>
							<div
								className='col-sm-12 col-lg-6 col-xl-6'
								style={{ width: data.video_url == '' && '100%' }}>
								<div
									dangerouslySetInnerHTML={{
										__html: data.footer_content,
									}}></div>

								<p className='h5'>{data.field_steps_navigation_label}</p>
								<ul className='fw-lighter content links'>
									{data.steps.map((res, index) => {
										return (
											<Link
												to={`phase_${InputIndex}_${index}`}
												smooth={true}
												duration={1000}
												key={res.title}>
												<li>
													<a href='#'>{res.title}</a>
												</li>
											</Link>
										);
									})}
								</ul>
							</div>
						</div>
					</div>
				)}
			</React.Fragment>
		);
	}

	function MigrationLicenseForm(params) {
		const [Name, setName] = useState('');
		const [Email, setEmail] = useState('');
		const [Nos, setNos] = useState('');
		const [Details, setDetails] = useState('');
		function LicensePostFunction(params) {
			axios
				.post(
					' https://3.208.44.204/migration_success_kit/v1/save-request-license ',
					{
						name: Name,
						email: Email,
						phone: Nos,
						more_details: Details,
						migration_data_id: MigrationID,
					}
				)
				.then((response) => console.log(response));
		}
		return (
			<div
				className='modal licence_modal license_modal'
				style={{ display: 'block' }}>
				<div className='modal-dialog modal-xl'>
					<style global jsx>{`
						header {
							opacity: 0.7;
						}
						.migration_progress {
							opacity: 0.7;
							filter: grayscale(0.6);
						}
						.MigrationSteps {
							opacity: 0.7;
							filter: grayscale(0.6);
						}
						body {
							overflow: hidden;
						}
					`}</style>
					<div className='modal-content'>
						<div className='modal-header'>
							<h5 className='modal-title' id='exampleModalLabel'>
								{data.steps[2].title}
							</h5>
							<button
								type='button'
								className='btn-close'
								onClick={() => setLicenseForm('none')}
								data-bs-dismiss='modal'
								aria-label='Close'></button>
						</div>
						<div className='modal-body'>
							<div className='col-12 px-2'>
								<div className='info-wrapper px-3 py-3'>
									<p>
										<i className='bi bi-info-circle-fill'></i>{' '}
										{LicenseData[0].field_licence_info_label}
									</p>
									<div className='col-12 px-3'>
										<div className='row'>
											<div className='col-4 license_fixed_info'>
												<div className='py-1'>
													{UserLicenseData.field_company_name_label}
												</div>
												<div className='fw-600'>
													{UserLicenseData.field_company_name}
												</div>
											</div>
											<div className='col-4 license_fixed_info'>
												<div className='py-1'>
													{UserLicenseData.field_control_room_version_label}
												</div>
												<div className='fw-600'>
													{UserLicenseData.field_control_room_version}
												</div>
											</div>
											<div className='col-4 license_fixed_info'>
												<div className='py-1'>
													{UserLicenseData.field_migation_type_label}
												</div>
												<div className='fw-600'>
													{UserLicenseData.field_migation_type}
												</div>
											</div>
										</div>
									</div>
								</div>
								<form className='mt-4'>
									<div
										dangerouslySetInnerHTML={{
											__html: LicenseData[0].field_header_description,
										}}></div>

									<div className='row'>
										<div className='col-6'>
											<label className='col-form-label'>
												{LicenseData[0].field_name_label}
											</label>
											<input
												type='text'
												className='form-control'
												onChange={(e) => setName(e.target.value)}
												placeholder={LicenseData[0].input_box_placeholder}
											/>
										</div>
										<div className='col-6'>
											<label className='col-form-label'>
												{LicenseData[0].field_email_label}
											</label>
											<input
												type='text'
												className='form-control'
												onChange={(e) => setEmail(e.target.value)}
												placeholder={LicenseData[0].input_box_placeholder}
											/>
										</div>
										<div className='col-6'>
											<label className='col-form-label'>
												{LicenseData[0].field_phone_number_label}
											</label>
											<input
												type='text'
												onChange={(e) => {
													setNos(e.target.value);
												}}
												className='form-control'
												placeholder={LicenseData[0].input_box_placeholder}
											/>
										</div>

										<div className='col-12'>
											<label className='col-form-label'>
												{LicenseData[0].field_more_detail_label}
											</label>
											<textarea
												className='form-control'
												id='message-text'
												onChange={(input) => {
													setDetails(input.target.value);
												}}
												placeholder={
													LicenseData[0].detail_placeholder
												}></textarea>
										</div>
									</div>
								</form>
							</div>
						</div>
						{Name && Email && Nos !== '' ? (
							<div className='modal-footer mb-3'>
								<button
									type='button'
									className='btn btn-border-blue'
									onClick={() => {
										data.steps[LicenseIndex.current]['licensed'] = true;
										MarkAsDoneRequest(LicenseID.current.id);
										data.steps[LicenseID.current.index].complete_status = true;
										setLicenseForm('none');
										Phase = Phase + 1;
										LicensePostFunction();

										sessionStorage.setItem('phaseSwitch', true);

										setPhaseData((e) => e);
									}}>
									{LicenseData[0].field_submit_label_for_licence}
								</button>
							</div>
						) : (
							<div className='modal-footer mb-3'>
								<button type='button' className='disabled_btn'>
									{LicenseData[0].field_submit_label_for_licence}
								</button>
							</div>
						)}
					</div>
				</div>
			</div>
		);
	}
	function MigrationSteps(params) {
		const [CompletedSteps, setCompletedSteps] = useState([]);
		const [AdditionalArray, setAdditionalArray] = useState([]);
		const [Collapsed, setCollapsed] = useState([]);
		const [VideoCreds, setVideoCreds] = useState([350, '100%']);
		const [Video, setVideo] = useState(false);
		const [CompletedLatest, setCompletedLatest] = useState(0);
		const [BotRunnerState, setBotRunnerState] = useState(null);

		function MarkAsUnDoneRequest(ID) {
			axios
				.post(
					' https://3.208.44.204/migration_success_kit/v1/mark-undone-step ',
					{
						migration_data_id: MigrationID,
						step_undone: ID,
					}
				)
				.then((response) => console.log(response));
		}
		const opts = {
			height: VideoCreds[0],
			width: VideoCreds[1],
			playerVars: {
				// https://developers.google.com/youtube/player_parameters
				autoplay: Video && 1,
			},
		};
		let RunnerPercentageValue = useRef('');
		useEffect(() => {
			document.addEventListener('click', () => {
				setVideoCreds([350, 570]);
				setVideo(false);
			});
			let CompletedStepLatest = [];
			let CompletedStepsArr = [];
			data.steps.forEach((e, index) => {
				if (e.complete_status == false) {
					CompletedStepLatest.push(index);
				} else {
					CompletedStepsArr.push(index);
				}
			});

			setCompletedLatest(CompletedStepLatest[0]);
			setCollapsed(CompletedStepsArr);
		}, []);
		console.log(Collapsed);
		return (
			<div className='col-12 mt-3'>
				<div className='collapse_wrapper'>
					{data.steps.map((steps, index) => {
						return (
							<React.Fragment key={steps.title}>
								{steps.licensed !== undefined ? (
									steps.complete_status && (
										<div className='collapse-item'>
											<div className='col-12 collapse-header spacebetween-align-div'>
												<h5 className='px-4'>
													Thank you for requesting your migration license.{' '}
												</h5>
											</div>

											<div className='collapse-body'>
												<div className='col-12 row px-3 py-2'>
													<p className='content'>
														We will respond within . If you don’t hear from us,
														email orders@automationanywhere.com. Once your
														license is ready, you or your procurement manager
														will receive an email with your license details.
														After that, come back to the Migration Success Kit
														to continue your migration.
													</p>
													<div
														className='mt-3'
														data-collapse-swticher='text-collapse'>
														{AdditionalArray.includes(index) ? (
															<h6
																data-collapse-btn='text-collapse'
																onClick={() => {
																	setAdditionalArray((e) =>
																		e.filter((data) => data !== index)
																	);
																}}>
																<i className='bi bi-chevron-down inverse-arrow'></i>{' '}
																{Addlabel}
															</h6>
														) : (
															<h6
																data-collapse-btn='text-collapse'
																onClick={() => {
																	setAdditionalArray([
																		...AdditionalArray,
																		index,
																	]);
																}}>
																<i className='bi bi-chevron-down'></i>{' '}
																{Addlabel}
															</h6>
														)}
														{AdditionalArray.includes(index) && (
															<React.Fragment>
																{[
																	{
																		title:
																			'Learn more about migration licenses',
																		uri: 'https://docs.automationanywhere.com/bundle/enterprise-v2019/page/enterprise-cloud/topics/migration/get-migration-license.html',
																	},
																].map((data) => {
																	return (
																		<div
																			key={data.title}
																			data-collapse-body='text-collapse'
																			className='links'>
																			<a
																				rel='noreferrer'
																				className='top-align-div'
																				href={data.uri}
																				target='_blank'>
																				<i className='doc_default_blue'></i>{' '}
																				<span className='text-decoration-underline px-2'>
																					{data.title}
																				</span>
																			</a>
																		</div>
																	);
																})}
															</React.Fragment>
														)}
													</div>

													<p className='content mt-4 mb-2'>
														In the meantime, learn more about the great new
														features of Automation 360:{' '}
													</p>
													<div className='links thank_comp'>
														<a href='#' target='_blank'>
															<i className='doc_default_black'></i>{' '}
															<span className='text-decoration-underline px-2'>
																Preparing for migration to Cloud{' '}
															</span>
														</a>
														<a href='#' target='_blank'>
															<i className='doc_default_black'></i>{' '}
															<span className='text-decoration-underline px-2'>
																How to migrate using Automation 360 and
																Enterprise 11 in parallel{' '}
															</span>
														</a>
														<a href='' target='_blank'>
															<i className='doc_default_black'></i>{' '}
															<span className='text-decoration-underline px-2'>
																How to migrate multiple environments, such as
																Dev, Test, and Production
															</span>
														</a>
													</div>
												</div>
											</div>
										</div>
									)
								) : (
									<div
										className={`collapse-item active phase_${InputIndex}_${index}`}
										style={{
											borderColor:
												Phase == InputIndex && CompletedLatest == index
													? '#4e6b8c '
													: 'white ',
										}}>
										<div className='col-12 collapse-header spacebetween-align-div px-3'>
											<div className='col-9'>
												{Collapsed.includes(index) ? (
													<div className='top-align-div'>
														<i
															className='bi bi-chevron-down fc-blue px-2 inverse-arrow'
															onClick={() => {
																setCollapsed((e) =>
																	e.filter((data) => data !== index)
																);
															}}></i>
														<h5 className='fw-600'>{steps.title}</h5>
													</div>
												) : (
													<div className='top-align-div'>
														<i
															className='bi bi-chevron-down fc-blue px-2'
															onClick={() => {
																setCollapsed([...Collapsed, index]);
															}}></i>
														<h5 className='fw-600'>{steps.title}</h5>
													</div>
												)}
											</div>
											{Phase == InputIndex && CompletedLatest == index && (
												<div className='col-3 right-align-div'>
													<div className='btn info-wrapper px-3 py-2 rounded-pill'>
														{ToDo}
													</div>
												</div>
											)}

											{steps.complete_status && (
												<div className='col-3 right-align-div'>
													<div className='btn info-wrapper px-3 py-2 rounded-pill'>
														{Completed}
													</div>
												</div>
											)}
										</div>
										{Collapsed.includes(index) == false && (
											<div className='collapse-body'>
												<div className='col-12 px-3 py-3'>
													<div className='row'>
														{steps.video_url !== '' && (
															<div className='col-sm-12 col-lg-6 col-xl-6'>
																{steps.video_url.search('youtube') == -1 ? (
																	<span
																		className={`wistia_embed wistia_async_${
																			steps.video_url.split('/')[
																				steps.video_url.split('/').length - 1
																			]
																		} popover=true popoverAnimateThumbnail=true`}
																		style={style}>
																		&nbsp;
																	</span>
																) : (
																	<React.Fragment>
																		{Video ? (
																			<React.Fragment>
																				{VideoCreds[2] == index && (
																					<div className='youtube_video_player'>
																						<style global jsx>{`
																							body {
																								overflow: hidden;
																							}
																						`}</style>
																						<YouTube
																							videoId={
																								steps.video_url.split('=')[
																									steps.video_url.split('=')
																										.length - 1
																								]
																							}
																							opts={opts}
																						/>
																					</div>
																				)}
																			</React.Fragment>
																		) : (
																			<div className='youtube_video_preLoaded'>
																				<YouTube
																					videoId={
																						steps.video_url.split('=')[
																							steps.video_url.split('=')
																								.length - 1
																						]
																					}
																					opts={opts}
																					onPlay={() => {
																						setVideoCreds([750, 1400, index]);
																						setVideo(true);
																					}}
																				/>
																			</div>
																		)}
																	</React.Fragment>
																)}
															</div>
														)}

														<div
															className='col-sm-12 col-lg-6 col-xl-6'
															style={{
																width: steps.video_url == '' && '100%',
															}}>
															<div
																dangerouslySetInnerHTML={{
																	__html: steps.footer_content,
																}}></div>
															{index == 1 && Phase == 0 && (
																<div className='form-group'>
																	<input
																		type='number'
																		className='textbox col-sm-9 col-lg-8 col-xl-8 is-valid'
																		max='100'
																		min='0'
																		maxLength='3'
																		onChange={(e) => {
																			if (e.target.value.search('-') == -1) {
																				RunnerPercentageValue.current =
																					e.target.value;
																				if (e.target.value.length == 0) {
																					setBotRunnerState(null);
																				} else {
																					setBotRunnerState(false);
																				}
																			} else {
																				e.target.value = 0;
																			}
																		}}
																		placeholder='Enter migratable bot percentage (%)'
																		required=''
																	/>
																	{typeof BotRunnerState == 'string' &&
																		(parseInt(RunnerPercentageValue.current) >
																		100 ? (
																			<p className='bot_runner_btn_fallback_message '>
																				Please enter a percentage 0%-100%
																			</p>
																		) : (
																			<p className='bot_runner_btn_fallback_message '>
																				The number field should only accept
																				numbers (no symbols, periods, letters),
																				and only up to 3 digits{' '}
																			</p>
																		))}
																	{data.steps[index - 1].complete_status ==
																	true ? (
																		<React.Fragment>
																			{BotRunnerState == null ||
																			typeof BotRunnerState == 'string' ? (
																				<button
																					className='disabled_btn  '
																					type='submit'
																					onMouseOver={(e) => {
																						if (
																							RunnerPercentageValue.current ==
																							''
																						) {
																							setBotRunnerState(
																								'Please enter the percent readiness from your Bot Scanner report '
																							);
																						}
																					}}
																					disabled=''>
																					Submit
																				</button>
																			) : (
																				<button
																					className='btn btn-secondary mt-3'
																					type='submit'
																					onClick={() => {
																						if (
																							parseInt(
																								RunnerPercentageValue.current
																							) > 100
																						) {
																							setBotRunnerState(
																								'Please enter a percentage 0%-100%'
																							);
																						} else {
																							setBotRunnerState(true);
																						}
																					}}
																					disabled=''>
																					Submit
																				</button>
																			)}
																		</React.Fragment>
																	) : (
																		<button
																			className='disabled_btn  '
																			type='submit'
																			disabled=''>
																			Submit
																		</button>
																	)}
																</div>
															)}

															<br />

															<div data-collapse-swticher='text-collapse'>
																{steps.field_additional_resources.length !==
																	0 && (
																	<React.Fragment>
																		{AdditionalArray.includes(index) ? (
																			<h6
																				data-collapse-btn='text-collapse'
																				onClick={() => {
																					setAdditionalArray((e) =>
																						e.filter((data) => data !== index)
																					);
																				}}>
																				<i className='bi bi-chevron-down inverse-arrow'></i>{' '}
																				{Addlabel}
																			</h6>
																		) : (
																			<h6
																				data-collapse-btn='text-collapse'
																				onClick={() => {
																					setAdditionalArray([
																						...AdditionalArray,
																						index,
																					]);
																				}}>
																				<i className='bi bi-chevron-down'></i>{' '}
																				{Addlabel}
																			</h6>
																		)}
																	</React.Fragment>
																)}

																{AdditionalArray.includes(index) && (
																	<React.Fragment>
																		{steps.field_additional_resources.map(
																			(data) => {
																				return (
																					<div
																						key={steps.title}
																						data-collapse-body='text-collapse'
																						className='links'>
																						<a
																							className='top-align-div'
																							href={data.uri}
																							target='_blank'
																							rel='noreferrer'>
																							<i className='bi bi-file-earmark-richtext'></i>{' '}
																							<span className='text-decoration-underline px-2'>
																								{data.title}
																							</span>
																						</a>
																					</div>
																				);
																			}
																		)}
																	</React.Fragment>
																)}
															</div>
														</div>
													</div>
												</div>
												{index == 1 && (
													<React.Fragment>
														{BotRunnerState == true && (
															<React.Fragment>
																{RunnerPercentageValue.current > 95 && (
																	<div className='col-12 py-4 px-4'>
																		<h6 className='success-info-wrapper px-3 py-3 fw-bold mb-3'>
																			<i className='bi bi-check-circle-fill text-success px-2'></i>{' '}
																			Great! Your bots are mostly ready to
																			migrate.
																		</h6>
																		<p className='content'>
																			{' '}
																			Review the report to see important
																			messages on:
																		</p>
																		<ul>
																			<li>
																				Any bots that need to be updated before
																				migration so they can be converted
																				successfully to Automation 360 format.
																			</li>
																			<li>
																				Any bots that cannot be migrated yet and
																				an estimated release timeline on when
																				they can be migrated so you can plan
																				their migration.
																			</li>
																		</ul>

																		<p className='content'>
																			{' '}
																			This enables you to plan your migration
																			journey better and prepare for a smooth
																			migration.
																		</p>
																	</div>
																)}
																{RunnerPercentageValue.current <= 95 && (
																	<div className='col-12 py-4 px-4'>
																		<p className='content'>
																			Since less than 95% of your bots are ready
																			to migrate, you might want to wait for a
																			future release to migrate. Review the Bot
																			Scanner report for details about the bots
																			that cannot be migrated yet and an
																			estimated release timeline when those bots
																			can be migrated.
																		</p>
																		<p className='content'>
																			If you wish to proceed with migration, you
																			can migrate all of the bots that are
																			supported now, then migrate the rest
																			later. Or, you can update the unsupported
																			bots in Enterprise 10 or 11 with supported
																			commands, then rerun the Bot Scanner and
																			migrate.
																		</p>
																		<p className='content'>
																			{' '}
																			If you wait to migrate, future versions of
																			Automation 360 will support migration of
																			more bots and features.
																		</p>
																	</div>
																)}
															</React.Fragment>
														)}
													</React.Fragment>
												)}

												<div className='col-12 px-2 py-2' id={index}>
													<div className='row spacebetween-align-div'>
														<div className='col-12 col-sm-12 col-lg-10 col-xl-10 d_flex'>
															{steps.field_request_licence_button !==
																undefined && (
																<React.Fragment>
																	{data.steps[index - 1].complete_status ==
																		true && steps.complete_status == false ? (
																		<div
																			data-bs-toggle='modal'
																			data-bs-target='#request_license'
																			data-bs-whatever='@license'
																			className='btn h6 bg_primary_blue btn_custom'
																			onClick={() => {
																				LicenseIndex.current = index;
																				LicenseID.current = {
																					id: steps.id,
																					index: index,
																				};
																				setLicenseForm('block');
																			}}>
																			<i className='bi bi-box-arrow-up-right'></i>{' '}
																			<span>Order my license</span>{' '}
																		</div>
																	) : (
																		<div
																			data-bs-toggle='modal'
																			data-bs-target='#request_license'
																			data-bs-whatever='@license'
																			className='disabled_btn'>
																			<i className='bi bi-box-arrow-up-right'></i>{' '}
																			<span>Order my license</span>{' '}
																		</div>
																	)}
																</React.Fragment>
															)}

															{steps.field_footer_content_links.map(
																(stepLink, index) => {
																	return (
																		<React.Fragment key={steps.title}>
																			{index == 0 ? (
																				<a
																					href={stepLink.uri}
																					target='_blank'
																					rel='noreferrer'>
																					<div className='btn h6 bg_primary_blue btn_custom'>
																						<i className='bi bi-download'></i>{' '}
																						<span> {stepLink.title}</span>{' '}
																					</div>
																				</a>
																			) : (
																				<a
																					href={stepLink.uri}
																					target='_blank'
																					rel='noreferrer'>
																					<div className='btn h6 btn-border-blue btn_custom'>
																						<i className='bi bi-file-earmark-richtext'></i>{' '}
																						<span> {stepLink.title}</span>{' '}
																					</div>
																				</a>
																			)}
																		</React.Fragment>
																	);
																}
															)}
														</div>

														{steps.field_request_licence_button ==
															undefined && (
															<React.Fragment>
																{steps.complete_status ? (
																	index == CompletedLatest - 1 && (
																		<div
																			className='btn btn-sm btn-border-blue btn-proceed'
																			onClick={() => {
																				data.steps[
																					index
																				].complete_status = false;
																				setCollapsed((e) => e);
																				MarkAsUnDoneRequest(steps.id);
																				setCompletedLatest((e) => e - 1);
																			}}>
																			<i className='bi bi-check-lg'></i>
																			{Editdata}
																		</div>
																	)
																) : index !== 0 ? (
																	data.steps[index - 1].complete_status ? (
																		index == data.steps.length - 1 ? (
																			<div
																				className='btn btn-sm btn-border-blue btn-proceed'
																				onClick={() => {
																					data.steps[
																						index
																					].complete_status = true;
																					setCollapsed([...Collapsed, index]);
																					MarkAsDoneRequest(steps.id);
																					setCompletedLatest((e) => e + 1);
																					setCompletedSteps([
																						...CompletedSteps,
																						true,
																					]);
																					sessionStorage.setItem(
																						'phaseSwitch',
																						true
																					);
																				}}>
																				<i className='bi bi-check-lg'></i>
																				{MarkasDone}
																			</div>
																		) : (
																			<div
																				className='btn btn-sm btn-border-blue btn-proceed'
																				onClick={() => {
																					data.steps[
																						index
																					].complete_status = true;
																					setCollapsed([...Collapsed, index]);
																					MarkAsDoneRequest(steps.id);
																					setCompletedLatest((e) => e + 1);
																				}}>
																				<i className='bi bi-check-lg'></i>
																				{MarkasDone}
																			</div>
																		)
																	) : (
																		<div className='disabled_btn'>
																			<i className='bi bi-check-lg'></i>
																			{MarkasDone}
																		</div>
																	)
																) : Phase == InputIndex ? (
																	<div
																		className='btn btn-sm btn-border-blue btn-proceed'
																		onClick={() => {
																			MarkAsDoneRequest(steps.id);
																			data.steps[index].complete_status = true;
																			setCollapsed([...Collapsed, index]);

																			setCompletedLatest((e) => e + 1);
																		}}>
																		<i className='bi bi-check-lg'></i>
																		{MarkasDone}
																	</div>
																) : (
																	<div className='disabled_btn'>
																		<i className='bi bi-check-lg'></i>
																		{MarkasDone}
																	</div>
																)}
															</React.Fragment>
														)}
													</div>
												</div>
											</div>
										)}
									</div>
								)}
							</React.Fragment>
						);
					})}
				</div>
			</div>
		);
	}

	return (
		<React.Fragment>
			<div className='MigrationSteps'>
				<MigrationHead />
				<MigrationSteps />
			</div>
			<div>{LicenseForm !== 'none' && <MigrationLicenseForm />}</div>
		</React.Fragment>
	);
}

export default PhaseComp;
