﻿import React, { useState, useEffect } from 'react';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import axios from 'axios';
import Router from 'next/router';

function Success({data}) {

	
	var Scroll   = require('react-scroll');
	var Element  = Scroll.Element;
	
	
	
	return (
		<Element name="Success">
			<section style={{ top: 0 }}>
			<div className='container' dangerouslySetInnerHTML={{__html: data}}>

			</div>
			{/* <div class='col-sm-12 px-4 py-0'>
					<div class='col-sm-12 col-lg-12 col-xl-12 margin-auto success-page'>
						<div class='mg_ass_welcome'>
							<h3 class='py-2 fc-theme fw-bold mb-2'>Complete Journey</h3>
							<div class='row'>
								<div class='col-2 col-sm-2 col-lg-1 col-xl-1'>
									<img src='/images/badge-icon.svg' />
								</div>
								<div class='col-10 col-sm-10 col-lg-11 col-xl-11 px-5'>
									<h5 class='fw-bold'>Congratulation!</h5>
									<p class='desc_content fs-6'>
										You have successfully completed the migration to A360.
									</p>
								</div>
							</div>
						</div>
						<div class='collapse-item mt-5'>
							<h5 class='collapse-header fw-600'>
								<i class='bi bi-chevron-down fc-blue px-2'></i> Help us
								improve!
							</h5>
							<div class='collapse-body'>
								<div class='col-12 px-3 py-3'>
									<p class='content'>
										Have a feedback for us? or want more details in the
										migration success kit? Take the survey and help us improve
										and show you the content you need while migrating to A360
									</p>
									<a href="https://automationanywhere.co1.qualtrics.com/jfe/form/SV_4ZRXvC66449nslE" target="_blank">
										<button class='btn bg_primary_blue mt-2'  >
										Take survey
									</button>
										</a>
									
								</div>
							</div>
						</div>

						<div class='collapse-item'>
							<h5 class='collapse-header fw-600'>
								<i class='bi bi-chevron-down fc-blue px-2'></i> What’s new
								in A360 cloud!
							</h5>

							<div class='collapse-body'>
								<div class='col-12 row px-3 py-3'>
									<div class='col-sm-12 col-lg-4 col-xl-4'>
										<span
											class='wistia_embed wistia_async_gnvwudhdir popover=true popoverAnimateThumbnail=true success_video'
											>
											&nbsp;
										</span>
									</div>
									<div class='col-sm-12 col-lg-8 col-xl-8'>
										<p class='content'>
											What’s next? Keep learning about the great new features of
											Automation 360
										</p>
										<div
											data-collapse-body='text-collapse'
											class='links mt-3'>
											<a
												class='top-align-div'
												rel='noreferrer'
												href='https://docs.automationanywhere.com/bundle/enterprise-v2019/page/enterprise-cloud/topics/release-notes/cloud-release-notes.html'
												target='_blank'>
												<i class='bi bi-file-earmark-richtext'></i>
												<span class='text-decoration-underline px-2'>
													Automation 360 release notes
												</span>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div> */}
		</section>
		</Element>
		
	);
}

export default Success;
