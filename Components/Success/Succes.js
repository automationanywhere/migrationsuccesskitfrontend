﻿import React, { useState, useEffect } from 'react';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import axios from 'axios';
import Router from 'next/router';

function MigrationSuccess() {
	const style = {
		display: 'inline-block',
		height: '300px',
		position: 'relative',
		width: '100%',
	};
	const [ParentData, setParentData] = useState([]);
	const [UserName, setUserName] = useState('');
	const [Passed, setPassed] = useState('');

	useEffect(() => {
		if (sessionStorage.getItem('auth_root') == 'success') {
			axios({
				url: 'https://3.208.44.204/migration_success_kit/get-global-constant?_format=json',
				method: 'get',
				headers: {
					Accept: 'application/vnd.api+json',
					Authorization: `Bearer ${sessionStorage.getItem('auth_token')}`,
				},
			})
				.then((e) => setParentData(e.data[0]))
				.then(() => setUserName(sessionStorage.getItem('user')))
				.then(() => setPassed(true));
		} else {
			// console.log(typeof sessionStorage.getItem('auth_root'));
			if (typeof sessionStorage.getItem('auth_root') == 'object') {
				Router.push('https://botstore.automationanywhere.com/');
			} else {
				Router.push(`/${sessionStorage.getItem('auth_root')}`);
			}
		}
	}, []);
	return (
		<React.Fragment>
			{Passed && (
				<React.Fragment>
					<Header
						title={ParentData.title[0].value}
						Links={ParentData.field_quick_links}
						LoginLabel={ParentData.field_login_text_label[0].value}
						LinkLabel={ParentData.field_quick_link_label[0].value}
						User={UserName}
					/>
					<section>
						<div className='container'>
							<div className='col-sm-12 px-4 py-0'>
								<div className='col-sm-12 col-lg-12 col-xl-12 margin-auto success-page'>
									<div className='mg_ass_welcome'>
										<h3 className='py-2 fc-theme fw-bold mb-2'>
											Complete Journey
										</h3>
										<div className='row'>
											<div className='col-2 col-sm-2 col-lg-1 col-xl-1'>
												<img src='/images/badge-icon.svg' />
											</div>
											<div className='col-10 col-sm-10 col-lg-11 col-xl-11 px-5'>
												<h5 className='fw-bold'>Congratulation!</h5>
												<p className='desc_content fs-6'>
													You have successfully completed the migration to A360.
												</p>
											</div>
										</div>
									</div>
									<div className='collapse-item mt-5'>
										<h5 className='collapse-header fw-600'>
											<i className='bi bi-chevron-down fc-blue px-2'></i> Help
											us improve!
										</h5>
										<div className='collapse-body'>
											<div className='col-12 px-3 py-3'>
												<p className='content'>
													Have a feedback for us? or want more details in the
													migration success kit? Take the survey and help us
													improve and show you the content you need while
													migrating to A360
												</p>
												<button className='btn bg_primary_blue mt-2' onClick={() => Router.push("https://automationanywhere.co1.qualtrics.com/jfe/form/SV_4ZRXvC66449nslE")}>
													Take survey
												</button>
											</div>
										</div>
									</div>

									<div className='collapse-item'>
										<h5 className='collapse-header fw-600'>
											<i className='bi bi-chevron-down fc-blue px-2'></i> What’s
											new in A360 cloud!
										</h5>

										<div className='collapse-body'>
											<div className='col-12 row px-3 py-3'>
												<div className='col-sm-12 col-lg-4 col-xl-4'>
													<span
														className='wistia_embed wistia_async_gnvwudhdir popover=true popoverAnimateThumbnail=true'
														style={style}>
														&nbsp;
													</span>
												</div>
												<div className='col-sm-12 col-lg-8 col-xl-8'>
													<p className='content'>
														What’s next? Keep learning about the great new
														features of Automation 360
													</p>
													<div
														data-collapse-body='text-collapse'
														className='links mt-3'>
														<a
															className='top-align-div'
															rel='noreferrer'
															href='https://docs.automationanywhere.com/bundle/enterprise-v2019/page/enterprise-cloud/topics/release-notes/cloud-release-notes.html'
															target='_blank'>
															<i className='bi bi-file-earmark-richtext'></i>
															<span className='text-decoration-underline px-2'>
																Automation 360 release notes
															</span>
														</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
					<Footer data={ParentData.field_footer_section[0].value} val='' />
				</React.Fragment>
			)}
		</React.Fragment>
	);
}

export default MigrationSuccess;
