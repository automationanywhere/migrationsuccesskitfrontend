import React, { useState, useEffect, useRef } from 'react';
import Head from 'next/head';
import Link from 'next/link';
import Router from 'next/router';
import { API_ENDPOINT_URL, HEADER_PROFILE_URL, HEADER_LOGOUT_URL } from '../../configuration';
import validator from 'validator'
import jwt_decode from "jwt-decode";
import axios from 'axios';
export default function Header({
	title,
	Links,
	LoginLabel,
	LinkLabel,
	Collapse,
	User,
}) {
	const [LinkDropDown, setLinkDropDown] = useState(
		'menu-item px-2 full-height'
	);
	const [CollapseNos, setCollapseNos] = useState(3);
	const [ActiveClass, setActiveClass] = useState(false);
	const [Profile, setProfile] = useState(false);
	const [Invite, setInvite] = useState(false)
	const [EmailList, setEmailList] = useState([])
	const [BackupArr, setBackupArr] = useState([])
	const [DeleteMode, setDeleteMode] = useState(false)
	const [EmailError, setEmailError] = useState(false)
	const [InviteLoader, setInviteLoader] = useState(false)
	const [EmaillCheck, setEmaillCheck] = useState(false)
	var decoded = jwt_decode(sessionStorage.getItem("system"));
	let optional_data = useRef("")
	let email_value = useRef("")
	useEffect(() => {
		document.addEventListener('click', (e) => {
			if (e.target.tagName == 'SECTION') {
				setActiveClass(false);
				setProfile(false);
			} else if (e.target.tagName == 'DIV') {
				// if (e.target.className !== 'menu-item px-2 full-height active') {
				setActiveClass(false);
				setProfile(false);

				// }
			}
		});
		// document.addEventListener('keydown', function (event) {
		// 	const key = event.key; // const {key} = event; ES6+
		// 	if (key === "Backspace") {
		// 		let session_data = sessionStorage.getItem("temp_invite")
		// 		let option_data = sessionStorage.getItem("optional")
		// 		let delete_data = sessionStorage.getItem("delete")
		// 		let texting = sessionStorage.getItem("text_cache")
		// 		// console.log(EmailList)
		// 		if (session_data !== null && option_data == null && delete_data == null && texting == null) {
		// 			let parsed = JSON.parse(session_data)
		// 			let sliced = parsed.slice(0, -1)
		// 			setEmailList(sliced)
		// 			sessionStorage.setItem("temp_invite", JSON.stringify(sliced))
		// 		}

		// 		// if(Invite == true){
		// 		// 	setEmailList(['test'])
		// 		// 	console.log("removing")
		// 		// }
		// 	}
		// });
	}, [screen.width]);
	function InvitePost(params) {
		function PostData(params) {
			(() => {
				setInviteLoader(true)
			})();
			axios.post(API_ENDPOINT_URL + "v1/invite-user/?_format=json", {
				email: email_value.current,
				text: optional_data.current
			}, {
				headers: {
					Accept: "application/vnd.api+json",
					Authorization:
						`Bearer ${decoded.auth_token}`,
					"Content-Type": "application/json",
				}
			}).then(function (response) {
				setLinkDropDown(false)
				setInvite(false)
				setInviteLoader(false)
				setEmailList([])
				setEmailError(false)
				setEmaillCheck(false)
			})
				.catch(function (error) {
					Router.push("api-failed");
				});
		}
		if (email_value.current.split(',').length == 1) {
			if (validator.isEmail(email_value.current)) {

				setEmaillCheck(true)
				setEmailError(false)
				PostData()
			} else {
				setEmailError(true)

			}
		} else {
			async function EmailIteration(params) {
				let error = false
				email_value.current.split(',').forEach((data) => {
					if (validator.isEmail(data) == false) {
						error = true

					}
				})
				if (error) {
					throw error
				} else {
					return error
				}
			}
			EmailIteration().then(() => PostData()).catch(() => setEmailError(true))

		}



	}
	return (
		<React.Fragment>
			<Head>
				<link
					rel='stylesheet'
					href='https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css'
				/>
			</Head>
			<header>
				<div className='container-fluid'>
					<div className='row'>
						<div className='col-8 col-sm-6 left-align-div py-2'>
							<span className='logo'>
								{' '}
								<img alt='' src='/images/logo-aa.svg' />
							</span>
							<span className='logo_text px-2 d-none d-sm-block'>{title}</span>
						</div>

						<div className='col-4 col-sm-6 _hm right-align-div pd-r-0'>
							<div className='hm-header-menu-list right-align-div full-height'>
								<div
									className="menu-item px-2 full-height"
									data-hm-wrapper='box'>
									<div
										className='menu-item-title px-2 right-align-div'
										onClick={() => {
											setInvite(true);

										}}
										data-hm-switcher='hm-switcher'>
										<span className='d-none d-md-block'>Invite</span>
										<i className='mx-2 icon-design icon-usergroup'></i>
									</div>


								</div>

								<div
									className={`menu-item px-2 full-height ${Profile && 'active'
										}`}
									data-hm-wrapper='box'>

									<div
										className='menu-item-title px-2 right-align-div'
										onClick={() => {
											setProfile((e) => !e);
										}}
										data-hm-switcher='hm-switcher'>
										<span className='mx-2 icon-design user_icon'>
											{User.charAt(0)}
										</span>
										<span className='d-none d-sm-block'>{User}</span>
										<i className='bi bi-caret-down-fill mx-2'></i>
									</div>


									<div
										className='menu-item-content text-center'
										style={{ paddingTop: '0' }}>
										<a
											className='menu-item-data'
											onClick={() =>
												Router.push(
													HEADER_PROFILE_URL
												)
											}>
											Profile
										</a>
										<a
											className='menu-item-data'
											onClick={() => {
												sessionStorage.removeItem('system');
												sessionStorage.removeItem('url_root');
												Router.push(
													HEADER_LOGOUT_URL

												);
											}}>
											Logout
										</a>
									</div>
								</div>
								{/* <div className='menu-item px-2 full-height' data-hm-wrapper='box'>
									<div
										className='menu-item-title px-2 right-align-div'
										data-hm-switcher='hm-switcher'>
										<span className='mx-2 icon-design user_icon'>J</span>
										<span className='d-none d-md-block'>John Doe</span>
										<i className='bi bi-caret-down-fill mx-2'></i>
									</div>
									<div className='menu-item-content text-center'>
										<a className='menu-item-data'>Profile</a>
										<a className='menu-item-data'>Logout</a>
									</div>
								</div> */}
							</div>
						</div>
					</div>
				</div>
				{Invite &&
					<div className="modal fade invite_team show" id="invite_team" tabindex="-1" aria-labelledby="invite_team_label" aria-hidden="true" style={
						{
							color: "black",
							backgroundColor: "#00000042",
							display: "flex",
							alignItems: "center",

						}
					}>
						<style jsx global>{`
        body {
          overflow : hidden;
        }
      `}</style>
						<div className="modal-dialog modal-lg" style={{
							width: "fit-content"
						}}>
							<div className="modal-content" style={{
								width: "422px",
								border: "none",
								borderBottomLeftRadius: "0",
								borderBottomRightRadius: "0"
							}}>
								{InviteLoader && <div className='loader'></div>}
								<div className="modal-header" >
									<h5 className="modal-title" id="invite_team_label" style={{
										fontSize: "18px",
										marginLeft: "7px"
									}}>Invite your team</h5>
									<button type="button" style={{
										fontSize: "11px",
										position: "relative",
										top: "2px",
										right: "13px"
									}} className="btn-close" data-bs-dismiss="modal" onClick={() => {
										setInvite(false)
										sessionStorage.removeItem("temp_invite")
										setEmailList([])
										setDeleteMode(false)
									}} aria-label="Close"></button>
								</div>
								<div className="modal-body" style={{
									visibility: InviteLoader && "hidden",
									marginBottom: "28px",
									paddingTop: "6px"
								}}>
									<div className="col-12 px-2">

										<form className="mt-2" onSubmit={(e) => {
											e.preventDefault()
											// setEmailList([...EmailList,e.target.worker_email.value])
											// e.target.worker_email.value = null
											// if(DeleteMode){
											// 	let email = e.target.worker_email.value
											// if (validator.isEmail(email)) {
											// 	setEmailList([...BackupArr,email])
											// 	e.target.worker_email.value = null
											// 	setDeleteMode(false)
											// 	setEmail("")
											// 	sessionStorage.setItem('temp_invite', JSON.stringify([...BackupArr,email]))
											// 	sessionStorage.removeItem("delete")

											// }

											// }else{
											// let email = e.target.worker_email.value

											// if(email.split(',').length == 1){

											// 	if (validator.isEmail(email)) {
											// 		if (EmailList.includes(email) == false) {
											// 			setEmailList([...EmailList, email])
											// 			// sessionStorage.setItem('temp_invite', JSON.stringify([...EmailList, email]))
											// 			e.target.worker_email.value = null
											// 			// sessionStorage.removeItem("text_cache")
											// 			setEmailError(false)
											// 		}

											// 	} else {
											// 		setEmailError(true)
											// 	}
											// }else{
											// 	let FinalEmail = []
											// 	email.split(',').forEach((email) => {
											// 		if (validator.isEmail(email.trim())) {
											// 			if (EmailList.includes(email.trim()) == false) {
											// 				FinalEmail.push(email.trim())
											// 				// sessionStorage.setItem('temp_invite', JSON.stringify([...EmailList, email.trim()]))
											// 				e.target.worker_email.value = null
											// 				// sessionStorage.removeItem("text_cache")
											// 				setEmailError(false)
											// 			}

											// 		} else {
											// 			setEmailError(true)
											// 		}
											// 	})

											// 	setEmailList(EmailList.concat(FinalEmail))
											// }


											// }



										}}>
											<div className="row">
												<div className="col-12 mb-3">
													<label className="col-form-label" style={{
														fontSize: "12px"
													}}>Email address</label>
													<div className="invite_email_parent" style={{
														display: EmailList.length >= 4 && "block"
													}}>
														<div className="invited_users">
															{EmailList.map(data => {
																return (
																	<div className="invited_users_object" onClick={() => {
																		// setDeleteMode(true)
																		// setBackupArr(EmailList.filter((e) => e !== data))
																		setEmailList(EmailList.filter((e) => e !== data))
																		// sessionStorage.setItem("delete",true)
																	}}>


																		<h5>

																			{data.split('@')[0].split('.')[0]}

																		</h5>

																	</div>
																)
															})}

														</div>
														<input type="text" style={{
															borderColor: "#BBBBBB"
														}} onChange={(e) => {
															// if (e.target.value.length == 0) {
															// 	sessionStorage.removeItem("text_cache")
															// } else {
															// 	sessionStorage.setItem("text_cache", true)
															// }
															let email = e.target.value
															email_value.current = email
															if (email.length !== 0) {
																setEmaillCheck(true)
															} else {
																setEmaillCheck(false)
															}



															console.log("changed")
														}} placeholder='Email, comma separated' name="worker_email" key={DeleteMode} autoComplete="off" />

														{/* {DeleteMode && <input type="text" placeholder='enter email and press enter' name="worker_email" defaultValue={EmailList[0]} key={DeleteMode} autoComplete="off" />} */}

													</div>
													{EmailError && <p className='sm-content' style={{
														color: "red",
														marginBottom: "0"
													}}>This is not a valid email. Please update.</p>}

													{/* <input onChange={(e) => setEmail(e.target.value)} type="text" className="form-control" placeholder="Email, comma separated "/> */}
												</div>
												<div className="col-12">
													<label className="col-form-label" style={{
														fontSize: "12px"
													}}>Comments (optional)</label>
													<textarea type="text" className="form-control resize-none" maxLength="500" style={{
														borderColor: "#BBBBBB"
													}} onChange={(e) => optional_data.current = e.target.value} placeholder="Type here" rows="5" onBlur={() => sessionStorage.removeItem("optional")} onFocus={() => sessionStorage.setItem("optional", true)}></textarea>
												</div>

											</div>

										</form>
									</div>

								</div>
								<div className="modal-footer mb-3" style={{
									visibility: InviteLoader && "hidden",
									height: "35px"
								}}>
									{!EmaillCheck ? <button type="button" className=" survey_btn">Send invite</button> : <button type="button" onClick={InvitePost} style={
										{
											backgroundColor: "rgb(78, 107, 140)",
											color: "white",
											position: "relative"
										}
									} className="survey_btn">Send invite</button>}

								</div>
							</div>
						</div>
					</div>
				}

			</header>

		</React.Fragment>
	);
}
