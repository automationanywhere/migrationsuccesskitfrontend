import React, { useContext } from 'react';
import Head from 'next/head';
import { MigrationVersionContext } from './MigrationContext';
function MigrationForm() {
	const theme = useContext(MigrationVersionContext);
	console.log(theme);
	// ** Multiple Options Available
	// function OptionMOA(params) {
	// 	return (
	// 		<>
	// 			<div className='r_crv'>
	// 				<div
	// 					className='form-group py-3 active'
	// 					data-result-category='crv'
	// 					data-result-for='success'>
	// 					<label className='fs-5'>
	// 						What Automation 360 deployment would you like to migrate to?
	// 					</label>
	// 					<div className='row'>
	// 						<div
	// 							className='col-sm-6 col-lg-4 col-xl-3 py-4 card-wrapper'
	// 							data-version='1'>
	// 							<div className='card version_type_card text-center left-align-div'>
	// 								<div className='card-img px-5 py-5'>
	// 									<img src='/images/demo.jpg' className='img-responsive' />
	// 								</div>
	// 								<div className='card-title px-3 py-3 bg-dark'>Cloud</div>
	// 								<div className='card-body'>
	// 									Hosted and managed in the Automation Anywhere Enterprise
	// 									Cloud.
	// 								</div>
	// 								<div className='special-tag'>Recommended!</div>
	// 							</div>
	// 						</div>
	// 						<div
	// 							className='col-sm-6 col-lg-4 col-xl-3 py-4 card-wrapper'
	// 							data-version='2'>
	// 							<div className='card version_type_card text-center'>
	// 								<div className='card-img px-5 py-5'>
	// 									<img src='/images/demo.jpg' className='img-responsive' />
	// 								</div>
	// 								<div className='card-title px-3 py-3 bg-dark'>
	// 									On-premises
	// 								</div>
	// 								<div className='card-body'>
	// 									Hosted on your infrastructure with optional updates from the
	// 									Automation Anywhere Enterprise Cloud
	// 								</div>
	// 							</div>
	// 						</div>
	// 					</div>
	// 				</div>
	// 			</div>
	// 			<div className='mt-4 px-4 py-4  bg-light helpful_data'>
	// 				<h6 className='fs-5'>Helpful links </h6>
	// 				<a
	// 					className='top-align-div'
	// 					href='https://www.automationanywhere.com/rpa/cloud-rpa'
	// 					target='_blank'>
	// 					<i className='bi bi-file-earmark-richtext'></i>{' '}
	// 					<span className='text-decoration-underline px-2'>
	// 						Learn about Cloud RPA{' '}
	// 					</span>
	// 				</a>
	// 				<a className='top-align-div' href='#' target='_blank'>
	// 					<i className='bi bi-youtube'></i>{' '}
	// 					<span className='text-decoration-underline px-2'>
	// 						How to choose your Automation 360 deployment model{' '}
	// 					</span>
	// 				</a>
	// 				<a
	// 					className='top-align-div'
	// 					target='_blank'
	// 					href='https://docs.automationanywhere.com/bundle/enterprise-v2019/page/enterprise-cloud/topics/migration/choose-deployment-model.html'>
	// 					<i className='bi bi-file-earmark-richtext'></i>{' '}
	// 					<span className='text-decoration-underline px-2'>
	// 						Read about deployment options
	// 					</span>
	// 				</a>
	// 			</div>
	// 		</>
	// 	);
	// }
	// ** Only on Premises Available
	// function OptionOPA(params) {
	// 	return (
	// 		<React.Fragment>
	// 			<div className='r_crv'>
	// 				<div
	// 					className='card-notify mt-3 px-4 py-4 active'
	// 					data-result-category='crv'
	// 					data-result-for='error'>
	// 					<p className='fs-5 fw-600'>
	// 						Your Control Room version can currently only be migrated to
	// 						Automation 360 on-premises. Each Automation 360 release enables
	// 						more Enterprise 10 and 11 versions to be migrated to cloud, so if
	// 						you want to migrate to cloud, we can notify you when the next
	// 						release comes out with more supported versions.
	// 						<br />
	// 						<br />
	// 						Would you like to:
	// 					</p>
	// 					<div className='form-group top-align-div'>
	// 						<span>
	// 							<input
	// 								type='radio'
	// 								name='premises'
	// 								className='crv_custom_radio'
	// 							/>
	// 						</span>
	// 						<div className='input-label px-3'>
	// 							<span className='fs-5'>
	// 								Wait to be notified when your Control Room version can be
	// 								migrated to cloud{' '}
	// 							</span>
	// 						</div>
	// 					</div>
	// 					<div className='form-group top-align-div'>
	// 						<span>
	// 							<input
	// 								type='radio'
	// 								name='premises'
	// 								className='crv_custom_radio'
	// 							/>
	// 						</span>
	// 						<div className='input-label px-3'>
	// 							<span className='fs-5'>Migrate to on-premises</span>
	// 						</div>
	// 					</div>
	// 				</div>
	// 				<div className='mt-4 px-4 py-4  bg-light helpful_data'>
	// 					<h6 className='fs-5'>Helpful links </h6>
	// 					<a className='top-align-div' href='#' target='_blank'>
	// 						<i className='bi bi-youtube'></i>{' '}
	// 						<span className='text-decoration-underline px-2'>
	// 							how to choose your Automation 360 deployment model
	// 						</span>
	// 					</a>
	// 					<a
	// 						className='top-align-div'
	// 						target='_blank'
	// 						href='https://docs.automationanywhere.com/bundle/enterprise-v2019/page/enterprise-cloud/topics/migration/choose-deployment-model.html'>
	// 						<i className='bi bi-file-earmark-richtext'></i>{' '}
	// 						<span className='text-decoration-underline px-2'>
	// 							Read about deployment options
	// 						</span>
	// 					</a>
	// 				</div>
	// 			</div>
	// 		</React.Fragment>
	// 	);
	// }
	// ** No Options Available
	function OptionNOA(params) {
		return (
			<React.Fragment>
				<div className='r_crv'>
					<div
						className='card-notify mt-3 px-4 py-4'
						data-result-category='crv'
						data-result-for='error'>
						<p className='fs-5 fw-600'>
							We’re sorry - your Control Room version currently cannot be
							migrated to Automation 360. Each Automation 360 release enables
							more Enterprise 10 and 11 versions to be migrated, so we can
							notify you when the next release comes out with more supported
							versions.
						</p>
						<p className='fs-5'>
							Click here to be notified when your Control Room version is ready
							to migrate:{' '}
						</p>
						<a href='#' className='fs-5 fw-600'>
							Notify Me
						</a>
					</div>
				</div>
				<div className='mt-4 px-4 py-4  bg-light helpful_data'>
					<h6 className='fs-5'>Helpful links </h6>
					<a className='top-align-div' href='#'>
						<i className='bi bi-youtube'></i>{' '}
						<span className='text-decoration-underline px-2'>
							how to choose your Automation 360 deployment model
						</span>
					</a>
					<a
						className='top-align-div'
						target='_blank'
						rel='noreferrer'
						href='https://docs.automationanywhere.com/bundle/enterprise-v2019/page/enterprise-cloud/topics/migration /migrate-certified-versions.html'>
						<i className='bi bi-file-earmark-richtext'></i>{' '}
						<span className='text-decoration-underline px-2'>
							Which Version are supported for migration to Automation 360?
						</span>
					</a>
					<a
						className='top-align-div'
						target='_blank'
						rel='noreferrer'
						href='https://docs.automationanywhere.com/bundle/enterprise-v2019/page/enterprise-cloud/topics/migration/choose-deployment-model.html'>
						<i className='bi bi-file-earmark-richtext'></i>{' '}
						<span className='text-decoration-underline px-2'>
							Read about deployment options
						</span>
					</a>
				</div>
			</React.Fragment>
		);
	}
	return (
		<div>
			<Head>
				<link
					rel='stylesheet'
					href='https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css'
				/>
			</Head>
			<OptionNOA />
		</div>
	);
}

export default MigrationForm;
