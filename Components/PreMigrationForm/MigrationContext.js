import { createContext } from 'react';

export const MigrationVersionContext = createContext(null);

