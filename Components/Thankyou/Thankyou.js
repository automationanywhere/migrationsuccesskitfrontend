import react,{useState} from "react"

function ThankyouComponent(index) {
    const [Add, setAdd] = useState(0)
    const arr = useRef(false)
    console.log(arr.current)
    function AdditionalResources(params) {
        const [Bar, setBar] = useState(false)
        return(
            <React.Fragment>
                <button onClick={() =>  setBar((e) => !e)}>click</button>
                <h1>{Bar ? "true":"false"}</h1>
            </React.Fragment>
            
        )
    }
    return (
        <div className='collapse-item'>
            <div className='col-12 collapse-header spacebetween-align-div'>
                <h5 className='px-4'>
                    {ReadinessData.thank_you[0].title}
                </h5>
            </div>

            <div className='collapse-body'>
                <div className='col-12 row px-3 py-2' >
                    <div dangerouslySetInnerHTML={{
                        __html: ReadinessData.thank_you[0].field_thanks_you_detail,
                    }}>

                    </div>
                    <AdditionalResources/>
                    <div
                        className='mt-3'
                        data-collapse-swticher='text-collapse'>
                        
                            <h6
                                data-collapse-btn='text-collapse'
                                onClick={() => {
                                    
                                    arr.current = !arr.current
                                }}>
                                <i className={`bi bi-chevron-down ${arr.current && "inverse-arrow"}`}></i>{' '}
                                {Addlabel}
                            </h6>
                        
                        {arr.current && (
                            <React.Fragment>
                                {ReadinessData.thank_you[0].field_additional_resource.map((data) => {
                                    return (
                                        <div
                                            key={data.title}
                                            data-collapse-body='text-collapse'
                                            className='links'>
                                            <a
                                                rel='noreferrer'
                                                className='top-align-div'
                                                href={data.uri}
                                                target='_blank'>
                                                <i className='doc_default_blue'></i>{' '}
                                                <span className='text-decoration-underline px-2'>
                                                    {data.title}
                                                </span>
                                            </a>
                                        </div>
                                    );
                                })}
                            </React.Fragment>
                        )}
                    </div>

                    <div dangerouslySetInnerHTML={{
                        __html: ReadinessData.thank_you[0].footer_content,
                    }}>

                    </div>


                    <div className='links thank_comp'>
                        {ReadinessData.thank_you[0].field_footer_content_links.map((data) => {
                            return (
                                <a href={data.uri} target='_blank' key={data.uri} rel='noreferrer'>
                                    <i className={data.description}></i>{' '}
                                    <span className='text-decoration-underline px-2'>
                                        {data.title}
                                    </span>
                                </a>
                            )
                        }

                        )}


                    </div>
                </div>
            </div>
        </div>
    )
}
export default ThankyouComponent;