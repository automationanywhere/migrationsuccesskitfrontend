import React from 'react'

function Footer({data,val}) {
    return (
        
		<footer className={`bg-light py-4 text-center fc-dark h6 footer ${val}`}>
			<div className='container'>
				<div
				    className='col-12'
					dangerouslySetInnerHTML={{
									__html: data,
				}}></div>
				</div>
		</footer>
					
    )
}

export default Footer
