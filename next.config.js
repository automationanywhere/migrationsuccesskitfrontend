const securityHeaders = [
	{
	  key: "X-Frame-Options",
	  value: "SAMEORIGIN",
	},
  ];
  

module.exports = {
	distDir: 'build',
	images: {
		loader: 'akamai',
		path: '',
	},
	async headers() {
		return [
		  {
			// Apply these headers to all routes in your application.
			source: "/:path*",
			headers: securityHeaders,
		  },
		  
		];
	  },
	async rewrites() {
		return [
		  {
			source: '/migration_success_kit/:path*',
			destination: 'https://devmskdrupal.aaiengineering.com/migration_success_kit/:path*' // Proxy to Backend
		  }
		]
	  }
};
